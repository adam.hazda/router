Router

Terminology

- __Route__ (DS)
    - It is a data structure holding following information:
        1. HTTP Method
        2. Type - Every Route is one of following types [ Controller, Argument Name, Argument Value ], these types are important for Route Resolving logic
        3. Position within URI (Path / Query string / Fragment)
        4. Identifier - string value identifying a Route within URI (https://eshop.com/ __product__ / __available__ , 2 identifiers: product, available)
        5. [Endpoint] - represented as a callable (equivalent to __Controller__) (ProductController::list)
        6. [Resource] - represents the main routable resource which might be a product, category, content or anything that can be rendered as a page or other type of document (like JSON)
        7. [ResourceSpecifiers] - these are entities specifying the main Resource like filters, tags, labels, switches and sutff (availability, parameters...)
        
- __Resource__ (DS)
    - It is a data structure holding following information:
        1.  Routable Type - this differentiates between many types of __routable__ entities like Products, Categories, Contents 
            but also there is a __routable type__ for availability and parameter filters, tags etc. which are really Resource Specifiers
            but this is just what they have in common. __Routable type__ also plays important role in the routing process.
            
- __Resource Specifier__ (DS)
    - It is a data structure holding following information:
        1. Routable Type - just as it's parent __Resource__
        2. Type - Every Resource Specifier in one of following types [ Standalone, Bound, Paired ]
            - Standalone - does not need any kind of pair specifier
            - Bound - in order to be valid, needs a child specifier(s) ( color/blue,red where __blue__ and __red__ are __child specifiers__ bound to __color__)
            - Paired - in order to be valid, needs a value ( price/200-300 where __200-300__ is the value)
        3. Single Occurrence - this is a flag saying whether or not it's allowed to use two Resources of one type within one URI
           (a typical example would be availability filters, one does not filter out __available__ and __out of stock__ at the same time, it makes no sense)
        4. Single Valued - this is a flag saying whether or not a Resource may have two child specifying Resources at the same time
           (lets suppose that the availability filter is not standalone but it's bound so it would look like this: 
           availability=in-stock,out-of-stock, this is nonsensical just as the previous case )
        5. Parent Ids - Resource Specifiers are not globally applicable, they always have to be assigned to some parent entity like a main __Resource__ representing a __category__ for example.
           There is a change to make the filtering work at homepage too given some Resource gets coupled with __"/"__ root Route

 - __Paired Resource Specifier__ (DS)
    - It is a child data structure which parent is the Resource Specifier
        1. Inherits all from parent +
        2. Paired Value - this holds a string value like "200-300" paired to price filter Paired Resource Specifier 
        
        
        
        
IdentifierCollection for RoutesResolver has to be ordered, so the collection contains identifiers in the same order as they appear in the URL.
Inside the query string there is never a standalone identifier, if there is one,. such as ?availability=1 it always has to have the =1 value, that is how query string works no matter 
what SEO people say
The __position__ option should be allowed only for __argument_name__ routes, __controller__ routes do not need it and __argument__ value routes have to be positioned where their parent routes are.
Multi-domain support may be added outside of this Routing component. You just need to add another enum. table for domains and create a pivot table between Routes and Domains.