<?php

namespace Hazadam\Router\Validation;

use Hazadam\Router\Validation\Interfaces\SeparatorValidatorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class SeparatorValidatorTest
 * @package Hazadam\Router\Validation
 */
class SeparatorValidatorTest extends TestCase
{
    /**
     * @var SeparatorValidatorInterface
     */
    protected $validator;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->validator = new SeparatorValidator([
            'argument_name' => ['/', '&'],
            'argument_value' => ['=', ','],
            'pagination' => ['/']
        ]);
    }

    public function testValidateUnknownRoutableType()
    {
        $unknownRoutableType = 'dummy';
        $this->expectException(\InvalidArgumentException::class);
        $this->validator->validate($unknownRoutableType, '');
    }

    public function testNegativeValidation()
    {
        $type = 'pagination';
        $separator = '?';
        $isValid = $this->validator->validate($type, $separator);
        $this->assertSame(false, $isValid);
    }

    public function testPositiveValidation()
    {
        $type = 'pagination';
        $separator = '/';
        $isValid = $this->validator->validate($type, $separator);
        $this->assertSame(true, $isValid);
    }
}