<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

use PHPUnit\Framework\TestCase;

/**
 * Class StrongTypeAbstractCollectionTest
 * @package Hazadam\Router\Misc\DataType
 */
class StrongTypeAbstractCollectionTest extends TestCase
{
    const SCALAR_TYPES = [
        "boolean",
        "integer",
        "double",
        "string",
        "array",
    ];

    public function testInvalidScalarDataTypePassedIntoScalarCollectionException()
    {
        $dataType = "string";
        $wrongItem = 2;
        $collection = $this->givenStrongTypedCollection($dataType);
        $exception = $this->afterAddingItemOfWrongType($collection, $wrongItem);
        $this->anExceptionShouldBeRaised($exception);
    }

    public function testInvalidScalarDataTypePassedIntoObjectCollectionException()
    {
        $dataType = \stdClass::class;
        $wrongItem = 2;
        $collection = $this->givenStrongTypedCollectionSpy($dataType);
        $exception = $this->afterAddingItemOfWrongType($collection, $wrongItem);
        $this->anExceptionShouldBeRaised($exception);
    }

    public function testInvalidObjectDataTypePassedIntoObjectCollectionException()
    {
        $dataType = \stdClass::class;
        $wrongItem = new DummyClass();
        $collection = $this->givenStrongTypedCollection($dataType);
        $exception = $this->afterAddingItemOfWrongType($collection, $wrongItem);
        $this->anExceptionShouldBeRaised($exception);
    }

    public function testValidSubclassObjectPassedIntoObjectCollection()
    {
        $dataType = \stdClass::class;
        $validItem = new DummyStdClass();
        $collection = $this->givenStrongTypedCollection($dataType);
        $exception = $this->afterAddingItemOfWrongType($collection, $validItem);
        $this->noExceptionIsRaised($exception);
    }

    public function testValidImplementingObjectPassedIntoCollection()
    {
        $interface = I::class;
        $validItem = new Impl();
        $collection = $this->givenStrongTypedCollection($interface);
        $exception = $this->afterAddingItemOfWrongType($collection, $validItem);
        $this->noExceptionIsRaised($exception);
    }

    /**
     * @param $dataType
     * @return StrongTypeAbstractCollection|__anonymous@3393
     */
    protected function givenStrongTypedCollection($dataType)
    {
        $collection = new class($dataType) extends StrongTypeAbstractCollection
        {
            public function __construct(string $dataType)
            {
                parent::__construct($dataType);
            }
            protected function onAdd($lastKey, $item):void {}
            protected function onRemove($key, $get):void {}
        };
        return $collection;
    }

    /**
     * @param $collection
     * @param $item
     * @return \Exception|null
     */
    protected function afterAddingItemOfWrongType($collection, $item)
    {
        $exception = null;
        try {
            $collection->add($item);
        } catch (\Exception $exception) {
        }
        return $exception;
    }

    /**
     * @param $dataType
     * @return StrongTypeAbstractCollection|__anonymous@3957
     */
    protected function givenStrongTypedCollectionSpy($dataType)
    {
        $collection = new class($dataType) extends StrongTypeAbstractCollection
        {
            public $passedType;

            public function __construct(string $dataType)
            {
                $this->passedType = $dataType;
                parent::__construct($dataType);
            }
            protected function onAdd($lastKey, $item):void {}
            protected function onRemove($key, $get):void {}
        };
        return $collection;
    }

    /**
     * @param $exception
     */
    protected function anExceptionShouldBeRaised($exception): void
    {
        $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
    }

    /**
     * @param $exception
     */
    protected function noExceptionIsRaised($exception): void
    {
        $this->assertSame(null, $exception);
    }
}

class DummyStdClass extends \stdClass {}
class DummyClass {}
interface I {}
class Impl implements I {}