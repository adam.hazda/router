<?php

namespace Hazadam\Router\Misc\DataType;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class UniqueAbstractCollectionTest
 * @package Hazadam\Router\Misc\DataType
 */
class UniqueAbstractCollectionTest extends TestCase
{
    /**
     * @var CollectionInterface
     */
    protected $collection;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->collection = new class() extends UniqueAbstractCollection {
            protected function onAdd($key, $item):void {}
            protected function onRemove($key, $item):void {}
        };
    }


    public function testAddTwoSameScalars()
    {
        $this->afterAddingTwoSameScalarItems();
        $this->theCountShouldBeOnlyOne();
    }

    public function testAddTwoSameObjects()
    {
        $this->afterAddingTwoSameObjectItems();
        $this->theCountShouldBeOnlyOne();
    }

    protected function afterAddingTwoSameScalarItems(): void
    {
        $this->collection->add("string1", 2);
        $this->collection->add("string1", 3);
    }

    protected function theCountShouldBeOnlyOne(): void
    {
        $this->assertSame(1, count($this->collection));
    }

    protected function afterAddingTwoSameObjectItems(): void
    {
        $object = new \stdClass();
        $this->collection->add($object, $object);
        $this->collection->add($object, "asdf");
    }
}