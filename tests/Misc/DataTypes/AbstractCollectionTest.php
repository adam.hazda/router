<?php

namespace Hazadam\Router\Misc\DataType;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class AbstractCollectionTest
 * @package Hazadam\Router\Misc\DataType
 */
class AbstractCollectionTest extends TestCase
{
    /**
     * @var CollectionInterface
     */
    protected $collection;

    protected function setUp()
    {
        $this->collection = new class extends AbstractCollection {
            protected function onAdd($lastKey, $item):void {}
            protected function onRemove($key, $get):void {}
        };
    }

    public function testCountIsZeroWhenEmptyCollection()
    {
        $this->newCollectionIsEmptyAndReturnsNulls();
    }

    public function testAddOneItemNoKey()
    {
        $item = "string";
        $at = 0;
        $this->afterAddingOneItemToCollection($item);
        $this->countOfItemsShouldBe(1);
        $this->itemByGivenKeyShouldBe($at, $item);
    }

    public function testAddManyItemsNoKeys()
    {
        $this->collection->add("string1");
        $this->collection->add("string2");
        $this->collection->add("string3");
        $this->assertSame(3, count($this->collection));
        $this->assertSame("string1", $this->collection->get(0));
        $this->assertSame("string2", $this->collection->get(1));
        $this->assertSame("string3", $this->collection->get(2));
    }

    public function testAddOneItemByKey()
    {
        $this->collection->add("string", 1);
        $this->assertSame(1, count($this->collection));
        $this->assertSame("string", $this->collection->get(1));
    }

    public function testAddManyItemsByKeys()
    {
        $this->collection->add("string1", 1);
        $this->collection->add("string2", 2);
        $this->collection->add("string3", 3);
        $this->assertSame(3, count($this->collection));
        $this->assertSame("string1", $this->collection->get(1));
        $this->assertSame("string2", $this->collection->get(2));
        $this->assertSame("string3", $this->collection->get(3));
    }

    public function testAddManItemsSomeByKeysSomeNot()
    {
        $this->collection->add("string1");
        $this->collection->add("string2", 2);
        $this->collection->add("string3", 4);
        $this->assertSame(3, count($this->collection));
        $this->assertSame("string1", $this->collection->get(0));
        $this->assertSame("string2", $this->collection->get(2));
        $this->assertSame("string3", $this->collection->get(4));
    }

    public function testFloatKeys()
    {
        $this->collection->add("string1", 1.0);
        $this->collection->add("string2", 2.2);
        $this->assertSame(2, count($this->collection));
        $this->assertTrue($this->collection->hasKey(1.0));
        $this->assertSame("string1", $this->collection->get(1.0));
        $this->assertSame("string2", $this->collection->get(2.2));
    }

    public function testIntegerKeys()
    {
        $this->collection->add("string1", 1);
        $this->collection->add("string2", 2);
        $this->assertSame(2, count($this->collection));
        $this->assertTrue($this->collection->hasKey(1));
        $this->assertSame("string1", $this->collection->get(1));
        $this->assertSame("string2", $this->collection->get(2));
    }

    public function testStringKeys()
    {
        $this->collection->add("string1", "1");
        $this->collection->add("string2", "2");
        $this->assertSame(2, count($this->collection));
        $this->assertTrue($this->collection->hasKey("1"));
        $this->assertSame("string1", $this->collection->get("1"));
        $this->assertSame("string2", $this->collection->get("2"));
    }

    public function testObjectsAsKeys()
    {
        $object1 = new \stdClass();
        $object2 = new \stdClass();
        $this->collection->add("string1", $object1);
        $this->collection->add("string2", $object2);
        $this->assertSame(2, count($this->collection));
        $this->assertTrue($this->collection->hasKey($object1));
        $this->assertSame("string1", $this->collection->get($object1));
        $this->assertSame("string2", $this->collection->get($object2));
    }

    public function testInvalidBooleanKeys()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->collection->add("dummy", true);
    }

    public function testInvalidArrayKeys()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->collection->add("string1", ["array", "of", "things"]);
    }

    public function testStringKeysOverride()
    {
        $this->collection->add("string1", "1");
        $this->collection->add("string2", "1");
        $this->assertSame(1, count($this->collection));
        $this->assertSame("string2", $this->collection->get("1"));
    }

    public function testFloatKeysOverride()
    {
        // this is interesting because float is never equal to another float but whatever
        $this->collection->add("string1", 1.23);
        $this->collection->add("string2", 1.23);
        $this->assertSame(1, count($this->collection));
        $this->assertSame("string2", $this->collection->get(1.23));
    }

    public function testObjectKeysOverride()
    {
        $object1 = new \stdClass();
        $this->collection->add("string1", $object1);
        $this->collection->add("string2", $object1);
        $this->assertSame(1, count($this->collection));
        $this->assertSame("string2", $this->collection->get($object1));
    }

    public function testGetIterator()
    {
        $this->collection->add(1);
        $this->collection->add(2);
        $this->collection->add(3);
        $iterator = $this->collection->getIterator();
        $this->assertInstanceOf(\Iterator::class, $iterator);
        $expectedKey = 0;

        foreach ($iterator as $key => $item) {

            $this->assertSame($expectedKey, $key);
            $this->assertSame($expectedKey + 1, $item);
            $expectedKey++;
        }
    }

    public function testGetIteratorWithObjects()
    {
        $object = new \stdClass();
        $this->collection->add(1);
        $this->collection->add($object);
        $iterator = $this->collection->getIterator();
        $this->assertInstanceOf(\Iterator::class, $iterator);
        $expectedKey = 0;

        foreach ($iterator as $key => $item) {

            if ($expectedKey === 0) {

                $this->assertSame($expectedKey, $key);
                $this->assertSame($expectedKey + 1, $item);

            } elseif ($expectedKey === 1) {

                $this->assertSame($expectedKey, $key);
                $this->assertSame($object, $item);
            }
            $expectedKey++;
        }
    }

    public function testGetAll()
    {
        $this->collection->add(1);
        $this->collection->add(2);
        $this->collection->add(3);
        $all = $this->collection->getAll();
        $this->assertTrue(is_array($all));
        $expectedKey = 0;

        foreach ($all as $key => $item) {

            $this->assertSame($expectedKey, $key);
            $this->assertSame($expectedKey + 1, $item);
            $expectedKey++;
        }
    }

    public function testGetAllWithObjects()
    {
        $object = new \stdClass();
        $this->collection->add(1);
        $this->collection->add($object);
        $all = $this->collection->getAll();
        $this->assertTrue(is_array($all));
        $expectedKey = 0;

        foreach ($all as $key => $item) {

            if ($expectedKey === 0) {

                $this->assertSame($expectedKey, $key);
                $this->assertSame($expectedKey + 1, $item);

            } elseif ($expectedKey === 1) {

                $this->assertSame($expectedKey, $key);
                $this->assertSame($object, $item);
            }
            $expectedKey++;
        }
    }

    public function testHas()
    {
        $this->collection->add(1);
        $this->collection->add("string");
        $object = new \stdClass();
        $this->collection->add($object);
        $this->assertTrue($this->collection->has(1));
        $this->assertTrue($this->collection->has($object));
        $this->assertTrue($this->collection->has("string"));
    }

    public function testRemoveByKey()
    {
        $object = new \stdClass();
        $this->collection->add("string");
        $this->collection->add("string1", 1);
        $this->collection->add("string2", $object);
        $this->collection->removeByKey(0);
        $this->collection->removeByKey(1);
        $this->collection->removeByKey($object);
        $this->assertSame(0, count($this->collection));
        $this->assertFalse($this->collection->hasKey(1));
        $this->assertFalse($this->collection->hasKey(0));
        $this->assertFalse($this->collection->hasKey($object));
    }

    public function testRemoveByNullException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->collection->removeByKey(null);
    }

    public function testRemove()
    {
        $object = new \stdClass();
        $object1 = new Dummy();

        $this->collection->add(1);
        $this->collection->add(1);
        $this->collection->add("string2", 3);
        $this->collection->add("string2", 4);
        $this->collection->add("string3", 5);
        $this->collection->add($object);
        $this->collection->add($object, "random key");
        $this->collection->add($object1);

        $this->collection->remove(1);
        $this->collection->remove("string2");
        $this->collection->remove($object);

        $this->assertSame(2, count($this->collection));
        $this->assertFalse($this->collection->has(1));
        $this->assertFalse($this->collection->has("string2"));
        $this->assertTrue($this->collection->has("string3"));
        $this->assertTrue($this->collection->has($object1));
    }

    public function testGetObject()
    {
        $object = new \stdClass();
        $this->collection->add($object);
        $this->assertInstanceOf(\stdClass::class, $this->collection->get(0));
    }

    protected function newCollectionIsEmptyAndReturnsNulls(): void
    {
        $this->assertSame(0, count($this->collection));
        $this->assertSame(null, $this->collection->get(2));
    }

    /**
     * @param $item
     */
    protected function afterAddingOneItemToCollection($item): void
    {
        $this->collection->add($item);
    }

    /**
     * @param $count
     */
    protected function countOfItemsShouldBe($count): void
    {
        $this->assertSame($count, count($this->collection));
    }

    /**
     * @param $item
     * @param $at
     */
    protected function itemByGivenKeyShouldBe($at, $item): void
    {
        $this->assertSame($item, $this->collection->get($at));
    }
}

class Dummy extends \stdClass {}