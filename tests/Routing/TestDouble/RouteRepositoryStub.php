<?php

namespace Hazadam\Router\Routing\TestDouble;

use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Storage\Repository\Interfaces\RouteRepositoryInterface;

/**
 * Class RouteRepositoryStub
 * @package Hazadam\Router\Routing\TestDouble
 */
class RouteRepositoryStub implements RouteRepositoryInterface
{
    /**
     * @var RouteCollectionInterface|null
     */
    protected $fetchByIdentifiersRoutes;

    /**
     * @var OrderedRouteCollectionInterface|null
     */
    protected $fetchByResources;

    /**
     * @inheritDoc
     */
    public function fetchRoutesByIdentifiers(
        array $identifiers, string $httpMethod, $languageId, $domainId = null
    ): RouteCollectionInterface
    {
        return $this->fetchByIdentifiersRoutes;
    }

    /**
     * @inheritDoc
     */
    public function fetchRoutesByResources(
        ResourceInterface $rootResource, array $resourceSpecifiers, string $httpMethod, $languageId, $domainId = null
    ): OrderedRouteCollectionInterface
    {
        return $this->fetchByResources;
    }

    /**
     * @param RouteCollectionInterface|null $fetchByIdentifiersRoutes
     */
    public function setFetchByIdentifiersRoutes(?RouteCollectionInterface $fetchByIdentifiersRoutes): void
    {
        $this->fetchByIdentifiersRoutes = $fetchByIdentifiersRoutes;
    }

    /**
     * @param RouteCollectionInterface|null $fetchByResources
     */
    public function setFetchByResources(?RouteCollectionInterface $fetchByResources): void
    {
        $this->fetchByResources = $fetchByResources;
    }
}