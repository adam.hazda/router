<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver;

use Hazadam\Router\Net\Endpoint;
use Hazadam\Router\Net\Interfaces\PaginatedResourceInterface;
use Hazadam\Router\Net\Interfaces\PairedResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\ResourceSpecifier;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Identifier;
use Hazadam\Router\Net\Uri\IdentifierCollection;
use Hazadam\Router\Routing\Resolver\Interfaces\RoutesResolverInterface;
use Hazadam\Router\Validation\SeparatorValidator;
use PHPUnit\Framework\TestCase;

/**
 * Class RoutesResolverTest
 * @package Hazadam\Router\Routing\Resolver
 */
class RoutesResolverTest extends TestCase
{
    /**
     * @var RoutesResolverInterface
     */
    protected $resolver;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->resolver = new RoutesResolver(new SeparatorValidator(), new PaginationResolver());
    }

    public function testResolveFromEmptyInputs()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $routesToResolve = $this->createEmptyRouteCollection();
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();
        $this->assertCount(0, $result->getResolvedRoutes());
        $this->assertCount(0, $result->getUnrecognizedIdentifiers());
        $this->assertSame(null, $resourcesSet->getRootResource());
        $this->assertSame([], $resourcesSet->getResourceSpecifiers());
        $this->assertSame('', $result->getController());
    }

    public function testResolveNothingWhenRootRouteDoesNotMatchWithRootIdentifier()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $realRootIdentifier = new Identifier('/root', '');
        $differentRootIdentifier = new Identifier('/root-not-so-much', '');
        $identifiers->add($realRootIdentifier);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $routesToResolve = $this->createEmptyRouteCollection();
        $rootRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $differentRootIdentifier, $dummyController, $dummyResource, []
        );
        $routesToResolve->add($rootRoute);

        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(0, $resolvedRoutes);
        $this->assertFalse($resolvedRoutes->has($rootRoute));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($realRootIdentifier));
        $this->assertSame(null, $resourcesSet->getRootResource());
        $this->assertSame([], $resourcesSet->getResourceSpecifiers());
        $this->assertSame('', $result->getController());
    }

    public function testRootRouteWithQueryPositionException()
    {
        $exception = null;

        try {

            $identifiers = $this->createEmptyIdentifierCollection();
            $rootIdentifier = new Identifier('/root', '');
            $identifiers->add($rootIdentifier);
            $dummyController = 'dummy';
            $dummyResource = new Resource(23, 'dummy');
            $routesToResolve = $this->createEmptyRouteCollection();
            $rootRoute = new Route(
                23, RouteInterface::CONTROLLER, RouteInterface::POSITION_QUERY_STRING, $rootIdentifier, $dummyController, $dummyResource, []
            );
            $routesToResolve->add($rootRoute);
            $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testRootRouteWithoutResourceException()
    {
        $exception = null;

        try {

            $identifiers = $this->createEmptyIdentifierCollection();
            $rootIdentifier = new Identifier('/root', '');
            $identifiers->add($rootIdentifier);
            $routesToResolve = $this->createEmptyRouteCollection();
            $rootRoute = new Route(
                23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', null, []
            );
            $routesToResolve->add($rootRoute);
            $this->resolver->resolveRoutes($routesToResolve, $identifiers);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testRootRouteWithoutEndpointException()
    {
        $exception = null;

        try {

            $identifiers = $this->createEmptyIdentifierCollection();
            $rootIdentifier = new Identifier('/root', '');
            $identifiers->add($rootIdentifier);
            $dummyResource = new Resource(1, 'product');
            $routesToResolve = $this->createEmptyRouteCollection();
            $rootRoute = new Route(
                23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $dummyResource, []
            );
            $routesToResolve->add($rootRoute);
            $this->resolver->resolveRoutes($routesToResolve, $identifiers);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testMoreThanOneRouteForRootIdentifierException()
    {
        $exception = null;

        try {

            $identifiers = $this->createEmptyIdentifierCollection();
            $rootIdentifier = new Identifier('/root', '');
            $identifiers->add($rootIdentifier);
            $dummyController = 'dummy';
            $dummyResource = new Resource(23, 'dummy');
            $routesToResolve = $this->createEmptyRouteCollection();
            $rootRoute1 = new Route(
                23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, $dummyController, $dummyResource, []
            );
            $rootRoute2 = new Route(
                25, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, $dummyController, $dummyResource, []
            );
            $routesToResolve->add($rootRoute1);
            $routesToResolve->add($rootRoute2);
            $this->resolver->resolveRoutes($routesToResolve, $identifiers);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testResolveOneRootRoute()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $rootIdentifier = new Identifier('/root', '');
        $identifiers->add($rootIdentifier);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $routesToResolve = $this->createEmptyRouteCollection();
        $rootRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, $dummyController, $dummyResource, []
        );
        $routesToResolve->add($rootRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($rootRoute));
        $this->assertCount(0, $unrecognizedIdentifiers);
        $this->assertSame($dummyController, $result->getController());
        $this->assertSame($dummyResource, $resourcesSet->getRootResource());
    }

    public function testResolveFromManyRootIdentifiersAndRoutes()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $rootIdentifier1 = new Identifier('/landing-page/1/so-far-so-good/this-is-too-much', '');
        $rootIdentifier2 = new Identifier('/landing-page/1/so-far-so-good', '');
        $rootIdentifier3 = new Identifier('/landing-page/1', '');
        $rootIdentifier4= new Identifier('/landing-page', '');
        $identifiers->add($rootIdentifier1);
        $identifiers->add($rootIdentifier2);
        $identifiers->add($rootIdentifier3);
        $identifiers->add($rootIdentifier4);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $routesToResolve = $this->createEmptyRouteCollection();
        $validRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier2, $dummyController, $dummyResource, []
        );
        $invalidRoute1 = new Route(
            45, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier3, $dummyController, $dummyResource, []
        );
        $invalidRoute2 = new Route(
            75, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier4, $dummyController, $dummyResource, []
        );
        $routesToResolve->add($validRoute);
        $routesToResolve->add($invalidRoute1);
        $routesToResolve->add($invalidRoute2);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($validRoute));
        $this->assertCount(3, $unrecognizedIdentifiers);
        $this->assertFalse($unrecognizedIdentifiers->has($rootIdentifier2));
        $this->assertSame($dummyResource, $resourcesSet->getRootResource());
        $this->assertSame([], $resourcesSet->getResourceSpecifiers());
        $this->assertSame($dummyController, $result->getController());
    }

    public function testResolveMoreThanOneResourceSpecifierForNonRootRouteException()
    {
        $exception = null;

        try {

            $identifiers = $this->createEmptyIdentifierCollection();
            $categoryIdentifier = new Identifier('/category', '');
            $availabilityIdentifier = new Identifier('available', '/');
            $identifiers->add($categoryIdentifier);
            $identifiers->add($availabilityIdentifier);
            $dummyController = 'dummy';
            $categoryResource = new Resource(23, 'dummy');
            $availabilityResourceSpecifier1 = new ResourceSpecifier(
                23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
            );
            $availabilityResourceSpecifier2 = new ResourceSpecifier(
                25, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
            );
            $routesToResolve = $this->createEmptyRouteCollection();
            $categoryRoute = new Route(
                23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
            );
            $availabilityRoute = new Route(
                24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier1, $availabilityResourceSpecifier2]
            );
            $routesToResolve->add($categoryRoute);
            $routesToResolve->add($availabilityRoute);
            $this->resolver->resolveRoutes($routesToResolve, $identifiers);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testResolveTwoRoutesWithValidResourceBinding()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $availabilityIdentifier = new Identifier('available', '/');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($availabilityIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($availabilityRoute));
        $this->assertCount(0, $unrecognizedIdentifiers);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
        $this->assertEquals([$availabilityResourceSpecifier], array_values($resourcesSet->getResourceSpecifiers()));
        $this->assertSame($dummyController, $result->getController());
    }

    public function testResolveTwoRoutesWithInvalidResourceBinding()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $availabilityIdentifier = new Identifier('available', '/');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($availabilityIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [100], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertFalse($resolvedRoutes->has($availabilityRoute));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($availabilityIdentifier));
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
        $this->assertEquals([], array_values($resourcesSet->getResourceSpecifiers()));
        $this->assertSame($dummyController, $result->getController());
    }

    public function testIgnoreResourceSpecifierOfTypeBoundWithoutChildren()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $colorIdentifier = new Identifier('color', '/');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($colorIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $colorResourceSpecifier = new ResourceSpecifier(
            23, 'color', ResourceSpecifierInterface::TYPE_BOUND, false, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $colorRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [$colorResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($colorRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection(); 
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertFalse($resolvedRoutes->has($colorRoute));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($colorIdentifier));
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
        $this->assertEquals([], array_values($resourcesSet->getResourceSpecifiers()));
        $this->assertSame($dummyController, $result->getController());
    }

    public function testIgnoreRootLandingPageAlsoContainingChildResourceSpecifierForRootResource()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $rootIdentifier1 = new Identifier('/landing-page/1/so-far-so-good/this-is-too-much', '');
        $rootIdentifier2 = new Identifier('/landing-page/1/so-far-so-good', '');
        $rootIdentifier3 = new Identifier('/landing-page/1', '');
        $rootIdentifier4 = new Identifier('/landing-page', '');
        $availabilityIdentifier = new Identifier('available', '/');
        $identifiers->add($rootIdentifier1);
        $identifiers->add($rootIdentifier2);
        $identifiers->add($rootIdentifier3);
        $identifiers->add($rootIdentifier4);
        $identifiers->add($availabilityIdentifier);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            123, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $validRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier2, $dummyController, $dummyResource, []
        );
        $invalidRoute1 = new Route(
            45, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier3, $dummyController, $dummyResource, [$availabilityResourceSpecifier]
        );
        $invalidRoute2 = new Route(
            75, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier4, $dummyController, $dummyResource, []
        );
        $availabilityRoute = new Route(
            123, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $routesToResolve->add($validRoute);
        $routesToResolve->add($invalidRoute1);
        $routesToResolve->add($invalidRoute2);
        $routesToResolve->add($availabilityRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($validRoute));
        $this->assertTrue($resolvedRoutes->has($availabilityRoute));
        $this->assertCount(3, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier1));
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier3));
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier4));
        $this->assertSame($dummyController, $result->getController());
        $this->assertSame($dummyResource, $resourcesSet->getRootResource());
        $this->assertCount(2, $resourceCollection);

    }

    public function testIgnoreRouteWhenItsResourceSpecifierIsAlreadyContainedWithinRootRoute()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $rootIdentifier1 = new Identifier('/landing-page/1/so-far-so-good/this-is-too-much', '');
        $rootIdentifier2 = new Identifier('/landing-page/1/so-far-so-good', '');
        $rootIdentifier3 = new Identifier('/landing-page/1', '');
        $rootIdentifier4 = new Identifier('/landing-page', '');
        $colorIdentifier = new Identifier('color', '/');
        $blueIdentifier = new Identifier('blue', '=');
        $identifiers->add($rootIdentifier1);
        $identifiers->add($rootIdentifier2);
        $identifiers->add($rootIdentifier3);
        $identifiers->add($rootIdentifier4);
        $identifiers->add($colorIdentifier);
        $identifiers->add($blueIdentifier);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $colorResourceSpecifier = new ResourceSpecifier(
            123, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [23], []
        );
        $blueResourceSpecifier = new ResourceSpecifier(
            124, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [123]
        );
        $redResourceSpecifier = new ResourceSpecifier(
            125, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [123]
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $validRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier2, $dummyController, $dummyResource, [$colorResourceSpecifier, $redResourceSpecifier]
        );
        $invalidRoute1 = new Route(
            45, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier3, $dummyController, $dummyResource, []
        );
        $invalidRoute2 = new Route(
            75, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier4, $dummyController, $dummyResource, []
        );
        $colorRoute = new Route(
            123, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [$colorResourceSpecifier]
        );
        $blueRoute = new Route(
            124, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [$blueResourceSpecifier]
        );
        $routesToResolve->add($validRoute);
        $routesToResolve->add($invalidRoute1);
        $routesToResolve->add($invalidRoute2);
        $routesToResolve->add($colorRoute);
        $routesToResolve->add($blueRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($validRoute));
        $this->assertFalse($resolvedRoutes->has($colorRoute));
        $this->assertTrue($resolvedRoutes->has($blueRoute));

        $this->assertCount(4, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier1));
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier3));
        $this->assertTrue($unrecognizedIdentifiers->has($rootIdentifier4));
        $this->assertTrue($unrecognizedIdentifiers->has($colorIdentifier));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(4, $resourceCollection);
        $this->assertSame($dummyResource, $resourcesSet->getRootResource());
    }

    public function testIgnoreRouteWithInvalidSeparator()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $availabilityIdentifier = new Identifier('available', ',');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($availabilityIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertFalse($resolvedRoutes->has($availabilityRoute));

        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($availabilityIdentifier));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(1, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testChooseTheRightIdentifierIfFree()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $invalidAvailableIdentifier = new Identifier('available', ',');
        $validAvailableIdentifier = new Identifier('available', '/');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($validAvailableIdentifier);
        $identifiers->add($invalidAvailableIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $invalidAvailableIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($availabilityRoute));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($invalidAvailableIdentifier));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(2, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testSingleOccurrenceFlagSet()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $availabilityIdentifier1 = new Identifier('available', '/');
        $availabilityIdentifier2 = new Identifier('out-of-stock', '/');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($availabilityIdentifier1);
        $identifiers->add($availabilityIdentifier2);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier1 = new ResourceSpecifier(
            23, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, true, false, [23], []
        );
        $availabilityResourceSpecifier2 = new ResourceSpecifier(
            25, 'availability', ResourceSpecifierInterface::TYPE_STANDALONE, true, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute1 = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier1, '', null, [$availabilityResourceSpecifier1]
        );
        $availabilityRoute2 = new Route(
            25, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier2, '', null, [$availabilityResourceSpecifier2]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute1);
        $routesToResolve->add($availabilityRoute2);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($availabilityRoute1));
        $this->assertFalse($resolvedRoutes->has($availabilityRoute2));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($availabilityIdentifier2));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(2, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testSingleValuedFlagSet()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $availabilityIdentifier = new Identifier('availability', '/');
        $availableIdentifier = new Identifier('available', '=');
        $outOfStockIdentifier = new Identifier('out-of-stock', ',');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($availabilityIdentifier);
        $identifiers->add($availableIdentifier);
        $identifiers->add($outOfStockIdentifier);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            23, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, true, true, [23], []
        );
        $availableResourceSpecifier = new ResourceSpecifier(
            25, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [23]
        );
        $outOfStockResourceSpecifier = new ResourceSpecifier(
            26, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [23]
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $availabilityRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]
        );
        $availableRoute = new Route(
            25, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $availableIdentifier, '', null, [$availableResourceSpecifier]
        );
        $outOfStockRoute = new Route(
            26, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $outOfStockIdentifier, '', null, [$outOfStockResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($availabilityRoute);
        $routesToResolve->add($availableRoute);
        $routesToResolve->add($outOfStockRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(3, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($availabilityRoute));
        $this->assertTrue($resolvedRoutes->has($availableRoute));
        $this->assertFalse($resolvedRoutes->has($outOfStockRoute));
        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($outOfStockIdentifier));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(3, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testResolvePairedResourceSpecifier()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $priceRangeIdentifier = new Identifier('price-range', '/');
        $priceIdentifier1 = new Identifier('200-300', '=');
        $priceIdentifier2 = new Identifier('2999', ',');
        $priceIdentifier3 = new Identifier('3002', 'invalid-separator');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($priceRangeIdentifier);
        $identifiers->add($priceIdentifier1);
        $identifiers->add($priceIdentifier2);
        $identifiers->add($priceIdentifier3);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $priceRangeResourceSpecifier = new ResourceSpecifier(
            23, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, true, false, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $priceRangeRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $priceRangeIdentifier, '', null, [$priceRangeResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($priceRangeRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($priceRangeRoute));

        $specifiers = $priceRangeRoute->getResourceSpecifiers();
        /** @var PairedResourceSpecifierInterface $priceRangeResourceSpecifier */
        $priceRangeResourceSpecifier = end($specifiers);

        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $priceRangeResourceSpecifier);
        $this->assertEquals(['200-300', '2999'], $priceRangeResourceSpecifier->getPairedValues());

        $this->assertCount(1, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($priceIdentifier3));
        $this->assertSame($dummyController, $result->getController());

        $this->assertCount(2, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
        $children = $resourceCollection->getChildren($categoryResource);
        $child = end($children);
        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $child);
    }

    public function testResolveSingleValuedPairedResourceSpecifier()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $priceRangeIdentifier = new Identifier('price-range', '/');
        $priceIdentifier1 = new Identifier('200-300', '=');
        $priceIdentifier2 = new Identifier('2999', ',');
        $priceIdentifier3 = new Identifier('3002', 'invalid-separator');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($priceRangeIdentifier);
        $identifiers->add($priceIdentifier1);
        $identifiers->add($priceIdentifier2);
        $identifiers->add($priceIdentifier3);
        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $priceRangeResourceSpecifier = new ResourceSpecifier(
            23, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, true, true, [23], []
        );
        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $priceRangeRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $priceRangeIdentifier, '', null, [$priceRangeResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($priceRangeRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(2, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($priceRangeRoute));

        $specifiers = $priceRangeRoute->getResourceSpecifiers();
        /** @var PairedResourceSpecifierInterface $priceRangeResourceSpecifier */
        $priceRangeResourceSpecifier = end($specifiers);

        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $priceRangeResourceSpecifier);
        $this->assertEquals(['200-300'], $priceRangeResourceSpecifier->getPairedValues());

        $this->assertCount(2, $unrecognizedIdentifiers);
        $this->assertTrue($unrecognizedIdentifiers->has($priceIdentifier2));
        $this->assertTrue($unrecognizedIdentifiers->has($priceIdentifier3));
        $this->assertSame($dummyController, $result->getController());
        $this->assertCount(2, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
        $children = $resourceCollection->getChildren($categoryResource);
        $child = end($children);
        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $child);
    }

    public function testResolveAmbiguousIdentifiers()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $colorIdentifier = new Identifier('color', '/');
        $valueIdentifier1 = new Identifier('red', '=');
        $valueIdentifier2 = new Identifier('blue', ',');
        $shadeIdentifier = new Identifier('shade', '/');
        $valueIdentifier3 = new Identifier('red', '=');
        $valueIdentifier4 = new Identifier('blue', ',');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($colorIdentifier);
        $identifiers->add($valueIdentifier1);
        $identifiers->add($valueIdentifier2);
        $identifiers->add($shadeIdentifier);
        $identifiers->add($valueIdentifier3);
        $identifiers->add($valueIdentifier4);

        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $colorResourceSpecifier = new ResourceSpecifier(
            23, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, true, false, [23], []
        );
        $blueResourceSpecifier = new ResourceSpecifier(
            24, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [23]
        );
        $redResourceSpecifier = new ResourceSpecifier(
            25, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [23]
        );
        $shadeResourceSpecifier = new ResourceSpecifier(
            26, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, false, false, [23], []
        );

        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $colorRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [$colorResourceSpecifier]
        );
        $blueRoute = new Route(
            25, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $valueIdentifier1, '', null, [$blueResourceSpecifier]
        );
        $redRoute = new Route(
            26, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $valueIdentifier2, '', null, [$redResourceSpecifier]
        );
        $shadeRoute = new Route(
            27, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $shadeIdentifier, '', null, [$shadeResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($colorRoute);
        $routesToResolve->add($blueRoute);
        $routesToResolve->add($redRoute);
        $routesToResolve->add($shadeRoute);

        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(5, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($colorRoute));
        $this->assertTrue($resolvedRoutes->has($blueRoute));
        $this->assertTrue($resolvedRoutes->has($redRoute));
        $this->assertTrue($resolvedRoutes->has($shadeRoute));

        $specifiers = $shadeRoute->getResourceSpecifiers();
        /** @var PairedResourceSpecifierInterface $shadePairedResourceSpecifier */
        $shadePairedResourceSpecifier = end($specifiers);

        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $shadePairedResourceSpecifier);
        $this->assertEquals(['red', 'blue'], $shadePairedResourceSpecifier->getPairedValues());

        $this->assertCount(0, $unrecognizedIdentifiers);
        $this->assertSame($dummyController, $result->getController());

        $this->assertCount(5, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testResolveDuplicateIdentifiersButDifferentRouteType()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $categoryIdentifier = new Identifier('/category', '');
        $criteriaIdentifier = new Identifier('criteria', '/');
        $criteriaValueIdentifier = new Identifier('shade', '=');
        $shadeIdentifier = new Identifier('shade', '/');
        $blueIdentifier = new Identifier('red', '=');
        $redIdentifier = new Identifier('blue', ',');
        $identifiers->add($categoryIdentifier);
        $identifiers->add($criteriaIdentifier);
        $identifiers->add($criteriaValueIdentifier);
        $identifiers->add($shadeIdentifier);
        $identifiers->add($blueIdentifier);
        $identifiers->add($redIdentifier);

        $dummyController = 'dummy';
        $categoryResource = new Resource(23, 'dummy');
        $criteriaResourceSpecifier = new ResourceSpecifier(
            23, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, true, false, [23], []
        );
        $criteriaValueResourceSpecifier = new ResourceSpecifier(
            24, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [23]
        );
        $shadeResourceSpecifier = new ResourceSpecifier(
            26, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, false, false, [23], []
        );

        $routesToResolve = $this->createEmptyRouteCollection();
        $categoryRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $categoryIdentifier, $dummyController, $categoryResource, []
        );
        $criteriaRoute = new Route(
            24, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $criteriaIdentifier, '', null, [$criteriaResourceSpecifier]
        );
        $criteriaValueRoute = new Route(
            25, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $criteriaValueIdentifier, '', null, [$criteriaValueResourceSpecifier]
        );
        $shadeRoute = new Route(
            27, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $shadeIdentifier, '', null, [$shadeResourceSpecifier]
        );
        $routesToResolve->add($categoryRoute);
        $routesToResolve->add($criteriaRoute);
        $routesToResolve->add($criteriaValueRoute);
        $routesToResolve->add($shadeRoute);

        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(4, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($categoryRoute));
        $this->assertTrue($resolvedRoutes->has($criteriaRoute));
        $this->assertTrue($resolvedRoutes->has($criteriaValueRoute));
        $this->assertTrue($resolvedRoutes->has($shadeRoute));

        $specifiers = $shadeRoute->getResourceSpecifiers();
        /** @var PairedResourceSpecifierInterface $shadePairedResourceSpecifier */
        $shadePairedResourceSpecifier = end($specifiers);

        $this->assertInstanceOf(PairedResourceSpecifierInterface::class, $shadePairedResourceSpecifier);
        $this->assertEquals(['red', 'blue'], $shadePairedResourceSpecifier->getPairedValues());

        $this->assertCount(0, $unrecognizedIdentifiers);
        $this->assertSame($dummyController, $result->getController());

        $this->assertCount(4, $resourceCollection);
        $this->assertSame($categoryResource, $resourcesSet->getRootResource());
    }

    public function testResolvePagination()
    {
        $identifiers = $this->createEmptyIdentifierCollection();
        $rootIdentifier = new Identifier('/root', '');
        $pageIdentifier = new Identifier('2', '/');
        $identifiers->add($rootIdentifier);
        $identifiers->add($pageIdentifier);
        $dummyController = 'dummy';
        $dummyResource = new Resource(23, 'dummy');
        $routesToResolve = $this->createEmptyRouteCollection();
        $rootRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, $dummyController, $dummyResource, []
        );
        $routesToResolve->add($rootRoute);
        $result = $this->resolver->resolveRoutes($routesToResolve, $identifiers);
        $resolvedRoutes = $result->getResolvedRoutes();
        $unrecognizedIdentifiers = $result->getUnrecognizedIdentifiers();
        $resourceCollection = $result->getResourceCollection();
        $resourcesSet = $resourceCollection->toResourceSet();

        $this->assertCount(1, $resolvedRoutes);
        $this->assertTrue($resolvedRoutes->has($rootRoute));
        $this->assertCount(0, $unrecognizedIdentifiers);
        $this->assertSame($dummyController, $result->getController());
        /** @var PaginatedResourceInterface $rootResource */
        $rootResource = $resourcesSet->getRootResource();
        $this->assertInstanceOf(PaginatedResourceInterface::class, $rootResource);
        $this->assertSame(2, $rootResource->getPage());
    }

    /**
     * @return IdentifierCollection
     */
    protected function createEmptyIdentifierCollection(): IdentifierCollection
    {
        $identifiers = new IdentifierCollection();

        return $identifiers;
    }

    /**
     * @return RouteCollection
     */
    protected function createEmptyRouteCollection(): RouteCollection
    {
        $routesToResolve = new RouteCollection();

        return $routesToResolve;
    }
}