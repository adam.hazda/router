<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver;

use Hazadam\Router\Net\Uri\Identifier;
use Hazadam\Router\Net\Uri\IdentifierCollection;
use Hazadam\Router\Net\Uri\UrlBuilder;
use Hazadam\Router\Routing\Resolver\Interfaces\PaginationResolverInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class PaginationResolverTest
 * @package Hazadam\Router\Routing\Resolver
 */
class PaginationResolverTest extends TestCase
{
    /**
     * @var PaginationResolverInterface
     */
    protected $resolver;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->resolver = new PaginationResolver();
    }

    public function testResolveFromEmptyCollection()
    {
        $identifiers = new IdentifierCollection();
        $page = $this->resolver->resolvePagination($identifiers);
        $this->assertSame(null, $page);
    }

    public function testResolve()
    {
        $identifiers = new IdentifierCollection();
        $identifiers->add(new Identifier("color", "/"));
        $identifiers->add(new Identifier("blue", "="));
        $paginationIdentifier = new Identifier("210", "/");
        $identifiers->add($paginationIdentifier);
        $page = $this->resolver->resolvePagination($identifiers);
        $this->assertSame(210, $page);
        $this->assertFalse($identifiers->has($paginationIdentifier));
    }
}