<?php declare(strict_types=1);

namespace Hazadam\Router\Routing;

use Hazadam\Router\Language\Resolving\TldResolver;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRoute;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\PaginationUrlDecorator;
use Hazadam\Router\Net\Uri\Generator\PathGenerator;
use Hazadam\Router\Net\Uri\Generator\QueryStringGenerator;
use Hazadam\Router\Net\Uri\Generator\ResourceUrlGenerator;
use Hazadam\Router\Net\Uri\Generator\RootRouteResolver;
use Hazadam\Router\Net\Uri\Identifier;
use Hazadam\Router\Net\Uri\Resolving\IdentifierResolver;
use Hazadam\Router\Net\Uri\UrlFactoryTrait;
use Hazadam\Router\Routing\Interfaces\RouterInterface;
use Hazadam\Router\Routing\Resolver\PaginationResolver;
use Hazadam\Router\Routing\Resolver\RoutesResolver;
use Hazadam\Router\Routing\TestDouble\RouteRepositoryStub;
use Hazadam\Router\Validation\SeparatorValidator;
use PHPUnit\Framework\TestCase;

/**
 * Class RouterTest
 * @package Hazadam\Router\Routing
 */
class RouterTest extends TestCase
{
    use UrlFactoryTrait;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var RouteRepositoryStub
     */
    protected $routeRepositoryStub;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->createRouteRepository();
        $languageResolver = new TldResolver([1 => 'cs'], 1);
        $identifierResolver = new IdentifierResolver();
        $routesResolver = new RoutesResolver(new SeparatorValidator(), new PaginationResolver());
        $resourceUrlGenerator = new ResourceUrlGenerator(new RootRouteResolver(),
            new PathGenerator(), new QueryStringGenerator(), new PaginationUrlDecorator()
        );
        $this->router = new Router(
            $languageResolver, $identifierResolver, $this->routeRepositoryStub, $routesResolver,
            $resourceUrlGenerator
        );
        $this->interpretationClosure = null;
    }

    public function testMatchEmptyUrl()
    {
        $emptyUrl = $this->buildUrlFromString('');

        $recognizedRoutes = new RouteCollection();
        $routesByResources = new RouteCollection();

        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET');
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame(null, $routingResult->getGeneratedUrl());
        $this->assertSame('', $resolvingResult->getController());
        $this->assertCount(0, $resolvingResult->getResolvedRoutes());
        $this->assertCount(0, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(0, $resolvingResult->getResourceCollection());
    }

    public function testNonResourceIdentifierNotFound()
    {
        $emptyUrl = $this->createUrlFromPath('/registration');
        $recognizedRoutes = new RouteCollection();
        $routesByResources = new RouteCollection();
        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET');
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame('/registration', $routingResult->getRequestedUrl()->getPath());
        $this->assertSame(null, $routingResult->getGeneratedUrl());
        $this->assertSame('', $resolvingResult->getController());
        $this->assertCount(0, $resolvingResult->getResolvedRoutes());
        $this->assertCount(3, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(0, $resolvingResult->getResourceCollection());
    }

    public function testMatchNonResourceUrl()
    {
        $emptyUrl = $this->createUrlFromPath('/registration');

        $registrationIdentifier = new Identifier('/registration', '');
        $registrationController = 'user::register';
        $registrationRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $registrationIdentifier, $registrationController, null, []
        );
        $recognizedRoutes = new RouteCollection();
        $recognizedRoutes->add($registrationRoute);
        $routesByResources = new RouteCollection();
        $routesByResources->add($registrationRoute);

        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET', $this);
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame(null, $routingResult->getGeneratedUrl());
        $this->assertSame($registrationController, $resolvingResult->getController());
        $this->assertCount(1, $resolvingResult->getResolvedRoutes());
        $this->assertCount(2, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(0, $resolvingResult->getResourceCollection());
    }

    public function testMatchSimpleResourceCorrectUrl()
    {
        $emptyUrl = $this->createUrlFromPath('/product');

        $productIdentifier = new Identifier('/product', '');
        $productController = 'product::index';
        $productResource = new Resource(1, 'product');
        $productRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $productIdentifier, $productController, $productResource, []
        );
        $orderedProductRoute = new OrderedRoute($productRoute, []);
        $recognizedRoutes = new RouteCollection();
        $recognizedRoutes->add($productRoute);
        $routesByResources = new OrderedRouteCollection();
        $routesByResources->add($orderedProductRoute);

        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET', $this);
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame('/product', $routingResult->getGeneratedUrl()->getPath());
        $this->assertSame($productController, $resolvingResult->getController());
        $this->assertCount(1, $resolvingResult->getResolvedRoutes());
        $this->assertCount(2, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(1, $resolvingResult->getResourceCollection());
    }

    public function testMatchSimpleResourceIncorrectUrl()
    {
        $emptyUrl = $this->createUrlFromPath('/product/rubbish');

        $productIdentifier = new Identifier('/product', '');
        $productController = 'product::index';
        $productResource = new Resource(1, 'product');
        $productRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $productIdentifier, $productController, $productResource, []
        );
        $productRoute = new OrderedRoute($productRoute, []);
        $recognizedRoutes = new RouteCollection();
        $recognizedRoutes->add($productRoute);
        $routesByResources = new OrderedRouteCollection();
        $routesByResources->add($productRoute);

        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET', $this);
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame('/product', $routingResult->getGeneratedUrl()->getPath());
        $this->assertSame($productController, $resolvingResult->getController());
        $this->assertCount(1, $resolvingResult->getResolvedRoutes());
        $this->assertCount(4, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(1, $resolvingResult->getResourceCollection());
    }


    public function testMatchSimplePaginatedResourceUrl()
    {
        $emptyUrl = $this->createUrlFromPath('/product/2/rubbish/4');

        $productIdentifier = new Identifier('/product', '');
        $productController = 'product::index';
        $productResource = new Resource(1, 'product');
        $productRoute = new Route(
            23, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $productIdentifier, $productController, $productResource, []
        );
        $productRoute = new OrderedRoute($productRoute, []);
        $recognizedRoutes = new RouteCollection();
        $recognizedRoutes->add($productRoute);
        $routesByResources = new OrderedRouteCollection();
        $routesByResources->add($productRoute);

        $this->routeRepositoryStub->setFetchByIdentifiersRoutes($recognizedRoutes);
        $this->routeRepositoryStub->setFetchByResources($routesByResources);

        $routingResult = $this->router->match($emptyUrl, 'GET', $this);
        $resolvingResult = $routingResult->getResolvingResult();

        $this->assertSame('/product/2', $routingResult->getGeneratedUrl()->getPath());
        $this->assertSame($productController, $resolvingResult->getController());
        $this->assertCount(1, $resolvingResult->getResolvedRoutes());
        $this->assertCount(7, $resolvingResult->getUnrecognizedIdentifiers());
        $this->assertCount(1, $resolvingResult->getResourceCollection());
    }

    protected function createRouteRepository(): void
    {
        $this->routeRepositoryStub = new RouteRepositoryStub();
    }
}