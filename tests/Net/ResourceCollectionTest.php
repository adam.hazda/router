<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class ResourceCollectionTest
 * @package Hazadam\Router\Net
 */
class ResourceCollectionTest extends TestCase
{
    /**
     * @var ResourceCollectionInterface
     */
    protected $collection;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->collection = new ResourceCollection();
    }

    public function testEmptyCollection()
    {
        $resourceSet = $this->collection->toResourceSet();
        $this->assertSame(null, $resourceSet->getRootResource());
        $this->assertSame([], $resourceSet->getResourceSpecifiers());
        $this->assertCount(0, $this->collection);
        $this->assertFalse($this->collection->hasResourceSpecifierId(2));
    }

    public function testAddRootResource()
    {
        $rootResource = new Resource(1, '');
        $this->collection->add($rootResource);
        $resourceSet = $this->collection->toResourceSet();
        $this->assertSame($rootResource, $resourceSet->getRootResource());
        $this->assertSame([], $resourceSet->getResourceSpecifiers());
        $this->assertCount(1, $this->collection);
        $this->assertFalse($this->collection->hasResourceSpecifierId(1));
    }

    public function testAddResourceSpecifier()
    {
        $resourceSpecifier = new ResourceSpecifier(1, '', '', false, false, [], []);
        $this->collection->add($resourceSpecifier);
        $resourceSet = $this->collection->toResourceSet();
        $this->assertSame(null, $resourceSet->getRootResource());
        $this->assertSame([$resourceSpecifier->getId() => $resourceSpecifier], $resourceSet->getResourceSpecifiers());
        $this->assertCount(1, $this->collection);
        $this->assertTrue($this->collection->hasResourceSpecifierId(1));
    }

    public function testRemoveAll()
    {
        $rootResource = new Resource(1, '');
        $resourceSpecifier = new ResourceSpecifier(1, '', '', false, false, [], []);
        $this->collection->add($rootResource);
        $this->collection->add($resourceSpecifier);
        $this->collection->remove($rootResource);
        $this->collection->remove($resourceSpecifier);
        $resourceSet = $this->collection->toResourceSet();
        $this->assertSame(null, $resourceSet->getRootResource());
        $this->assertSame([], $resourceSet->getResourceSpecifiers());
        $this->assertCount(0, $this->collection);
        $this->assertFalse($this->collection->hasResourceSpecifierId(1));
    }

    public function testSimpleTree()
    {
        $rootResource = new Resource(54, '');
        $resourceSpecifier1 = new ResourceSpecifier(34, '', '', false, false, [54], []);
        $resourceSpecifier2 = new ResourceSpecifier(35, '', '', false, false, [54], []);
        $resourceSpecifier3 = new ResourceSpecifier(55, '', '', false, false, [], [34]);
        $resourceSpecifier4 = new ResourceSpecifier(65, '', '', false, false, [], [35]);
        $resourceSpecifier5 = new ResourceSpecifier(25, '', '', false, false, [], [34]);
        $resourceSpecifier6 = new ResourceSpecifier(26, '', '', false, false, [], [35]);
        $this->collection->add($rootResource);
        $this->collection->add($resourceSpecifier1);
        $this->collection->add($resourceSpecifier2);
        $this->collection->add($resourceSpecifier3);
        $this->collection->add($resourceSpecifier4);
        $this->collection->add($resourceSpecifier5);
        $this->collection->add($resourceSpecifier6);
        $this->collection->remove($resourceSpecifier4);
        $resourceSet = $this->collection->toResourceSet();
        $this->assertSame($rootResource, $resourceSet->getRootResource());
        $this->assertEquals([
                $resourceSpecifier1->getId() => $resourceSpecifier1,
                $resourceSpecifier2->getId() => $resourceSpecifier2,
                $resourceSpecifier3->getId() => $resourceSpecifier3,
                $resourceSpecifier5->getId() => $resourceSpecifier5,
                $resourceSpecifier6->getId() => $resourceSpecifier6,
        ], $resourceSet->getResourceSpecifiers());
        $this->assertCount(6, $this->collection);
        $this->assertEquals([
            $resourceSpecifier1, $resourceSpecifier2
        ], array_values($this->collection->getChildren($rootResource)));
        $this->assertEquals([
            $resourceSpecifier3, $resourceSpecifier5
        ], array_values($this->collection->getChildren($resourceSpecifier1)));
        $this->assertEquals([
            $resourceSpecifier6
        ], array_values($this->collection->getChildren($resourceSpecifier2)));
        $this->assertFalse($this->collection->hasResourceSpecifierId(65));
        $this->assertTrue($this->collection->hasResourceSpecifierId(35));
        $this->assertTrue($this->collection->hasResourceSpecifierId(34));
        $this->assertTrue($this->collection->hasResourceSpecifierId(55));
        $this->assertTrue($this->collection->hasResourceSpecifierId(25));
        $this->assertTrue($this->collection->hasResourceSpecifierId(26));
    }
}
