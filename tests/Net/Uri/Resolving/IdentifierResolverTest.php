<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Resolving;

use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;
use Hazadam\Router\Net\Uri\Resolving\Interfaces\IdentifierResolverInterface;
use Hazadam\Router\Net\Uri\UrlBuilder;
use PHPUnit\Framework\TestCase;

/**
 * Class IdentifierResolverTest
 * @package Hazadam\Router\Net\Uri\Resolving
 */
class IdentifierResolverTest extends TestCase
{
    /**
     * @var IdentifierResolverInterface
     */
    protected $resolver;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->resolver = new IdentifierResolver();
    }

    public function testResolveFromNoPath()
    {
        $url = 'https://www.eshop.com';
        $url = (new UrlBuilder())->buildFromString($url);
        $identifiers = $this->resolver->resolveIdentifiers($url);
        $this->assertCount(0, $identifiers);
    }

    public function testResolveFromEmptyUrl()
    {
        $url = 'https://www.eshop.com/?#';
        $url = (new UrlBuilder())->buildFromString($url);
        $identifiers = $this->resolver->resolveIdentifiers($url);
        $this->assertSame('/', $identifiers->get(0)->getString());
        $this->assertCount(1, $identifiers);
    }

    public function testIgnoreMultiDimensionalArrayParameter()
    {
        $url = 'https://www.eshop.com?color=red&param[key1][key2]=value1&param[key1][key3]=value2&param[key4]=value2';
        $url = (new UrlBuilder())->buildFromString($url);
        $identifiers = $this->resolver->resolveIdentifiers($url);

        $expectedStrings = [
            '?color=red&param[key1][key2]=value1&param[key1][key3]=value2&param[key4]=value2',
            '?color=red&param[key1][key2]=value1&param[key1][key3]=value2',
            '?color=red&param[key1][key2]=value1',
            '?color=red',
            'color', 'red', 'param', 'value2',
        ];
        $expectedSeparators = [
            '', '', '', '',
            '?', '=', '&', '='
        ];

        $index = 0;

        /** @var IdentifierInterface $identifier */
        foreach ($identifiers->getIterator() as $identifier) {

            $this->assertSame($expectedStrings[$index], $identifier->getString());
            $this->assertSame($expectedSeparators[$index], $identifier->getSeparator());
            $index++;
        }

        $this->assertCount(8, $identifiers);
    }

    public function testResolveFromRichUrl()
    {
        $url = 'https://www.eshop.com/category/color=blue/size=large?color=red&available=1';
        $url = (new UrlBuilder())->buildFromString($url);
        $identifiers = $this->resolver->resolveIdentifiers($url);

        $expectedStrings = [
            '/category/color=blue/size=large?color=red&available=1',
            '/category/color=blue/size=large?color=red',
            '/category/color=blue/size=large',
            '/category/color=blue/size',
            '/category/color=blue',
            '/category/color',
            '/category',
            '/',
            'category', 'color', 'blue', 'size', 'large', 'color', 'red', 'available', '1'
        ];
        $expectedSeparators = [
            '', '', '', '', '', '', '', '',
            '/', '/', '=', '/', '=', '?', '=', '&', '='
        ];
        $index = 0;

        /** @var IdentifierInterface $identifier */
        foreach ($identifiers->getIterator() as $identifier) {

            $this->assertSame($expectedStrings[$index], $identifier->getString());
            $this->assertSame($expectedSeparators[$index], $identifier->getSeparator());
            $index++;
        }
        $this->assertCount(17, $identifiers);
    }
}