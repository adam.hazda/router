<?php

namespace Hazadam\Router\Net\Uri;

use PHPUnit\Framework\TestCase;

/**
 * Class IdentifierCollectionTest
 * @package Hazadam\Router\Net\Uri
 */
class IdentifierCollectionTest extends TestCase
{
    /**
     * @var IdentifierCollection
     */
    protected $collection;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->collection = new IdentifierCollection();
    }

    public function testEmptyCollection()
    {
        $this->assertSame([], $this->collection->getByString("dummy"));
    }

    public function testAggregateMethods()
    {
        $identifier1 = new Identifier("i1", "sep1");
        $identifier2 = new Identifier("i1", "sep2");
        $identifier3 = new Identifier("i3", "sep3");
        $identifier4 = new Identifier("i1", "sep1");


        $this->collection->add($identifier1);
        $this->collection->add($identifier2);
        $this->collection->add($identifier3);
        $this->collection->add($identifier4);
        $this->collection->remove($identifier2);


        $identifiersByString = $this->collection->getByString("i1");
        $this->assertSame(2, count($identifiersByString));
        $this->assertSame($identifier4, $this->collection->getNext($identifier3));
    }
}