<?php

namespace Hazadam\Router\Net\Uri;

use Hazadam\Router\Net\Uri\Interfaces\UrlBuilderInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class UrlBuilderTest
 * @package Hazadam\Net\Uri
 */
class UrlBuilderTest extends TestCase
{
    /**
     * @var UrlBuilderInterface
     */
    protected $urlBuilder;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->urlBuilder = new UrlBuilder();
    }

    public function testBuildFromEmptyString()
    {
        $url = "";
        $url = $this->urlBuilder->buildFromString($url);
        $this->assertSame(null, $url->getScheme());
        $this->assertSame(null, $url->getUser());
        $this->assertSame(null, $url->getPass());
        $this->assertSame(null, $url->getHost());
        $this->assertSame("", $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testBuildFromString()
    {
        $url = "https://adam.hazda:password@www.shop.cz:8080/path-to-script?argument=value&argument2=value2#price-range=200-300";
        $url = $this->urlBuilder->buildFromString($url);
        $this->assertSame("https", $url->getScheme());
        $this->assertSame("www.shop.cz", $url->getHost());
        $this->assertSame("/path-to-script", $url->getPath());
        $this->assertSame("argument=value&argument2=value2", $url->getQuery());
        $this->assertSame("price-range=200-300", $url->getFragment());
    }

    public function testStringFromUrlObject()
    {
        $origStringUrl = "https://adam.hazda:password@www.shop.cz:8080/path-to-script?argument=value&argument2=value2#price-range=200-300";
        $url = $this->urlBuilder->buildFromString($origStringUrl);
        $url = $this->urlBuilder->urlToString($url);
        $this->assertSame($origStringUrl, $url);
    }
}