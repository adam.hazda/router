<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri;

use Hazadam\Router\Net\Uri\Generator\HelperTrait;

/**
 * Trait UrlFactoryTrait
 * @package Hazadam\Router\Net\Uri
 */
trait UrlFactoryTrait
{
    use HelperTrait;

    /**
     * @param $url
     * @return Url
     */
    protected function buildUrlFromString($url): Url
    {
        $url = (new UrlBuilder())->buildFromString($url);

        return $url;
    }
}