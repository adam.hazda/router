<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Interfaces\PaginationUrlDecoratorInterface;
use Hazadam\Router\Net\Uri\Url;
use PHPUnit\Framework\TestCase;

/**
 * Class PaginationUrlDecoratorTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class PaginationUrlDecoratorTest extends TestCase
{
    /**
     * @var PaginationUrlDecoratorInterface
     */
    protected $decorator;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->decorator = new PaginationUrlDecorator();
    }

    public function testAddPageToUrl()
    {
        $url = new Url(null, null, null, null, null, '/category', null, null);
        $this->decorator->addPage($url, 1234);
        $this->assertSame('/category/1234', $url->getPath());
    }

    public function testAddPageToUrlWithTrailingSlash()
    {
        $url = new Url(null, null, null, null, null, '/category/', null, null);
        $this->decorator->addPage($url, 1234);
        $this->assertSame('/category/1234', $url->getPath());
    }
}