<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\PaginatedResource;
use Hazadam\Router\Net\PairedResourceSpecifier;
use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\ResourceCollection;
use Hazadam\Router\Net\ResourceSpecifier;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRoute;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;
use Hazadam\Router\Net\Uri\Generator\Composition\RoutesTransformer;
use Hazadam\Router\Net\Uri\Generator\Interfaces\ResourceUrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Identifier;
use PHPUnit\Framework\TestCase;

/**
 * Class ResourceUrlGeneratorTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class ResourceUrlGeneratorTest extends TestCase
{
    /**
     * @var ResourceUrlGeneratorInterface
     */
    protected $generator;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->generator = new ResourceUrlGenerator(
            new RootRouteResolver(), new PathGenerator(), new QueryStringGenerator(),
            new PaginationUrlDecorator()
        );
    }

    public function testGenerateFromEmptyInputsException()
    {
        $exception = null;

        try {

            $resourceCollection = new ResourceCollection();
            $availableRoutes = new OrderedRouteCollection();
            $this->generator->generate($resourceCollection, $availableRoutes);

        } catch (\InvalidArgumentException $exception) {}

        $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
    }

    public function testGenerateFromInputsNotMatchingIds()
    {
        $exception = null;

        try {

            $dummyIdentifier = new Identifier('dummy', '');
            $dummyAvailableResource = new Resource(2, '');
            $dummyRequestedResource = new Resource(3, '');
            $dummyRoute = new Route(
                1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $dummyIdentifier, '', $dummyAvailableResource, []
            );
            $dummyRoute = new OrderedRoute($dummyRoute, []);
            $resourceCollection = new ResourceCollection();
            $resourceCollection->add($dummyRequestedResource);
            $availableRoutes = new OrderedRouteCollection();
            $availableRoutes->add($dummyRoute);
            $this->generator->generate($resourceCollection, $availableRoutes);

        } catch (\InvalidArgumentException $exception) {}

        $this->assertInstanceOf(\InvalidArgumentException::class, $exception);
    }

    public function testGenerateFromTwoRootRoutesBothNotLandingPageException()
    {
        $exception = null;

        try {

            $identifier = new Identifier('dummy', '');
            $availableResource = new Resource(2, '');
            $requestedResource = new Resource(2, '');
            $route1 = new Route(
                1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $identifier, '', $availableResource, []
            );
            $route1 = new OrderedRoute($route1, []);
            $route2 = new Route(
                2, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $identifier, '', $availableResource, []
            );
            $route2 = new OrderedRoute($route2, []);
            $resourceCollection = new ResourceCollection();
            $resourceCollection->add($requestedResource);
            $availableRoutes = new OrderedRouteCollection();
            $availableRoutes->add($route1);
            $availableRoutes->add($route2);
            $this->generator->generate($resourceCollection, $availableRoutes);

        } catch (\LogicException $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testGenerateRootResourceInPath()
    {
        $identifier = new Identifier('dummy', '');
        $resource = new Resource(2, '');
        $route = new Route(1, '', 0, $identifier, '', $resource, []);
        $route = new OrderedRoute($route, []);
        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($resource);
        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($route);
        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/dummy', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateRootResourceInPathWithPage()
    {
        $identifier = new Identifier('dummy', '');
        $resource = new Resource(2, '');
        $paginatedResource = new PaginatedResource($resource, 42);
        $route = new Route(1, '', 0, $identifier, '', $paginatedResource, []);
        $route = new OrderedRoute($route, []);
        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($paginatedResource);
        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($route);
        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/dummy/42', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateRootResourceInQueryStringException()
    {
        $exception = null;

        try {

            $identifier = new Identifier('dummy', '');
            $resource = new Resource(2, '');
            $route = new Route(
                1, RouteInterface::CONTROLLER, RouteInterface::POSITION_QUERY_STRING, $identifier, '', $resource, []
            );
            $route = new OrderedRoute($route, []);
            $resourceCollection = new ResourceCollection();
            $resourceCollection->add($resource);
            $availableRoutes = new OrderedRouteCollection();
            $availableRoutes->add($route);
            $this->generator->generate($resourceCollection, $availableRoutes);

        } catch (\LogicException $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testGenerateTwoPathRoutes()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);
        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 1]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($availabilityRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/available', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateAllTypesOfPathRoutes()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(
            2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []
        );
        $colorRoute = new Route(
            3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [
            $colorResourceSpecifier
        ]);
        $colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(
            3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $blueRoute = new Route(
            4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [
            $blueResourceSpecifier
        ]);
        $blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(
            4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $redRoute = new Route(
            5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $redIdentifier, '', null, [
            $redResourceSpecifier
        ]);
        $redRoute = new OrderedRoute($redRoute, [3 => 1]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);
        $resourceCollection->add($colorResourceSpecifier);
        $resourceCollection->add($blueResourceSpecifier);
        $resourceCollection->add($redResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/color=red,blue/available', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateStandaloneRouteInQueryString()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_QUERY_STRING, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($availabilityRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category', $url->getPath());
        $this->assertSame('?available=1', $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateAllTypesOfQueryStringRoutes()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_QUERY_STRING, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(
            2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []
        );
        $colorRoute = new Route(
            3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_QUERY_STRING, $colorIdentifier, '', null, [
            $colorResourceSpecifier
        ]);
        $colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(
            3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $blueRoute = new Route(
            4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $blueIdentifier, '', null, [
            $blueResourceSpecifier
        ]);
        $blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(
            4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $redRoute = new Route(
            5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $redIdentifier, '', null, [
            $redResourceSpecifier
        ]);
        $redRoute = new OrderedRoute($redRoute, [3 => 1]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);
        $resourceCollection->add($colorResourceSpecifier);
        $resourceCollection->add($blueResourceSpecifier);
        $resourceCollection->add($redResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category', $url->getPath());
        $this->assertSame('?color%5B0%5D=red&color%5B1%5D=blue&available=1', $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateTheSameRoutesInBothPathAndQueryString()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(
            2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []
        );
        $colorRoute = new Route(
            3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [
            $colorResourceSpecifier
        ]);
        $colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(
            3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $blueRoute = new Route(
            4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $blueIdentifier, '', null, [
            $blueResourceSpecifier
        ]);
        $blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(
            4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $redRoute = new Route(
            5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $redIdentifier, '', null, [
            $redResourceSpecifier
        ]);
        $redRoute = new OrderedRoute($redRoute, [3 => 1]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);
        $resourceCollection->add($blueResourceSpecifier);
        $resourceCollection->add($colorResourceSpecifier);
        $resourceCollection->add($redResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/color=red/available', $url->getPath());
        $this->assertSame('?color=blue', $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateFromMoreRoutesThanRequestedResources()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []
        );
        $availabilityRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(
            2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []
        );
        $colorRoute = new Route(
            3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [
            $colorResourceSpecifier
        ]);
        $colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(
            3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $blueRoute = new Route(
            4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [
            $blueResourceSpecifier
        ]);
        $blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(
            4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $redRoute = new Route(
            5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $redIdentifier, '', null, [
            $redResourceSpecifier
        ]);
        $redRoute = new OrderedRoute($redRoute, [3 => 1]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        /* $resourceCollection->add($availabilityResourceSpecifier); commented on purpose */
        $resourceCollection->add($colorResourceSpecifier);
        /* $resourceCollection->add($blueResourceSpecifier); commented on purpose */
        $resourceCollection->add($redResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/color=red', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGeneratePairedResourceSpecifiers()
    {
        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(
            1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $priceRangeIdentifier = new Identifier('price-range', '');
        $priceRangeResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, false, false, [2], []
        );
        $priceRangePairedResourceSpecifier = new PairedResourceSpecifier($priceRangeResourceSpecifier, ['200-300', '2000']);
        $priceRangeRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $priceRangeIdentifier, '', null, [
            $priceRangePairedResourceSpecifier
        ]);
        $priceRangeRoute = new OrderedRoute($priceRangeRoute, [1 => 2]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($priceRangePairedResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($priceRangeRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/price-range=200-300,2000', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testGenerateFromMultipleLandingPagesOnlyOneFitsTheSpecifiers()
    {
        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(
            1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [1], []
        );
        $availabilityRoute = new Route(
            1, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [
            $availabilityResourceSpecifier
        ]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [5 => 1, 6 => 1]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(
            2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [1], []
        );
        $colorRoute = new Route(
            2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [
            $colorResourceSpecifier
        ]);
        $colorRoute = new OrderedRoute($colorRoute, [5 => 2]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(
            3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $blueRoute = new Route(
            3, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [
            $blueResourceSpecifier
        ]);
        $blueRoute = new OrderedRoute($blueRoute, [2 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(
            4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]
        );
        $redRoute = new Route(
            4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $redIdentifier, '', null, [
            $redResourceSpecifier
        ]);
        $redRoute = new OrderedRoute($redRoute, [2 => 1]);

        $root = new Resource(1, '');

        $rootIdentifier = new Identifier('category', '');
        $rootRoute = new Route(
            5, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $root, []
        );
        $rootRoute = new OrderedRoute($rootRoute, []);

        $landingPageIdentifier1 = new Identifier('landing-page1', '');
        $landingRoute1 = new Route(
            6, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $landingPageIdentifier1, '', $root, [
                $availabilityResourceSpecifier, $colorResourceSpecifier, $blueResourceSpecifier, $redResourceSpecifier
            ]
        );
        $landingRoute1 = new OrderedRoute($landingRoute1, []);

        $landingPageIdentifier2 = new Identifier('landing-page2', '');
        $landingRoute2 = new Route(
            7, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $landingPageIdentifier2, '', $root, [
                $availabilityResourceSpecifier, $colorResourceSpecifier, $redResourceSpecifier
            ]
        );
        $landingRoute2 = new OrderedRoute($landingRoute2, []);

        $landingPageIdentifier3 = new Identifier('landing-page3', '');
        $landingRoute3 = new Route(
            8, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $landingPageIdentifier3, '', $root, [
                $availabilityResourceSpecifier
            ]
        );
        $landingRoute3 = new OrderedRoute($landingRoute3, []);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($root);
        $resourceCollection->add($availabilityResourceSpecifier);
        $resourceCollection->add($colorResourceSpecifier);
        $resourceCollection->add($blueResourceSpecifier);
        $resourceCollection->add($redResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($landingRoute1);
        $availableRoutes->add($landingRoute2);
        $availableRoutes->add($landingRoute3);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);

        $this->assertSame('/landing-page1', $url->getPath());
        $this->assertSame(null, $url->getQuery());
        $this->assertSame(null, $url->getFragment());
    }

    public function testRepeatedGeneration()
    {

        $rootIdentifier = new Identifier('category', '');
        $rootResource = new Resource(2, '');
        $rootRoute = new Route(1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []);
        $rootRoute = new OrderedRoute($rootRoute, []);

        $availabilityIdentifier = new Identifier('available', '');
        $availabilityResourceSpecifier = new ResourceSpecifier(1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []);
        $availabilityRoute = new Route(2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]);
        $availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

        $colorIdentifier = new Identifier('color', '');
        $colorResourceSpecifier = new ResourceSpecifier(2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []);
        $colorRoute = new Route(3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [$colorResourceSpecifier]);
        $colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

        $blueIdentifier = new Identifier('blue', '');
        $blueResourceSpecifier = new ResourceSpecifier(3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
        $blueRoute = new Route(4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [$blueResourceSpecifier]);
        $blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

        $redIdentifier = new Identifier('red', '');
        $redResourceSpecifier = new ResourceSpecifier(4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
        $redRoute = new Route(5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $redIdentifier, '', null, [$redResourceSpecifier]);
        $redRoute = new OrderedRoute($redRoute, [3 => 1]);

        $priceRangeIdentifier = new Identifier('price-range', '');
        $priceRangeResourceSpecifier = new ResourceSpecifier(10, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, false, false, [2], []);
        $priceRangePairedResourceSpecifier = new PairedResourceSpecifier($priceRangeResourceSpecifier, ['200-300', '2000']);
        $priceRangeRoute = new Route(11, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $priceRangeIdentifier, '', null, [$priceRangePairedResourceSpecifier]);
        $priceRangeRoute = new OrderedRoute($priceRangeRoute, [1 => 3]);

        $resourceCollection = new ResourceCollection();
        $resourceCollection->add($rootResource);
        $resourceCollection->add($availabilityResourceSpecifier);
        $resourceCollection->add($colorResourceSpecifier);
        $resourceCollection->add($blueResourceSpecifier);
        $resourceCollection->add($redResourceSpecifier);
        $resourceCollection->add($priceRangePairedResourceSpecifier);

        $availableRoutes = new OrderedRouteCollection();
        $availableRoutes->add($rootRoute);
        $availableRoutes->add($availabilityRoute);
        $availableRoutes->add($colorRoute);
        $availableRoutes->add($blueRoute);
        $availableRoutes->add($redRoute);
        $availableRoutes->add($priceRangeRoute);

        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/color=blue/available/price-range=200-300,2000', $url->getPath());

        $resourceCollection->add($redResourceSpecifier);
        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/color=blue/available/price-range=200-300,2000', $url->getPath());
        $this->assertSame('?color=red', $url->getQuery());

        $resourceCollection->remove($blueResourceSpecifier);
        $url = $this->generator->generate($resourceCollection, $availableRoutes);
        $this->assertSame('/category/available/price-range=200-300,2000', $url->getPath());
        $this->assertSame('?color=red', $url->getQuery());
    }
}