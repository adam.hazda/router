<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

/**
 * Class CompositionFactory
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class CompositionFactory
{
    /**
     * @return Composition
     */
    const QUERY_STRING_ROOT = 'root';

    /**
     * @param $rootId
     * @return Composition
     */
    public static function createQueryStringComposition($rootId): Composition
    {
        $composition = new Composition();
        $composition->add(new Identifier($rootId, [], [], '?'));

        return $composition;
    }
}