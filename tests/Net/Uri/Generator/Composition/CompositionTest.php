<?php

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use PHPUnit\Framework\TestCase;

/**
 * Class CompositionTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class CompositionTest extends TestCase
{
    /**
     * @var Composition
     */
    protected $composition;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->composition = new Composition();
    }

    public function testEmptyComposition()
    {
        $dummyParentId = 1;
        $this->assertSame(null, $this->composition->getRootIdentifier());
        $this->assertSame([], $this->composition->getChildIdentifiersIds($dummyParentId));
    }

    public function testCompositionWithRootIdentifier()
    {
        $identifier1 = new Identifier(1, [], [], "/");
        $identifier2 = new Identifier(2, [], [], "/");
        $identifier3 = new Identifier(3, [], [], "/");
        $this->composition->add($identifier1);
        $this->composition->add($identifier2);
        $this->composition->remove($identifier2);
        $this->composition->add($identifier3);
        $this->assertSame($identifier3, $this->composition->getRootIdentifier());
    }

    public function testCompositionWithParentIdsAndOrdering()
    {
        $identifier1 = new Identifier(1, [2, 3],       [2 => 2, 3 => 2], "/");
        $identifier2 = new Identifier(2, [2, 3, 4],    [2 => 1, 3 => 3, 4 => 1], "/");
        $identifier3 = new Identifier(3, [2, 3, 4, 5], [2 => 4, 3 => 1, 4 => 2, 5 => 2], "/");
        $identifier4 = new Identifier(4, [2, 3, 4, 5], [2 => 3, 3 => 4, 4 => 3, 5 => 1], "/");
        $identifier5 = new Identifier(5, [2, 3, 4, 5], [2 => 5, 3 => 5, 4 => 4, 5 => 3], "/");
        $this->composition->add($identifier1);
        $this->composition->add($identifier2);
        $this->composition->add($identifier3);
        $this->composition->add($identifier4);
        $this->composition->add($identifier5);
        $this->composition->remove($identifier5);
        $this->assertSame([2, 1, 4, 3], array_values($this->composition->getChildIdentifiersIds(2)));
        $this->assertSame([3, 1, 2, 4], array_values($this->composition->getChildIdentifiersIds(3)));
        $this->assertSame([2, 3, 4], array_values($this->composition->getChildIdentifiersIds(4)));
        $this->assertSame([4, 3], array_values($this->composition->getChildIdentifiersIds(5)));
    }
}
