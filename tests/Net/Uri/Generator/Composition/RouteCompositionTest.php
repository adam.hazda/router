<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\ResourceCollection;
use Hazadam\Router\Net\ResourceSpecifier;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Identifier;
use PHPUnit\Framework\TestCase;

/**
 * Class RouteCompositionTest
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class RouteCompositionTest extends TestCase
{
    /**
     * @var OrderedRouteCollectionInterface
     */
    protected $routeCollection;

    /**
     * @var RouteComposition
     */
    protected $routeComposition;

    protected function setUp()
    {
        $this->routeCollection = new OrderedRouteCollection();
        $this->routeComposition = new RouteComposition($this->routeCollection);
    }

    public function testEmptyRouteCollection()
    {
        $this->assertNull($this->routeComposition->getRootIdentifier());
        $this->assertEmpty($this->routeComposition->getChildIdentifiersIds(1));
        $this->assertNull($this->routeComposition->get(1));
    }

    public function testCollectionWithRootRoute()
    {
        $rootRoute = new Route(23, '', 0, new Identifier('/root', '/'), '', null, []);
        $orderedRoute = new OrderedRoute($rootRoute, []);
        $this->routeCollection->add($orderedRoute);
        $rootIdentifier = $this->routeComposition->getRootIdentifier();
        $this->assertNotNull($rootIdentifier);
        $this->assertSame('root', $rootIdentifier->getString());
        $this->assertNotNull($this->routeComposition->get(23));
        $this->assertSame([], $rootIdentifier->getOrdering());
        $this->assertSame([], $rootIdentifier->getParentIds());
    }

    public function testCollectionWithTwoRoutesInvalidBinding()
    {
        $rootRoute = new Route(23, '', 0, new Identifier('/root', '/'), '', null, []);
        $rootRoute = new OrderedRoute($rootRoute, []);
        $subRoute = new Route(26, '', 0, new Identifier('sub-route', ''), '', null, []);
        $subRoute = new OrderedRoute($subRoute, [24 => 1]);
        $this->routeCollection->add($rootRoute);
        $this->routeCollection->add($subRoute);
        $rootIdentifier = $this->routeComposition->getRootIdentifier();
        $this->assertNotNull($rootIdentifier);
        $this->assertSame('root', $rootIdentifier->getString());
        $this->assertNotNull($this->routeComposition->get(23));
        $this->assertSame([], $rootIdentifier->getOrdering());
        $this->assertSame([], $rootIdentifier->getParentIds());
        $this->assertSame([], $this->routeComposition->getChildIdentifiersIds(23));
    }

    public function testCollectionWithTwoRoutesValidBinding()
    {
        $resourceSpecifier = new ResourceSpecifier(12, '' ,'', false, false, [], []);
        $rootRoute = new Route(23, '', 0, new Identifier('/root', '/'), '', null, []);
        $rootRoute = new OrderedRoute($rootRoute, []);
        $subRoute = new Route(26, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier]);
        $subRoute = new OrderedRoute($subRoute, [23 => 1]);
        $this->routeCollection->add($rootRoute);
        $this->routeCollection->add($subRoute);
        $this->assertSame([26], $this->routeComposition->getChildIdentifiersIds(23));
        $this->assertSame('sub-route', $this->routeComposition->get(26)->getString());
    }

    public function testCollectionWithManyRoutes()
    {
        $resourceSpecifier1 = new ResourceSpecifier(12, '' ,'', false, false, [], []);
        $resourceSpecifier2 = new ResourceSpecifier(13, '' ,'', false, false, [], []);
        $resourceSpecifier3 = new ResourceSpecifier(14, '' ,'', false, false, [], []);
        $rootRoute = new Route(23, '', 0, new Identifier('/root', '/'), '', null, []);
        $rootRoute = new OrderedRoute($rootRoute, []);
        $subRoute1 = new Route(26, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier1]);
        $subRoute1 = new OrderedRoute($subRoute1, [23 => 2]);
        $subRoute2 = new Route(22, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier2]);
        $subRoute2 = new OrderedRoute($subRoute2, [23 => 1]);
        $subSubRoute1 = new Route(21, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier3]);
        $subSubRoute1 = new OrderedRoute($subSubRoute1, [22 => 1]);
        $this->routeCollection->add($rootRoute);
        $this->routeCollection->add($subRoute1);
        $this->routeCollection->add($subRoute2);
        $this->routeCollection->add($subSubRoute1);
        $this->assertSame([22, 26], $this->routeComposition->getChildIdentifiersIds(23));
        $this->assertSame([21], $this->routeComposition->getChildIdentifiersIds(22));
    }

    public function testChildFreeBoundResourceSpecifierRoute()
    {
        $resourceSpecifier1 = new ResourceSpecifier(12, '' ,'', false, false, [], []);
        $resourceSpecifier2 = new ResourceSpecifier(13, '' ,ResourceSpecifierInterface::TYPE_BOUND, false, false, [], []);
        $resourceSpecifier3 = new ResourceSpecifier(14, '' ,'', false, false, [], []);
        $rootRoute = new Route(23, '', 0, new Identifier('/root', '/'), '', null, []);
        $rootRoute = new OrderedRoute($rootRoute, []);
        $subRoute1 = new Route(26, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier1]);
        $subRoute1 = new OrderedRoute($subRoute1, [23 => 2]);
        $subRoute2 = new Route(22, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier2]);
        $subRoute2 = new OrderedRoute($subRoute2, [23 => 1]);
        $subSubRoute1 = new Route(21, '', 0, new Identifier('sub-route', ''), '', null, [$resourceSpecifier3]);
        $subSubRoute1 = new OrderedRoute($subSubRoute1, [27 => 1]);
        $this->routeCollection->add($rootRoute);
        $this->routeCollection->add($subRoute1);
        $this->routeCollection->add($subRoute2);
        $this->routeCollection->add($subSubRoute1);
        $this->assertSame([26], array_values($this->routeComposition->getChildIdentifiersIds(23)));
        $this->assertSame([], $this->routeComposition->getChildIdentifiersIds(22));
    }
}