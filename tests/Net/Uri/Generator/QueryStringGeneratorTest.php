<?php

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Composition\Composition;
use Hazadam\Router\Net\Uri\Generator\Composition\CompositionFactory;
use Hazadam\Router\Net\Uri\Generator\Composition\Identifier;
use Hazadam\Router\Net\Uri\Generator\Interfaces\UrlGeneratorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class QueryStringGeneratorTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class QueryStringGeneratorTest extends TestCase
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $generator;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->generator = new QueryStringGenerator();
    }

    public function testGenerateFromEmptyComposition()
    {
        $composition = new Composition();
        $url = $this->generator->generate($composition);
        $this->assertSame(null, $url->getQuery());
    }

    public function testGenerateFromRootIdentifier()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $rootIdentifier = new Identifier(
            1, [$root], [$root => 1], 'shoes'
        );
        $composition->add($rootIdentifier);
        $url = $this->generator->generate($composition);
        $this->assertSame('?shoes=1', $url->getQuery());
    }

    public function testGenerateFromTwoSameLevelIdentifiers()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $identifier1 = new Identifier(
            1, [$root], [$root => 1], 'shoes'
        );
        $identifier2 = new Identifier(
            2, [$root], [$root => 2], 'in-stock'
        );
        $composition->add($identifier1);
        $composition->add($identifier2);
        $url = $this->generator->generate($composition);
        $this->assertSame('?shoes=1&in-stock=1', $url->getQuery());
    }

    public function testGenerateFromThreeSameLevelIdentifiers()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $identifier1 = new Identifier(1, [$root], [$root => 1], 'in-stock');
        $identifier2 = new Identifier(2, [$root], [$root => 3], 'new');
        $identifier3 = new Identifier(3, [$root], [$root => 2], 'in-sale');
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $url = $this->generator->generate($composition);
        $this->assertSame('?in-stock=1&in-sale=1&new=1', $url->getQuery());
    }

    public function testGenerateFromTwoLevelsIdentifiers()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $identifier1 = new Identifier(
            1, [$root], [$root => 1], 'color'
        );
        $identifier2 = new Identifier(2, [1], [1 => 2], 'blue');
        $identifier3 = new Identifier(3, [1], [1 => 1], 'red');
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $url = $this->generator->generate($composition);
        $this->assertSame('?color%5B0%5D=red&color%5B1%5D=blue', $url->getQuery());
    }

    public function testGenerateFromComplexComposition()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $identifier1 = new Identifier(1, [$root], [$root => 1], 'color');
        $identifier2 = new Identifier(2, [1], [1 => 2], 'blue');
        $identifier3 = new Identifier(3, [1], [1 => 1], 'red');
        $identifier4 = new Identifier(4, [$root], [$root => 2], 'in-stock');
        $identifier5 = new Identifier(5, [$root], [$root => 3], 'price');
        $identifier6 = new Identifier(6, [5], [5 => 1], '200-300');
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $composition->add($identifier4);
        $composition->add($identifier5);
        $composition->add($identifier6);
        $url = $this->generator->generate($composition);
        $this->assertSame('?color%5B0%5D=red&color%5B1%5D=blue&in-stock=1&price=200-300', $url->getQuery());
    }

    public function testGenerateFromMultipleParentsIdentifier()
    {
        $root = CompositionFactory::QUERY_STRING_ROOT;
        $composition = CompositionFactory::createQueryStringComposition($root);
        $identifier1 = new Identifier(1, [$root], [$root => 1], '1');
        $identifier2 = new Identifier(2, [$root], [$root => 2], '2');
        $identifier3 = new Identifier(3, [1, 2], [1 => 1,2  => 1], 'child');
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $url = $this->generator->generate($composition);
        $this->assertSame('?1=child&2=child', $url->getQuery());
    }
}