<?php

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Composition\Composition;
use Hazadam\Router\Net\Uri\Generator\Composition\Identifier;
use Hazadam\Router\Net\Uri\Generator\Interfaces\UrlGeneratorInterface;
use PHPUnit\Framework\TestCase;

/**
 * Class PathGeneratorTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class PathGeneratorTest extends TestCase
{
    /**
     * @var UrlGeneratorInterface
     */
    protected $generator;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->generator = new PathGenerator();
    }

    public function testGenerateFromEmptyComposition()
    {
        $composition = new Composition();
        $url = $this->generator->generate($composition);
        $this->assertSame('', $url->getPath());
    }

    public function testGenerateFromRootIdentifier()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $composition->add($rootIdentifier);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes', $url->getPath());
    }

    public function testGenerateFromOneIdentifier()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $identifier1 = new Identifier(2, [1], [1 => 1], 'in-stock');
        $composition->add($rootIdentifier);
        $composition->add($identifier1);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes/in-stock', $url->getPath());
    }

    public function testGenerateFromTwoSameLevelIdentifiers()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $identifier1 = new Identifier(2, [1], [1 => 1], 'in-stock');
        $identifier2 = new Identifier(3, [1], [1 => 2], 'new');
        $composition->add($rootIdentifier);
        $composition->add($identifier1);
        $composition->add($identifier2);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes/in-stock/new', $url->getPath());
    }

    public function testGenerateFromThreeSameLevelIdentifiers()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $identifier1 = new Identifier(2, [1], [1 => 1], 'in-stock');
        $identifier2 = new Identifier(3, [1], [1 => 3], 'new');
        $identifier3 = new Identifier(4, [1], [1 => 2], 'in-sale');
        $composition->add($rootIdentifier);
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes/in-stock/in-sale/new', $url->getPath());
    }

    public function testGenerateFromTwoLevelsIdentifiers()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $identifier1 = new Identifier(2, [1], [1 => 1], 'color');
        $identifier2 = new Identifier(3, [2], [2 => 2], 'blue');
        $identifier3 = new Identifier(4, [2], [2 => 1], 'red');
        $identifier4 = new Identifier(5, [2], [2 => 3], 'yellow');
        $composition->add($rootIdentifier);
        $composition->add($identifier1);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $composition->add($identifier4);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes/color=red,blue,yellow', $url->getPath());
    }

    public function testGenerateFromComplexComposition()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'shoes');
        $identifier2 = new Identifier(2, [1], [1 => 1], 'color');
        $identifier3 = new Identifier(3, [2], [2 => 2], 'blue');
        $identifier4 = new Identifier(4, [2], [2 => 1], 'red');
        $identifier5 = new Identifier(5, [1], [1 => 2], 'in-stock');
        $identifier6 = new Identifier(6, [1], [1 => 3], 'price');
        $identifier7 = new Identifier(7, [6], [6 => 1], '200-300');
        $composition->add($rootIdentifier);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $composition->add($identifier4);
        $composition->add($identifier5);
        $composition->add($identifier6);
        $composition->add($identifier7);
        $url = $this->generator->generate($composition);
        $this->assertSame('/shoes/color=red,blue/in-stock/price=200-300', $url->getPath());
    }

    public function testGenerateFromMultipleParentsIdentifier()
    {
        $composition = new Composition();
        $rootIdentifier = new Identifier(1, [], [], 'root');
        $identifier2 = new Identifier(2, [1], [1 => 1], '1');
        $identifier3 = new Identifier(3, [1], [1 => 2], '2');
        $identifier4 = new Identifier(4, [2, 3], [2 => 1, 3 => 1], 'child');
        $composition->add($rootIdentifier);
        $composition->add($identifier2);
        $composition->add($identifier3);
        $composition->add($identifier4);
        $url = $this->generator->generate($composition);
        $this->assertSame('/root/1=child/2=child', $url->getPath());
    }
}