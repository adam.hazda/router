<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\ResourceSpecifier;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRoute;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;
use Hazadam\Router\Net\Uri\Generator\Interfaces\RootRouteResolverInterface;
use Hazadam\Router\Net\Uri\Identifier;
use PHPUnit\Framework\TestCase;

/**
 * Class RootRouteResolverTest
 * @package Hazadam\Router\Net\Uri\Generator
 */
class RootRouteResolverTest extends TestCase
{
    /**
     * @var RootRouteResolverInterface
     */
    protected $resolver;

    protected function setUp()
    {
        $this->resolver = new RootRouteResolver();
    }

    public function testResolveFromEmptyInputs()
    {
        $resourceSet = new ResourceSet();
        $availableRoutes = new OrderedRouteCollection();
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertNull($rootRoute);
    }

    public function testResolveFromEmptyAvailableRoutes()
    {
        $resourceSet = new ResourceSet();
        $resourceSet->setRootResource(new Resource(1, ''));
        $availableRoutes = new OrderedRouteCollection();
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertNull($rootRoute);
    }

    public function testResolveFromNonMatchingInputs()
    {
        $resourceSet = new ResourceSet();
        $wantedResource = new Resource(1, '');
        $availableResource = new Resource(2, '');
        $resourceSet->setRootResource($wantedResource);
        $availableRoutes = new OrderedRouteCollection();
        $route = new Route(1, '', 0, new Identifier('', ''), '', $availableResource, []);
        $orderedRoute = new OrderedRoute($route, []);
        $availableRoutes->add($orderedRoute);
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertNull($rootRoute);
    }

    public function testResolveFromMatchingInputs()
    {
        $resourceSet = new ResourceSet();
        $resource = new Resource(1, '');
        $resourceSet->setRootResource($resource);
        $availableRoutes = new OrderedRouteCollection();
        $route = new Route(1, '', 0, new Identifier('', ''), '', $resource, []);
        $orderedRoute = new OrderedRoute($route, []);
        $availableRoutes->add($orderedRoute);
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertSame($orderedRoute, $rootRoute);
    }

    public function testTooManyRootRoutesException()
    {
        $exception = null;

        try {

            $resourceSet = new ResourceSet();
            $resource = new Resource(1, '');
            $resourceSet->setRootResource($resource);
            $availableRoutes = new OrderedRouteCollection();
            $route = new Route(1, '', 0, new Identifier('', ''), '', $resource, []);
            $redundantRoute = new Route(2, '', 0, new Identifier('', ''), '', $resource, []);
            $orderedRoute = new OrderedRoute($route, []);
            $redundantRoute = new OrderedRoute($redundantRoute, []);
            $availableRoutes->add($orderedRoute);
            $availableRoutes->add($redundantRoute);
            $this->resolver->resolveRoot($resourceSet, $availableRoutes);

        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testResolveLandingPageRoute()
    {
        $resourceSet = new ResourceSet();
        $resource = new Resource(1, '');
        $resourceSpecifier = new ResourceSpecifier(1, '', '', false, false, [1], []);
        $resourceSet->setRootResource($resource);
        $resourceSet->addResourceSpecifier($resourceSpecifier);
        $availableRoutes = new OrderedRouteCollection();
        $ordinaryRoute = new Route(2, '', 0, new Identifier('', ''), '', $resource, []);
        $orderedOrdinaryRoute = new OrderedRoute($ordinaryRoute, []);
        $landingPageRoute = new Route(1, '', 0, new Identifier('', ''), '', $resource, [$resourceSpecifier]);
        $orderedLandingPageRoute = new OrderedRoute($landingPageRoute, []);
        $availableRoutes->add($orderedOrdinaryRoute);
        $availableRoutes->add($orderedLandingPageRoute);
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertSame($orderedLandingPageRoute, $rootRoute);
    }

    public function testTooManyLandingPagesException()
    {
        $exception = null;

        try {

            $resourceSet = new ResourceSet();
            $resource = new Resource(1, '');
            $resourceSpecifier = new ResourceSpecifier(1, '', '', false, false, [1], []);
            $resourceSet->setRootResource($resource);
            $resourceSet->addResourceSpecifier($resourceSpecifier);
            $availableRoutes = new OrderedRouteCollection();
            $ordinaryRoute = new Route(2, '', 0, new Identifier('', ''), '', $resource, []);
            $orderedOrdinaryRoute = new OrderedRoute($ordinaryRoute, []);
            $landingPageRoute = new Route(1, '', 0, new Identifier('', ''), '', $resource, [$resourceSpecifier]);
            $orderedLandingPageRoute = new OrderedRoute($landingPageRoute, []);
            $redundantLandingPageRoute = new Route(3, '', 0, new Identifier('', ''), '', $resource, [$resourceSpecifier]);
            $orderedRedundantLandingPageRoute = new OrderedRoute($redundantLandingPageRoute, []);
            $availableRoutes->add($orderedOrdinaryRoute);
            $availableRoutes->add($orderedLandingPageRoute);
            $availableRoutes->add($orderedRedundantLandingPageRoute);
            $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        } catch (\Exception $exception) {}

        $this->assertInstanceOf(\LogicException::class, $exception);
    }

    public function testIgnoreLandingPageWithMoreResourceSpecifiersThanRequested()
    {
        $resourceSet = new ResourceSet();
        $resource = new Resource(1, '');
        $resourceSpecifier = new ResourceSpecifier(1, '', '', false, false, [1], []);
        $redundantResourceSpecifier = new ResourceSpecifier(2, '', '' , false, false, [1], []);
        $resourceSet->setRootResource($resource);
        $resourceSet->addResourceSpecifier($resourceSpecifier);
        $availableRoutes = new OrderedRouteCollection();
        $ordinaryRoute = new Route(2, '', 0, new Identifier('', ''), '', $resource, []);
        $orderedOrdinaryRoute = new OrderedRoute($ordinaryRoute, []);
        $landingPageRoute = new Route(1, '', 0, new Identifier('', ''), '', $resource, [
                $resourceSpecifier, $redundantResourceSpecifier
            ]
        );
        $orderedLandingPageRoute = new OrderedRoute($landingPageRoute, []);
        $availableRoutes->add($orderedOrdinaryRoute);
        $availableRoutes->add($orderedLandingPageRoute);
        $rootRoute = $this->resolver->resolveRoot($resourceSet, $availableRoutes);
        $this->assertSame($orderedOrdinaryRoute, $rootRoute);
    }
}