<?php

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Uri\Identifier;
use PHPUnit\Framework\TestCase;

/**
 * Class RouteCollectionTest
 * @package Hazadam\Router
 */
class RouteCollectionTest extends TestCase
{
    /**
     * @var RouteCollectionInterface
     */
    protected $collection;

    /**
     * @inheritDoc
     */
    const CONTROLLER = 'controller';

    protected function setUp()
    {
        $this->collection = new RouteCollection();
    }

    public function testEmptyCollection()
    {
        $this->assertSame([], $this->collection->getByIdentifier("dummy"));
        $this->assertSame([], $this->collection->getByRoutableType('dummy'));
        $this->assertSame([], $this->collection->getControllerRoutes());
        $this->assertSame([], $this->collection->getAllByResourceId(2));
        $this->assertSame([], $this->collection->getSimpleByResourceId(2));
        $this->assertSame([], $this->collection->getByParentResourceSpecifierId(2));
        $this->assertSame([], $this->collection->getCoveringResourceSpecifierIds([]));
        $this->assertSame([], $this->collection->getSimpleByResourceId(2));
    }

    public function testOneItemCollection()
    {
        $resource = new Resource(1, "product");

        $route = new Route(
            1, self::CONTROLLER, 0, new Identifier('dummy', ''), "controller::action", $resource, [
                new ResourceSpecifier(1, "paramter_name", '', true, false, [1], []),
                new ResourceSpecifier(2, "paramter_value", '', true, false, [], [2]),
                new ResourceSpecifier(3, "paramter_value", '', true, false, [], [2])
            ]
        );
        $this->collection->add($route);
        $this->assertSame([$route], array_values($this->collection->getByIdentifier("dummy")));
        $this->assertSame([$route], array_values($this->collection->getByRoutableType('product')));
        $this->assertSame([$route], array_values($this->collection->getControllerRoutes()));
        $this->assertSame([$route], array_values($this->collection->getAllByResourceId(1)));
        $this->assertSame([], array_values($this->collection->getAllByResourceId(2)));
        $this->assertSame([$route], array_values($this->collection->getByResourceSpecifierId(1)));
        $this->assertSame([$route], array_values($this->collection->getByParentResourceSpecifierId(2)));
        $this->assertSame([$route], array_values($this->collection->getCoveringResourceSpecifierIds([2,1,3])));
        $this->assertSame([], array_values($this->collection->getSimpleByResourceId(1)));
    }

    public function testManyItemsCollections()
    {
        $mainRoute1 = new Route(
            1, self::CONTROLLER, 0, new Identifier('/products', ''), 'alist::render', new Resource(1, 'category'), []
        );
        $mainRoute2 = new Route(
            2, self::CONTROLLER, 0, new Identifier('/product', ''), 'details::render', new Resource(2, 'product'), []
        );
        $specifierRoute1 = new Route(
            3, self::CONTROLLER, 0, new Identifier('skladem', '/'), '', null, [new ResourceSpecifier(1, 'availability', '', true, false, [1], [])]
        );
        $specifierRoute2 = new Route(
            4, self::CONTROLLER, 0, new Identifier('color', '/'), '', null, [new ResourceSpecifier(2, 'parameter_name', '', true, false, [1], [])]
        );
        $specifierRoute3 = new Route(
            5, self::CONTROLLER, 0, new Identifier('blue', '='), '', null, [new ResourceSpecifier(3, 'parameter_value', '', true, false, [], [2])]
        );
        $specifierRoute4 = new Route(
            6, self::CONTROLLER, 0, new Identifier('blue', '/'), '', null, [new ResourceSpecifier(4, 'state', '', true, false, [1], [])]
        );

        $this->collection->add($mainRoute1);
        $this->collection->add($mainRoute2);
        $this->collection->add($specifierRoute1);
        $this->collection->add($specifierRoute2);
        $this->collection->add($specifierRoute3);
        $this->collection->add($specifierRoute4);

        $this->assertEquals([$mainRoute1, $mainRoute2], array_values($this->collection->getControllerRoutes()));
        $this->assertEquals([$mainRoute1, $mainRoute2], array_values($this->collection->getWithResources()));
        $this->assertEquals([$specifierRoute2], array_values($this->collection->getByIdentifier('color')));
        $this->assertEquals(
            [$specifierRoute3, $specifierRoute4], array_values($this->collection->getByIdentifier('blue'))
        );
        $this->assertEquals(
            [$specifierRoute1, $specifierRoute2, $specifierRoute4],
            array_values($this->collection->getByParentResourceId(1))
        );
        $this->assertEquals([$mainRoute1], array_values($this->collection->getAllByResourceId(1)));
        $this->assertEquals([], array_values($this->collection->getAllByResourceId(4)));
        $this->assertEquals([$specifierRoute4], array_values($this->collection->getByResourceSpecifierId(4)));
        $this->assertEquals([$mainRoute1], array_values($this->collection->getByRoutableType('category')));
        $this->assertEquals([$specifierRoute4], array_values($this->collection->getByRoutableType('state')));
        $this->assertSame([$mainRoute1], array_values($this->collection->getSimpleByResourceId(1)));
        $this->assertSame([$mainRoute2], array_values($this->collection->getSimpleByResourceId(2)));
    }

    public function testRemoveItems()
    {
        $mainRoute1 = new Route(
            1, self::CONTROLLER, 0, new Identifier('/products', ''), 'alist::render', new Resource(1, 'category'), []
        );
        $mainRoute2 = new Route(
            2, self::CONTROLLER, 0, new Identifier('/product', ''), 'details::render', new Resource(2, 'product'), []
        );
        $specifierRoute1 = new Route(
            3, self::CONTROLLER, 0, new Identifier('skladem', '/'), '', null, [new ResourceSpecifier(1, 'parameter_name', '', true, false, [1], [])]
        );
        $specifierRoute2 = new Route(
            4, self::CONTROLLER, 0, new Identifier('color', '/'), '', null, [new ResourceSpecifier(2, 'parameter_name', '', true, false, [1], [])]
        );
        $specifierRoute3 = new Route(
            5, self::CONTROLLER, 0, new Identifier('blue', '='), '', null, [new ResourceSpecifier(3, 'parameter_value', '', true, false, [4], [])]
        );
        $specifierRoute4 = new Route(
            6, self::CONTROLLER, 0, new Identifier('blue', '/'), '', null, [new ResourceSpecifier(4, 'state', true, '', false, [1], [])]
        );
        $specifierRoute5 = new Route(
            7, self::CONTROLLER, 0, new Identifier('blue', '/'), '', null, [new ResourceSpecifier(4, 'state', true, '', false, [1], [])]
        );

        $this->collection->add($mainRoute1);
        $this->collection->add($mainRoute2);
        $this->collection->add($specifierRoute1);
        $this->collection->add($specifierRoute2);
        $this->collection->add($specifierRoute3);
        $this->collection->add($specifierRoute4);
        $this->collection->add($specifierRoute5);

        $this->collection->remove($mainRoute2);
        $this->collection->remove($specifierRoute5);

        $this->assertSame(1, count($this->collection->getControllerRoutes()));
        $this->assertSame(0, count($this->collection->getAllByResourceId(6)));
        $this->assertSame(1, count($this->collection->getByResourceSpecifierId(4)));
        $this->assertSame(1, count($this->collection->getByRoutableType('state')));
        $this->assertSame(3, count($this->collection->getByParentResourceId(1)));
    }
}