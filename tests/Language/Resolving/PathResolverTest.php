<?php

namespace Hazadam\Router\Language\Resolving;

use Hazadam\Router\Language\Resolving\Interfaces\LanguageResolverInterface;
use Hazadam\Router\Net\Uri\UrlFactoryTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class PathResolverTest
 * @package Hazadam\Router\Language\Resolving
 */
class PathResolverTest extends TestCase
{
    use LanguageResolverTestTrait, UrlFactoryTrait;

    const LANGUAGES_MAP = [
        2 => 'cz',
        1 => 'en',
        0 => 'de'
    ];

    /**
     * @var LanguageResolverInterface
     */
    protected $resolver;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->resolver = new PathResolver(self::LANGUAGES_MAP, 2);
    }

    public function testResolveDefaultLanguage()
    {
        $url = $this->givenUrlWithNoLanguageIdentifierInPath();
        $this->theResolvedLanguageIsDefaultLanguage($url);
    }

    public function testInvalidConstructorArgumentsException()
    {
        $this->exceptionIsRaised();
        $this->whenPathResolverIsGivenLanguageMapWithoutDefaultLanguage();
    }

    public function testResolveNonDefaultLanguage()
    {
        $url = $this->givenUrlWithLanguageIdentifier("en");
        $this->theResolvedLanguageShouldBeEnglish($url);
    }

    /**
     * @return Url|string
     */
    protected function givenUrlWithNoLanguageIdentifierInPath()
    {
        $url = "https://www.eshop.cz/";
        $url = $this->buildUrlFromString($url);
        return $url;
    }

    /**
     * @param string $identifier
     * @return Url|string
     */
    protected function givenUrlWithLanguageIdentifier(string $identifier)
    {
        $url = sprintf("https://www.eshop.cz/%s/shoes/in-stock", $identifier);
        $url = $this->buildUrlFromString($url);

        return $url;
    }
}