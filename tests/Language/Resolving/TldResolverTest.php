<?php

namespace Hazadam\Router\Language\Resolving;

use Hazadam\Router\Language\Resolving\Interfaces\LanguageResolverInterface;
use Hazadam\Router\Net\Uri\UrlFactoryTrait;
use PHPUnit\Framework\TestCase;

/**
 * Class TldResolverTest
 * @package Hazadam\Router\Language\Resolving
 */
class TldResolverTest extends TestCase
{
    use LanguageResolverTestTrait, UrlFactoryTrait;

    /**
     * @var LanguageResolverInterface
     */
    protected $resolver;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $this->resolver = new TldResolver(PathResolverTest::LANGUAGES_MAP, 2);
    }

    public function testDefaultLanguageResolving()
    {
        $defaultLanguageUrl = "https://www.eshop.cz/boty/skladem";
        $url = $this->buildUrlFromString($defaultLanguageUrl);
        $this->theResolvedLanguageIsDefaultLanguage($url);
    }

    public function testNonDefaultLanguageResolving()
    {
        $englishLanguageUrl = "https://www.eshop.en/shoes/in-stock";
        $url = $this->buildUrlFromString($englishLanguageUrl);
        $this->theResolvedLanguageShouldBeEnglish($url);
    }
}