<?php

namespace Hazadam\Router\Language\Resolving;
use Hazadam\Router\Net\Uri\Url;
use Hazadam\Router\Net\Uri\UrlBuilder;

/**
 * Trait LanguageResolverTestTrait
 * @package Hazadam\Router\Language\Resolving
 */
trait LanguageResolverTestTrait
{
    /**
     * @param $url
     */
    protected function theResolvedLanguageIsDefaultLanguage($url): void
    {
        $language = $this->resolver->resolveFromUrl($url);
        $this->assertSame(2, $language->getId());
        $this->assertSame("cz", $language->toString());
    }

    protected function exceptionIsRaised(): void
    {
        $this->expectException(\InvalidArgumentException::class);
    }

    protected function whenPathResolverIsGivenLanguageMapWithoutDefaultLanguage(): void
    {
        new PathResolver([], 2);
    }

    /**
     * @param $url
     */
    protected function theResolvedLanguageShouldBeEnglish($url): void
    {
        $language = $this->resolver->resolveFromUrl($url);
        $this->assertSame(1, $language->getId());
        $this->assertSame("en", $language->toString());
        $this->assertSame("en", (string)$language);
    }
}