<?php declare(strict_types=1);

namespace Hazadam\Router\Validation;

use Hazadam\Router\Validation\Interfaces\SeparatorValidatorInterface;

/**
 * Class SeparatorValidator
 * @package Hazadam\Router\Validation
 */
class SeparatorValidator implements SeparatorValidatorInterface
{
    /**
     * @var array
     */
    protected $separatorsMap = [];

    /**
     * SeparatorValidator constructor.
     * @param array $separatorsMap
     */
    public function __construct(array $separatorsMap = [])
    {
        $this->separatorsMap = $separatorsMap === [] ? static::DEFAULT_SEPARATORS : $separatorsMap;
    }

    /**
     * @inheritDoc
     */
    public function validate(string $routeType, string $separator): bool
    {
        if (! isset($this->separatorsMap[$routeType])) {

            throw new \InvalidArgumentException(sprintf(
                "Cannot validate for unknown routable type: %s", $routeType
            ));
        }

        $isValid = in_array($separator, $this->separatorsMap[$routeType]);

        return $isValid;
    }
}