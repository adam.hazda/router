<?php declare(strict_types=1);

namespace Hazadam\Router\Validation\Interfaces;

/**
 * Interface SeparatorValidatorInterface
 * @package Hazadam\Router\Validation\Interfaces
 */
interface SeparatorValidatorInterface
{
    /**
     * @var string
     */
    const ARGUMENT_NAME = 'argument_name';

    /**
     * @var string
     */
    const ARGUMENT_VALUE = 'argument_value';

    /**
     * @var array
     */
    const DEFAULT_SEPARATORS = [
        self::ARGUMENT_NAME => ['/', '?', '&'],
        self::ARGUMENT_VALUE => ['=', ','],
    ];

    /**
     * @param string $routeType
     * @param string $separator
     * @return bool
     */
    public function validate(string $routeType, string $separator): bool;
}