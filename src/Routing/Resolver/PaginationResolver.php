<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver;

use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;
use Hazadam\Router\Net\Uri\Url;
use Hazadam\Router\Routing\Resolver\Interfaces\PaginationResolverInterface;

/**
 * Class PaginationResolver
 * @package Hazadam\Router\Routing\Resolver
 */
class PaginationResolver implements PaginationResolverInterface
{
    /**
     * @inheritDoc
     */
    const REGEX_PATTERN = "#^\/(\d+)$#";

    /**
     * @param IdentifierCollectionInterface $collection
     * @return int|null
     */
    public function resolvePagination(IdentifierCollectionInterface $collection): ?int
    {
        $page = null;
        /** @var IdentifierInterface $identifier */
        foreach ($collection as $identifier) {

            $path = $identifier->getSeparator() . $identifier->getString();
            preg_match(self::REGEX_PATTERN, $path, $matches);
            $possiblePage = end($matches);

            if (count($matches) > 1 && count($collection->getByString($possiblePage)) > 0) {

                $page = intval($possiblePage);
                $collection->remove($identifier);
                break;
            }
        }

        return $page;
    }
}