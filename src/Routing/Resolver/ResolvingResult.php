<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver;

use Hazadam\Router\Net\Interfaces\EndpointInterface;
use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;

/**
 * Class ResolvingResult
 * @package Hazadam\Router\Routing\Resolver
 */
class ResolvingResult
{
    /**
     * @var RouteCollectionInterface
     */
    protected $resolvedRoutes;

    /**
     * @var IdentifierCollectionInterface
     */
    protected $unrecognizedIdentifiers;

    /**
     * @var ResourceCollectionInterface
     */
    protected $resourceCollection;

    /**
     * @var string
     */
    protected $controller;

    /**
     * ResolvingResult constructor.
     * @param RouteCollectionInterface $resolvedRoutes
     * @param IdentifierCollectionInterface $unrecognizedIdentifiers
     * @param ResourceCollectionInterface $resourceCollection
     * @param string $controller
     */
    public function __construct(
        RouteCollectionInterface $resolvedRoutes, IdentifierCollectionInterface $unrecognizedIdentifiers,
        ResourceCollectionInterface $resourceCollection, string $controller
    )
    {
        $this->resolvedRoutes = $resolvedRoutes;
        $this->unrecognizedIdentifiers = $unrecognizedIdentifiers;
        $this->resourceCollection = $resourceCollection;
        $this->controller = $controller;
    }

    /**
     * @return RouteCollectionInterface
     */
    public function getResolvedRoutes(): RouteCollectionInterface
    {
        return $this->resolvedRoutes;
    }

    /**
     * @return IdentifierCollectionInterface
     */
    public function getUnrecognizedIdentifiers(): IdentifierCollectionInterface
    {
        return $this->unrecognizedIdentifiers;
    }

    /**
     * @return ResourceCollectionInterface
     */
    public function getResourceCollection(): ResourceCollectionInterface
    {
        return $this->resourceCollection;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }
}