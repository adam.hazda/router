<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver;

use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\PaginatedResource;
use Hazadam\Router\Net\PairedResourceSpecifier;
use Hazadam\Router\Net\ResourceCollection;
use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;
use Hazadam\Router\Routing\Resolver\Interfaces\PaginationResolverInterface;
use Hazadam\Router\Routing\Resolver\Interfaces\RoutesResolverInterface;
use Hazadam\Router\Validation\Interfaces\SeparatorValidatorInterface;

/**
 * Class RoutesResolver
 * @package Hazadam\Router\Routing\Resolver
 */
class RoutesResolver implements RoutesResolverInterface
{
    /**
     * @var SeparatorValidatorInterface
     */
    protected $separatorValidator;

    /**
     * @var PaginationResolverInterface
     */
    protected $paginationResolver;

    /**
     * @var array
     */
    const PAIRED_VALUES_SEPARATORS = [',', '='];

    /**
     * RoutesResolver constructor.
     * @param SeparatorValidatorInterface $separatorValidator
     * @param PaginationResolverInterface $paginationResolver
     */
    public function __construct(
        SeparatorValidatorInterface $separatorValidator, PaginationResolverInterface $paginationResolver
    )
    {
        $this->separatorValidator = $separatorValidator;
        $this->paginationResolver = $paginationResolver;
    }

    /**
     * @inheritDoc
     */
    public function resolveRoutes(
        RouteCollectionInterface $recognizedRoutes, IdentifierCollectionInterface $identifierCollection
    ): ResolvingResult
    {
        $controller = '';
        $resolvedRoutes = new RouteCollection();
        $resourceCollection = new ResourceCollection();
        list($rootRoute, $rootIdentifier) = $this->findRoots($recognizedRoutes, $identifierCollection);

        if ($rootIdentifier instanceof IdentifierInterface && $rootRoute instanceof RouteInterface) {

            $this->validateRootRoute($rootRoute);
            $resolvedRoutes->add($rootRoute);
            $controller = $rootRoute->getController();
            $identifierCollection->remove($rootIdentifier);

            if ($rootRoute->getResource()) {

                $this->processSubRoute(
                    $rootRoute, $recognizedRoutes, $resolvedRoutes, $resourceCollection, $identifierCollection
                );
                $this->checkPagination($identifierCollection, $rootRoute);
                $this->registerRootRouteResources($resourceCollection, $rootRoute);
            }
        }

        return new ResolvingResult($resolvedRoutes, $identifierCollection, $resourceCollection, $controller);
    }

    /**
     * @param RouteInterface $parentRoute
     * @param RouteCollectionInterface $recognizedRoutes
     * @param RouteCollectionInterface $resolvedRoutes
     * @param ResourceCollectionInterface $resourceCollection
     * @param IdentifierCollectionInterface $resolvedIdentifiersFromUri
     * @return int
     */
    protected function processSubRoute(
        RouteInterface $parentRoute, RouteCollectionInterface $recognizedRoutes, RouteCollectionInterface $resolvedRoutes,
        ResourceCollectionInterface $resourceCollection, IdentifierCollectionInterface $resolvedIdentifiersFromUri
    ): int
    {
        list($routesToProcess, $parentResourceSpecifier) = $this->extractSubRoutes($parentRoute, $recognizedRoutes);

        $resolvedRoutesCount = 0;
        /** @var RouteInterface $subRoute */
        foreach ($routesToProcess as $subRoute) {

            if ($subRoute->getResource())
                continue;

            $associatedIdentifier = $this->giveValidIdentifier($resolvedIdentifiersFromUri, $subRoute);

            if (null === $associatedIdentifier)
                continue;

            $resourceSpecifier = $this->extractResourceSpecifierFromNonRootRoute($subRoute);

            if ($this->checkSingleOccurrenceConstraint($resolvedRoutes, $resourceSpecifier))
                continue;

            $resolvedSubRoutesCount = $this->processSubRoute(
                $subRoute, $recognizedRoutes, $resolvedRoutes, $resourceCollection, $resolvedIdentifiersFromUri
            );

            if (empty($resolvedRoutes->getByResourceSpecifierId($resourceSpecifier->getId()))) {

                $routeToAdd = null;

                switch ($resourceSpecifier->getType()) {

                    case ResourceSpecifierInterface::TYPE_BOUND:

                        if ($resolvedSubRoutesCount > 0) {

                            $routeToAdd = $subRoute;
                        }

                        break;
                    case ResourceSpecifierInterface::TYPE_STANDALONE:

                        if (! $parentResourceSpecifier
                            ||
                            ($parentResourceSpecifier && $this->checkSingleValuedConstraint($resolvedRoutes, $parentResourceSpecifier))
                        ) {
                            $routeToAdd = $subRoute;
                        }

                        break;
                    case ResourceSpecifierInterface::TYPE_PAIRED:

                        $pairedValues = $this->parsePossiblePairedValues(
                            $resourceSpecifier, $resolvedIdentifiersFromUri, $associatedIdentifier
                        );

                        if (! empty($pairedValues)) {

                            $resourceSpecifier = new PairedResourceSpecifier($resourceSpecifier, $pairedValues);
                            $subRoute->setResourceSpecifiers([$resourceSpecifier]);
                            $routeToAdd = $subRoute;
                        }

                        break;
                }

                if ($routeToAdd) {

                    $resolvedRoutes->add($routeToAdd);
                    $resourceCollection->add($resourceSpecifier);
                    $resolvedIdentifiersFromUri->remove($associatedIdentifier);
                    $resolvedRoutesCount++;
                }
            }
        }

        return $resolvedRoutesCount;
    }

    /**
     * @param RouteCollectionInterface $resolvedRoutes
     * @param $parentResourceSpecifier
     * @return bool
     */
    protected function checkSingleValuedConstraint(
        RouteCollectionInterface $resolvedRoutes, ResourceSpecifierInterface $parentResourceSpecifier
    ): bool
    {
        $isSingleValuedViolated = true;

        if ($parentResourceSpecifier->isSingleValued()) {

            $isSingleValuedViolated = empty($resolvedRoutes->getByParentResourceSpecifierId(
                $parentResourceSpecifier->getId()
            ));
        }

        return $isSingleValuedViolated;
    }

    /**
     * @param IdentifierCollectionInterface $resolvedIdentifiersFromUri
     * @param RouteInterface $route
     * @return IdentifierInterface|null
     */
    protected function giveValidIdentifier(
        IdentifierCollectionInterface $resolvedIdentifiersFromUri, RouteInterface $route
    ): ?IdentifierInterface
    {
        $validIdentifier = null;
        /** @var IdentifierInterface[] $identifiers */
        $identifiers = $resolvedIdentifiersFromUri->getByString($route->getIdentifier()->getString());

        foreach ($identifiers as $identifier) {

            if ($this->separatorValidator->validate($route->getType(), $identifier->getSeparator())) {

                $validIdentifier = $identifier;
                break;
            }
        }

        return $validIdentifier;
    }

    /**
     * @param RouteCollectionInterface $resolvedRoutes
     * @param ResourceSpecifierInterface $resourceSpecifier
     * @return bool
     */
    protected function checkSingleOccurrenceConstraint(
        RouteCollectionInterface $resolvedRoutes, ResourceSpecifierInterface $resourceSpecifier
    ): bool
    {
        $singleOccurrenceIsViolated = $resourceSpecifier->isSingleOccurrence()
            &&
            !empty($resolvedRoutes->getByRoutableType($resourceSpecifier->getRoutableType()));

        return $singleOccurrenceIsViolated;
    }

    /**
     * @param RouteInterface $subRoute
     * @return ResourceSpecifierInterface|null
     */
    protected function extractResourceSpecifierFromNonRootRoute(RouteInterface $subRoute): ?ResourceSpecifierInterface
    {
        /** @var ResourceSpecifierInterface[] $resourceSpecifiers */
        $resourceSpecifiers = $subRoute->getResourceSpecifiers();
        $resourceSpecifier = end($resourceSpecifiers);

        if (count($resourceSpecifiers) > 1) {

            throw new \LogicException(sprintf(
                "A non-root Route MUST NOT have more than 1 ResourceSpecifier," .
                " this Route '%s' is misconfigured", $subRoute->getId()
            ));
        }

        return $resourceSpecifier ?? null;
    }

    /**
     * @param RouteInterface $parentRoute
     * @param RouteCollectionInterface $recognizedRoutes
     * @return array
     */
    protected function extractSubRoutes(RouteInterface $parentRoute, RouteCollectionInterface $recognizedRoutes): array
    {
        $routesToProcess = [];

        if ($parentRoute->getResource()) {

            $routesToProcess = $recognizedRoutes->getByParentResourceId($parentRoute->getResource()->getId());

        } else {

            $resourceSpecifier = $this->extractResourceSpecifierFromNonRootRoute($parentRoute);

            if ($resourceSpecifier !== null) {

                $routesToProcess = $recognizedRoutes->getByParentResourceSpecifierId($resourceSpecifier->getId());
            }
        }

        return [$routesToProcess, $resourceSpecifier ?? null];
    }

    /**
     * @param ResourceSpecifierInterface $resourceSpecifier
     * @param IdentifierCollectionInterface $resolvedIdentifiersFromUri
     * @param IdentifierInterface $associatedIdentifier
     * @return array
     */
    protected function parsePossiblePairedValues(
        ResourceSpecifierInterface $resourceSpecifier, IdentifierCollectionInterface $resolvedIdentifiersFromUri,
        IdentifierInterface $associatedIdentifier
    ): array
    {
        $pairedValues = [];
        $nextIdentifier = $resolvedIdentifiersFromUri->getNext($associatedIdentifier);

        if ($nextIdentifier && in_array($nextIdentifier->getSeparator(), self::PAIRED_VALUES_SEPARATORS)) {

            $pairedValues[] = $nextIdentifier->getString();

            if (! $resourceSpecifier->isSingleValued()) {

                $nextValues = $this->parsePossiblePairedValues($resourceSpecifier, $resolvedIdentifiersFromUri, $nextIdentifier);
                $pairedValues = array_merge($pairedValues, $nextValues);
            }

            $resolvedIdentifiersFromUri->remove($nextIdentifier);
        }

        return $pairedValues;
    }

    /**
     * @param RouteCollectionInterface $recognizedRoutes
     * @param IdentifierCollectionInterface $identifierCollection
     * @return array
     */
    protected function findRoots(
        RouteCollectionInterface $recognizedRoutes, IdentifierCollectionInterface $identifierCollection
    ): array
    {
        $rootRoutes = [];
        $rootIdentifier = null;

        /** Identifiers have to come in order as they appeared within URI */
        /** @var IdentifierInterface $identifier */
        foreach ($identifierCollection->getIterator() as $identifier) {

            $rootRoutes = $recognizedRoutes->getByIdentifier($identifier->getString());

            if (!empty($rootRoutes)) {

                $rootIdentifier = $identifier;
                break;
            }
        }

        if (count($rootRoutes) > 1) {

            throw new \LogicException(sprintf(
                "There are more than 1 Route for root Identifier '%s'," .
                " that is some serious data inconsistency, please fix it", $rootIdentifier->getString()
            ));
        }

        $rootRoute = end($rootRoutes);

        return array($rootRoute, $rootIdentifier);
    }

    /**
     * @param RouteInterface $rootRoute
     */
    protected function validateRootRoute(RouteInterface $rootRoute): void
    {
        if ($rootRoute->getPosition() !== RouteInterface::POSITION_PATH) {

            throw new \LogicException(sprintf(
                "Root Routes cannot be positioned in query string, this Route '%s' is misconfigured",
                $rootRoute->getId()
            ));
        }

        $hasResource = (bool) $rootRoute->getResource();
        $hasEndpoint = (bool) $rootRoute->getController();
        $doesNotHaveResource = ! $hasResource;
        $doesNotHaveEndpoint = ! $hasEndpoint;

        if (($hasResource && $doesNotHaveEndpoint) || ($doesNotHaveResource && $doesNotHaveEndpoint)) {

            throw new \LogicException(sprintf(
                "Root Routes have to have both Resource and Endpoint, this Route '%s' is misconfigured",
                $rootRoute->getId()
            ));
        }
    }

    /**
     * @param ResourceCollectionInterface $resourceCollection
     * @param RouteInterface $rootRoute
     */
    protected function registerRootRouteResources(
        ResourceCollectionInterface $resourceCollection, RouteInterface $rootRoute
    ): void
    {
        $resourceCollection->add($rootRoute->getResource());

        foreach ($rootRoute->getResourceSpecifiers() as $resourceSpecifier) {

            $resourceCollection->add($resourceSpecifier);
        }
    }

    /**
     * @param IdentifierCollectionInterface $identifierCollection
     * @param RouteInterface $rootRoute
     */
    protected function checkPagination(
        IdentifierCollectionInterface $identifierCollection, RouteInterface $rootRoute
    ): void
    {
        $page = $this->paginationResolver->resolvePagination($identifierCollection);

        if (is_int($page) && $page > 0) {

            $rootResource = $rootRoute->getResource();
            $paginatedResource = new PaginatedResource($rootResource, $page);
            $rootRoute->setResource($paginatedResource);
        }
    }
}