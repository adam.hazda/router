<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver\Interfaces;

use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Interface PaginationResolverInterface
 * @package Hazadam\Router\Routing\Resolver\Interfaces
 */
interface PaginationResolverInterface
{
    /**
     * @param IdentifierCollectionInterface $collection
     * @return int
     */
    public function resolvePagination(IdentifierCollectionInterface $collection): ?int;
}