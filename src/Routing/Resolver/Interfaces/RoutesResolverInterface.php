<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Resolver\Interfaces;

use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Routing\Resolver\ResolvingResult;

/**
 * Interface RoutesResolverInterface
 * @package Hazadam\Router\Routing\Resolver\Interfaces
 */
interface RoutesResolverInterface
{
    /**
     * @param RouteCollectionInterface $recognizedRoutes
     * @param IdentifierCollectionInterface $identifierCollection
     * @return ResolvingResult
     */
    public function resolveRoutes(
        RouteCollectionInterface $recognizedRoutes, IdentifierCollectionInterface $identifierCollection
    ): ResolvingResult;
}