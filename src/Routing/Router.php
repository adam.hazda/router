<?php declare(strict_types=1);

namespace Hazadam\Router\Routing;

use Hazadam\Router\Language\Resolving\Interfaces\LanguageResolverInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\ResourceUrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Resolving\Interfaces\IdentifierResolverInterface;
use Hazadam\Router\Net\Uri\Url;
use Hazadam\Router\Routing\Interfaces\RouterInterface;
use Hazadam\Router\Routing\Resolver\Interfaces\RoutesResolverInterface;
use Hazadam\Router\Storage\Repository\Interfaces\RouteRepositoryInterface;

/**
 * Class Router
 * @package Hazadam\Router\Routing
 */
class Router implements RouterInterface
{
    /**
     * @var LanguageResolverInterface
     */
    protected $languageResolver;

    /**
     * @var IdentifierResolverInterface
     */
    protected $identifierResolver;

    /**
     * @var RouteRepositoryInterface
     */
    protected $routeRepository;

    /**
     * @var RoutesResolverInterface
     */
    protected $routesResolver;

    /**
     * @var ResourceUrlGeneratorInterface
     */
    protected $resourceUrlGenerator;

    /**
     * Router constructor.
     * @param LanguageResolverInterface $languageResolver
     * @param IdentifierResolverInterface $identifierResolver
     * @param RouteRepositoryInterface $routeRepository
     * @param RoutesResolverInterface $routesResolver
     * @param ResourceUrlGeneratorInterface $resourceUrlGenerator
     */
    public function __construct(
        LanguageResolverInterface $languageResolver, IdentifierResolverInterface $identifierResolver,
        RouteRepositoryInterface $routeRepository, RoutesResolverInterface $routesResolver,
        ResourceUrlGeneratorInterface $resourceUrlGenerator
    )
    {
        $this->languageResolver = $languageResolver;
        $this->identifierResolver = $identifierResolver;
        $this->routeRepository = $routeRepository;
        $this->routesResolver = $routesResolver;
        $this->resourceUrlGenerator = $resourceUrlGenerator;
    }

    /**
     * @inheritDoc
     */
    public function match(Url $url, string $httpMethod, $domainId = null): RoutingResult
    {
        $language = $this->languageResolver->resolveFromUrl($url);
        $identifiersCollection = $this->identifierResolver->resolveIdentifiers($url);
        $identifiers = $identifiersCollection->getAll();
        $recognizedRoutes = $this->routeRepository->fetchRoutesByIdentifiers(
            $identifiers, $httpMethod, $language->getId()
        );
        $resolvingResult = $this->routesResolver->resolveRoutes($recognizedRoutes, $identifiersCollection);
        $resourceCollection = $resolvingResult->getResourceCollection();
        $resourceSet = $resourceCollection->toResourceSet();
        $generatedUrl = null;

        if ($resourceSet->getRootResource()) {

            $availableRoutes = $this->routeRepository->fetchRoutesByResources(
                $resourceSet->getRootResource(), $resourceSet->getResourceSpecifiers(),
                $httpMethod, $language->getId(), $domainId
            );
            $generatedUrl = $this->resourceUrlGenerator->generate($resourceCollection, $availableRoutes);
        }

        $routingResult = new RoutingResult($resolvingResult, $url, $generatedUrl);

        return $routingResult;
    }
}