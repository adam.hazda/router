<?php declare(strict_types=1);

namespace Hazadam\Router\Routing;

use Hazadam\Router\Net\Uri\Url;
use Hazadam\Router\Routing\Resolver\ResolvingResult;

/**
 * Class RoutingResult
 * @package Hazadam\Router\Routing
 */
class RoutingResult
{
    /**
     * @var ResolvingResult
     */
    protected $resolvingResult;

    /**
     * @var Url
     */
    protected $requestedUrl;

    /**
     * @var Url|null
     */
    protected $generatedUrl;

    /**
     * RoutingResult constructor.
     * @param ResolvingResult $resolvingResult
     * @param Url $requestedUrl
     * @param Url|null $generatedUrl
     */
    public function __construct(ResolvingResult $resolvingResult, Url $requestedUrl, ?Url $generatedUrl)
    {
        $this->resolvingResult = $resolvingResult;
        $this->requestedUrl = $requestedUrl;
        $this->generatedUrl = $generatedUrl;
    }

    /**
     * @return ResolvingResult
     */
    public function getResolvingResult(): ResolvingResult
    {
        return $this->resolvingResult;
    }

    /**
     * @return Url
     */
    public function getRequestedUrl(): Url
    {
        return $this->requestedUrl;
    }

    /**
     * @return Url|null
     */
    public function getGeneratedUrl(): ?Url
    {
        return $this->generatedUrl;
    }
}