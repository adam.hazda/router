<?php declare(strict_types=1);

namespace Hazadam\Router\Routing\Interfaces;

use Hazadam\Router\Net\Uri\Url;
use Hazadam\Router\Routing\RoutingResult;

/**
 * Interface RouterInterface
 * @package Hazadam\Router\Routing\Interfaces
 */
interface RouterInterface
{
    /**
     * @param Url $url
     * @param string $httpMethod
     * @param null $domainId
     * @return RoutingResult
     */
    public function match(Url $url, string $httpMethod, $domainId = null): RoutingResult;
}