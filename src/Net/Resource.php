<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Misc\DataType\UniquelyIdentifiableTrait;

/**
 * Class Resource
 * @package Hazadam\Router
 */
class Resource implements ResourceInterface
{
    use UniquelyIdentifiableTrait, RoutableTypeTrait;

    /**
     * Resource constructor.
     * @param $id
     * @param string $routableType
     */
    public function __construct($id, string $routableType)
    {
        $this->id = $id;
        $this->routableType = $routableType;
    }
}