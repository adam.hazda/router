<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Resolving;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;
use Hazadam\Router\Net\Uri\Identifier;
use Hazadam\Router\Net\Uri\IdentifierCollection;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Resolving\Interfaces\IdentifierResolverInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class IdentifierResolver
 * @package Hazadam\Router\Net\Uri\Resolving
 */
class IdentifierResolver implements IdentifierResolverInterface
{
    /**
     * @var string
     */
    const ROOT = '/';

    /**
     * @var string
     */
    const REGEX_PATTERN = "#(.)([^\/\=\,]+)#";

    /**
     * @var string
     */
    const QS_SEPARATOR = '?';

    /**
     * @var string
     */
    const EQ_SIGN = '=';

    /**
     * @var string
     */
    const QS_PARAMETERS_GLUE = '&';

    /**
     * @inheritDoc
     */
    public function resolveIdentifiers(Url $url): IdentifierCollectionInterface
    {
        $collection = $this->createCollection();
        /**
         * The order of these method calls is FREAKING IMPORTANT DON'T CHANGE IT!
         */
        $this->addQueryStringControllerIdentifiers($url, $collection);
        $this->addAllPathIdentifiers($url, $collection);
        $this->addQueryStringPartialIdentifiers($url, $collection);

        return $collection;
    }

    /**
     * @param Url $url
     * @return array
     */
    protected function parseQueryString(Url $url): array
    {
        $queryString = $url->getQuery() ?? '';
        parse_str($queryString, $queryParameters);

        return $queryParameters;
    }

    /**
     * @return IdentifierCollection
     */
    protected function createCollection(): IdentifierCollection
    {
        $collection = new IdentifierCollection();

        return $collection;
    }

    /**
     * @param string $string
     * @param string $separator
     * @return Identifier
     */
    protected function createIdentifier(string $string, string $separator): Identifier
    {
        $identifier = new Identifier($string, $separator);

        return $identifier;
    }

    /**
     * @param Url $url
     * @param CollectionInterface $collection
     */
    protected function addQueryStringControllerIdentifiers(Url $url, CollectionInterface $collection): void
    {
        $string = self::QS_SEPARATOR . $url->getQuery();

        if ($string !== self::QS_SEPARATOR) {

            $collection->add(new Identifier($url->getPath() . $string, ''));
        }

        $parameters = explode(self::QS_PARAMETERS_GLUE, $string);

        for ($index = count($parameters) - 1; $index >= 0; $index--) {

            $length = strlen(self::QS_PARAMETERS_GLUE . $parameters[$index]);
            $string = substr($string, 0, 0 - $length);

            if (is_string($string) && strlen($string) > 1) {

                $collection->add(new Identifier($url->getPath() . $string, ''));
            }
        }
    }

    /**
     * @param Url $url
     * @param CollectionInterface $collection
     */
    protected function addAllPathIdentifiers(Url $url, CollectionInterface $collection): void
    {
        preg_match_all(self::REGEX_PATTERN, $url->getPath(), $matches);
        $identifiers = $matches[0] ?? [];
        $separators = $matches[1] ?? [];
        $trimmedIdentifiers = $matches[2] ?? [];

        $this->addMainPathIdentifier($url, $collection);
        $this->addPathControllerIdentifiers($url, $identifiers, $collection);
        $this->addPathRootIdentifier($url, $collection);
        $this->addPathPartialIdentifiers($trimmedIdentifiers, $separators, $collection);
    }

    /**
     * @param Url $url
     * @param $collection
     */
    protected function addQueryStringPartialIdentifiers(Url $url, CollectionInterface $collection): void
    {
        $queryParameters = $this->parseQueryString($url);
        $separator = self::QS_SEPARATOR;

        foreach ($queryParameters as $key => $value) {

            $collection->add($this->createIdentifier($key, $separator));

            if (is_array($value)) {

                foreach ($value as $singleValue) {

                    if (is_array($singleValue))
                        continue;

                    $collection->add($this->createIdentifier($singleValue, self::EQ_SIGN));
                }
            } else {

                $collection->add(new Identifier($value, self::EQ_SIGN));
            }

            $separator = self::QS_PARAMETERS_GLUE;
        }
    }

    /**
     * @param Url $url
     * @param CollectionInterface $collection
     */
    protected function addPathRootIdentifier(Url $url, CollectionInterface $collection): void
    {
        if ($url->getPath() !== '') {

            $collection->add($this->createIdentifier(self::ROOT, ''));
        }
    }

    /**
     * @param Url $url
     * @param CollectionInterface $collection
     */
    protected function addMainPathIdentifier(Url $url, CollectionInterface $collection): void
    {
        if ($url->getPath() !== self::ROOT && $url->getPath() !== '') {

            $collection->add($this->createIdentifier($url->getPath(), ''));
        }
    }

    /**
     * @param Url $url
     * @param array $identifiers
     * @param CollectionInterface $collection
     */
    protected function addPathControllerIdentifiers(Url $url, array $identifiers, CollectionInterface $collection): void
    {
        $string = rtrim($url->getPath(), self::ROOT);

        for ($index = count($identifiers) - 1; $index >= 0; $index--) {

            $length = strlen($identifiers[$index]);
            $string = substr($string, 0, 0 - $length);

            if (is_string($string) && strlen($string) > 1) {

                $collection->add(new Identifier($string, ''));
            }
        }
    }

    /**
     * @param array $partialIdentifiers
     * @param array $separators
     * @param CollectionInterface $collection
     */
    protected function addPathPartialIdentifiers(
        array $partialIdentifiers, array $separators, CollectionInterface $collection
    ): void
    {
        $trimmedIdentifiersCount = count($partialIdentifiers);

        for ($index = 0; $index < $trimmedIdentifiersCount; $index++) {

            $collection->add($this->createIdentifier($partialIdentifiers[$index], $separators[$index]));
        }
    }
}