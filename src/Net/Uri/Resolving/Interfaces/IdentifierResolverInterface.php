<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Resolving\Interfaces;

use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Interface IdentifierResolverInterface
 * @package Hazadam\Router\Net\Uri\Interfaces
 */
interface IdentifierResolverInterface
{
    /**
     * @param Url $url
     * @return IdentifierCollectionInterface
     */
    public function resolveIdentifiers(Url $url): IdentifierCollectionInterface;
}