<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri;

use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;

/**
 * Class Identifier
 * @package Hazadam\Router\Net\Uri
 */
class Identifier implements IdentifierInterface
{
    /**
     * @var string
     */
    protected $string;

    /**
     * @var string
     */
    protected $separator;

    /**
     * Identifier constructor.
     * @param string $string
     * @param string $separator
     */
    public function __construct(string $string, string $separator)
    {
        $this->string = $string;
        $this->separator = $separator;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }

    /**
     * @inheritDoc
     */
    public function setString(string $string): void
    {
        $this->string = $string;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }
}