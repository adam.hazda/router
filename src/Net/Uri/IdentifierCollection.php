<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri;

use Hazadam\Router\Misc\DataType\UniqueStrongTypeAbstractCollection;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierCollectionInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;

/**
 * Class IdentifierCollection
 * @package Hazadam\Router\Net\Uri
 */
class IdentifierCollection extends UniqueStrongTypeAbstractCollection implements IdentifierCollectionInterface
{
    /**
     * @var array
     */
    protected $stringMap = [];

    /**
     * @inheritDoc
     */
    public function __construct(string $dataType = IdentifierInterface::class)
    {
        parent::__construct($dataType);
    }

    /**
     * @param string $string
     * @return IdentifierInterface[]
     */
    public function getByString(string $string): array
    {
        $keys = $this->stringMap[$string] ?? [];
        $items = array_intersect_key($this->objects, $keys);

        return $items ?: [];
    }

    /**
     * @inheritDoc
     */
    public function getNext(IdentifierInterface $identifier): ?IdentifierInterface
    {
        $nextIdentifier = null;
        $hash = spl_object_id($identifier);
        $items = $this->items;
        reset($items);

        do {
            if ($items[key($items)] === $hash) {

                next($items);

                if (current($items)) {

                    $nextIdentifier = $this->objects[key($items)];
                    break;
                }
            }

            next($items);
        } while (current($items));

        return $nextIdentifier;
    }

    /**
     * @param $key
     * @param $item
     */
    protected function onAdd($key, $item): void
    {
        /** @var IdentifierInterface $item */
        $item = $this->objects[$key];
        $this->stringMap[$item->getString()][$key] = null;
    }

    /**
     * @param $key
     * @param $item
     */
    protected function onRemove($key, $item): void
    {
        /** @var IdentifierInterface $item */
        $item = $this->objects[$key];
        unset($this->stringMap[$item->getString()][$key]);
    }
}