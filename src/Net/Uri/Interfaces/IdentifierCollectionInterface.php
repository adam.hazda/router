<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Interfaces;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;


/**
 * Class IdentifierCollection
 * @package Hazadam\Router\Net\Uri
 */
interface IdentifierCollectionInterface extends CollectionInterface
{
    /**
     * @param string $string
     * @return IdentifierInterface[]
     */
    public function getByString(string $string): array;

    /**
     * @param IdentifierInterface $identifier
     * @return IdentifierInterface|null
     */
    public function getNext(IdentifierInterface $identifier): ?IdentifierInterface;
}