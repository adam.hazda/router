<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Interfaces;

/**
 * Interface IdentifierInterface
 * @package Hazadam\Net\Uri\Interfaces
 */
interface IdentifierInterface
{
    /**
     * @return string
     */
    public function getString(): string;

    /**
     * @param string $string
     */
    public function setString(string $string): void;

    /**
     * @return string
     */
    public function getSeparator(): string;
}