<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Interfaces;

use Hazadam\Router\Net\Uri\Url;

/**
 * Interface UrlBuilderInterface
 * @package Hazadam\Net\Uri\Interfaces
 */
interface UrlBuilderInterface
{
    /**
     * @param string $url
     * @return Url
     */
    public function buildFromString(string $url): Url;

    /**
     * @param Url $url
     * @return string
     */
    public function urlToString(Url $url): string;
}