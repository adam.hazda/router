<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri;

/**
 * Class Url
 * @package Hazadam\Net\Uri
 */
class Url
{
    /**
     * @var string|null
     */
    protected $scheme;

    /**
     * @var string|null
     */
    protected $user;

    /**
     * @var string|null
     */
    protected $pass;

    /**
     * @var string|null
     */
    protected $host;

    /**
     * @var int|null
     */
    protected $port;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string|null
     */
    protected $query;

    /**
     * @var string|null
     */
    protected $fragment;

    /**
     * Url constructor.
     * @param null|string $scheme
     * @param null|string $user
     * @param null|string $pass
     * @param null|string $host
     * @param int|null $port
     * @param string $path
     * @param null|string $query
     * @param null|string $fragment
     */
    public function __construct(
        ?string $scheme, ?string $user, ?string $pass, ?string $host,
        ?int $port, string $path, ?string $query, ?string $fragment
    )
    {
        $this->scheme = $scheme;
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
        $this->port = $port;
        $this->path = $path;
        $this->query = $query;
        $this->fragment = $fragment;
    }


    /**
     * @return null|string
     */
    public function getScheme(): ?string
    {
        return $this->scheme;
    }

    /**
     * @return null|string
     */
    public function getUser(): ?string
    {
        return $this->user;
    }

    /**
     * @return null|string
     */
    public function getPass(): ?string
    {
        return $this->pass;
    }

    /**
     * @return null|string
     */
    public function getHost(): ?string
    {
        return $this->host;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return null|string
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @param string|null $query
     */
    public function setQuery($query): void
    {
        $this->query = $query;
    }

    /**
     * @return null|string
     */
    public function getFragment(): ?string
    {
        return $this->fragment;
    }
}