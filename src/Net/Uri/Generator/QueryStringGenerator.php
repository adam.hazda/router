<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Composition\Identifier;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\CompositionInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\UrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class QueryStringGenerator
 * @package Hazadam\Router\Net\Uri\Generator
 */
class QueryStringGenerator implements UrlGeneratorInterface
{
    /**
     * @inheritDoc
     */
    public function generate(CompositionInterface $composition): Url
    {
        $queryString = null;
        $rootIdentifier = $composition->getRootIdentifier();

        if ($rootIdentifier !== null) {

            $parameters = [];
            /** @var Identifier $childIdentifier */
            foreach ($composition->getChildIdentifiersIds($rootIdentifier->getId()) as $childIdentifierId) {

                $childSubIdentifiers = $composition->getChildIdentifiersIds($childIdentifierId);
                $parameters = $this->populateParameters($composition, $childSubIdentifiers, $parameters, $childIdentifierId);
            }

            $queryString = $rootIdentifier->getString() . http_build_query($parameters);
        }

        return $this->createUrlFromQuery($queryString);
    }

    /**
     * @param $query
     * @return Url
     */
    protected function createUrlFromQuery($query): Url
    {
        $url = new Url(null, null, null, null, null, '', $query, null);

        return $url;
    }

    /**
     * @param CompositionInterface $composition
     * @param array $childSubIdentifiers
     * @param array $parameters
     * @param $childIdentifierId
     * @return array
     */
    protected function populateParameters(
        CompositionInterface $composition, array $childSubIdentifiers, array $parameters, $childIdentifierId
    ): array
    {


        if (count($childSubIdentifiers) === 1) {

            $childSubIdentifier = end($childSubIdentifiers);
            $parameters[$composition->get($childIdentifierId)->getString()] = $composition->get($childSubIdentifier)->getString();

        } elseif (count($childSubIdentifiers) > 1) {

            foreach ($childSubIdentifiers as $childSubIdentifier) {

                $parameters[$composition->get($childIdentifierId)->getString()][] = $composition->get($childSubIdentifier)->getString();
            }
        } else {

            $parameters[$composition->get($childIdentifierId)->getString()] = true;
        }

        return $parameters;
    }
}