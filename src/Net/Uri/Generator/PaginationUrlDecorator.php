<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Interfaces\PaginationUrlDecoratorInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class PaginationUrlDecorator
 * @package Hazadam\Router\Net\Uri\Generator
 */
class PaginationUrlDecorator implements PaginationUrlDecoratorInterface
{
    /**
     * @inheritDoc
     */
    const PAGINATION_SEPARATOR = '/';

    /**
     * @inheritdoc
     */
    public function addPage(Url $url, int $page): void
    {
        $url->setPath(rtrim($url->getPath(), self::PAGINATION_SEPARATOR) . self::PAGINATION_SEPARATOR . $page);
    }
}