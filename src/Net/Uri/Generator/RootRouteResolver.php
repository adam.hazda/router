<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;
use Hazadam\Router\Net\Uri\Generator\Interfaces\RootRouteResolverInterface;

/**
 * Class RootRouteResolver
 * @package Hazadam\Router\Net\Uri\Generator
 */
class RootRouteResolver implements RootRouteResolverInterface
{
    /**
     * @inheritdoc
     */
    public function resolveRoot(
        ResourceSet $resourceSet, OrderedRouteCollectionInterface $availableRoutes, bool $skipLandingPages = false
    ): ?RouteInterface
    {
        $root = null;

        if ($resourceSet->getRootResource() instanceof ResourceInterface) {

            $root = $this->extractRootRoute(
                $availableRoutes, $resourceSet->getRootResource(),
                array_keys($resourceSet->getResourceSpecifiers()), $skipLandingPages
            );
        }

        return $root;
    }

    /**
     * @param RouteCollectionInterface $availableRoutes
     * @param ResourceInterface $rootResource
     * @param array $resourceSpecifierIds
     * @param bool $skipLandingPages
     * @return RouteInterface
     */
    protected function extractRootRoute(
        RouteCollectionInterface $availableRoutes, ResourceInterface $rootResource,
        array $resourceSpecifierIds, bool $skipLandingPages = false
    ): ?RouteInterface
    {
        $rootRoute = null;

        if (! $skipLandingPages) {

            $rootRoute = $this->searchForLandingPage($availableRoutes, $resourceSpecifierIds);
        }

        if (! $rootRoute) {

            $rootRoutes = $availableRoutes->getSimpleByResourceId($rootResource->getId());

            if (count($rootRoutes) > 1) {
                throw new \LogicException(
                    "There CANNOT be two simple non-landing-page Routes for one Resource, " .
                    "at least not among available routes for generator,.."
                );
            }

            if (! empty($rootRoutes))
                $rootRoute = end($rootRoutes);
        }

        return $rootRoute;
    }

    /**
     * @param RouteCollectionInterface $availableRoutes
     * @param array $resourceSpecifierIds
     * @return RouteInterface|null
     */
    protected function searchForLandingPage(
        RouteCollectionInterface $availableRoutes, array $resourceSpecifierIds
    ): ?RouteInterface
    {
        $possibleLandingPageRoutes = $availableRoutes->getCoveringResourceSpecifierIds($resourceSpecifierIds);

        if (count($possibleLandingPageRoutes) > 1) {
            throw new \LogicException(
                "There CANNOT be two simple non-landing-page Routes for one Resource, " .
                "at least not among available routes for generator,.."
            );
        }

        if (!empty($possibleLandingPageRoutes)) {

            $rootRoute = end($possibleLandingPageRoutes);
        }

        return $rootRoute ?? null;
    }
}