<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Interfaces;

use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\CompositionInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Interface UrlGeneratorInterface
 * @package Hazadam\Router\Net\Uri\Generator\Interfaces
 */
interface UrlGeneratorInterface
{
    /**
     * @param CompositionInterface $composition
     * @return Url
     */
    public function generate(CompositionInterface $composition): Url;
}