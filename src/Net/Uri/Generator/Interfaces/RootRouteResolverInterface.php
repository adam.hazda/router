<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Interfaces;

use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;

/**
 * Interface RootRouteResolverInterface
 * @package Hazadam\Router\Net\Uri\Generator\Interfaces
 */
interface RootRouteResolverInterface
{
    /**
     * @param ResourceSet $resourceSet
     * @param OrderedRouteCollectionInterface $availableRoutes
     * @param bool $skipLandingPages
     * @return RouteInterface|null
     */
    public function resolveRoot(
        ResourceSet $resourceSet, OrderedRouteCollectionInterface $availableRoutes, bool $skipLandingPages = false
    ): ?RouteInterface;
}