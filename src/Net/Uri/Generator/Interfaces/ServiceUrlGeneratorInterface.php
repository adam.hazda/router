<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Interfaces;

use Hazadam\Router\Net\Uri\Url;

/**
 * Interface ServiceUrlGeneratorInterface
 * @package Hazadam\Router\Net\Uri\Generator\Interfaces
 */
interface ServiceUrlGeneratorInterface
{
    /**
     * @param string $serviceName
     * @return Url
     */
    public function generate(string $serviceName): Url;
}