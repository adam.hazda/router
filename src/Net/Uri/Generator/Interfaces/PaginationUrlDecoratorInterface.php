<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Interfaces;

use Hazadam\Router\Net\Uri\Url;

/**
 * Interface PaginationUrlDecoratorInterface
 * @package Hazadam\Router\Net\Uri\Generator\Interfaces
 */
interface PaginationUrlDecoratorInterface
{
    /**
     * @param Url $url
     * @param int $page
     */
    public function addPage(Url $url, int $page): void;
}