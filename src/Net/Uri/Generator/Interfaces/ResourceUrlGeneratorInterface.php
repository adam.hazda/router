<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Interfaces;

use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Interface ResourceUrlGeneratorInterface
 * @package Hazadam\Router\Net\Uri\Generator\Interfaces
 */
interface ResourceUrlGeneratorInterface
{
    /**
     * @param ResourceCollectionInterface $resourcesToGenerateFrom
     * @param OrderedRouteCollectionInterface $availableRoutes
     * @return Url
     */
    public function generate(
        ResourceCollectionInterface $resourcesToGenerateFrom, OrderedRouteCollectionInterface $availableRoutes
    ): Url;
}