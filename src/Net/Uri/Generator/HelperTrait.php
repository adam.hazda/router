<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Trait RoutesHelperTrait
 * @package Hazadam\Router\Net\Uri\Generator
 */
trait HelperTrait
{
    /**
     * @param string $path
     * @return Url
     */
    protected function createUrlFromPath(string $path): Url
    {
        $url = new Url(null, null, null, null, null, $path, null, null);

        return $url;
    }
}