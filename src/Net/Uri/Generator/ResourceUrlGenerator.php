<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;
use Hazadam\Router\Net\Interfaces\PairedResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\PaginatedResource;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRoute;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\RouteComposition;
use Hazadam\Router\Net\Uri\Generator\Interfaces\PaginationUrlDecoratorInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\ResourceUrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\RootRouteResolverInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\UrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Identifier;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class ResourceUrlGenerator
 * @package Hazadam\Router\Net\Uri\Generator
 */
class ResourceUrlGenerator implements ResourceUrlGeneratorInterface
{
    use HelperTrait;

    /**
     * @var string
     */
    const RESOURCE_SPECIFIER_VALUES_REGEX = "#=.+$#";

    /**
     * @var RootRouteResolverInterface
     */
    protected $rootRouteResolver;

    /**
     * @var UrlGeneratorInterface
     */
    protected $pathGenerator;

    /**
     * @var UrlGeneratorInterface
     */
    protected $queryStringGenerator;

    /**
     * @var PaginationUrlDecoratorInterface
     */
    protected $paginationUrlDecorator;

    /**
     * ResourceUrlGenerator constructor.
     * @param RootRouteResolverInterface $rootRouteResolver
     * @param UrlGeneratorInterface $pathGenerator
     * @param UrlGeneratorInterface $queryStringGenerator
     * @param PaginationUrlDecoratorInterface $paginationUrlDecorator
     */
    public function __construct(
        RootRouteResolverInterface $rootRouteResolver, UrlGeneratorInterface $pathGenerator,
        UrlGeneratorInterface $queryStringGenerator, PaginationUrlDecoratorInterface $paginationUrlDecorator
    )
    {
        $this->rootRouteResolver = $rootRouteResolver;
        $this->pathGenerator = $pathGenerator;
        $this->queryStringGenerator = $queryStringGenerator;
        $this->paginationUrlDecorator = $paginationUrlDecorator;
    }

    /**
     * @inheritdoc
     */
    public function generate(
        ResourceCollectionInterface $resourcesToGenerateFrom, OrderedRouteCollectionInterface $availableRoutes
    ): Url
    {
        $resourceSet = $resourcesToGenerateFrom->toResourceSet();
        $rootResource = $resourceSet->getRootResource();
        $this->validateRootResource($rootResource);

        $rootRoute = $this->rootRouteResolver->resolveRoot($resourceSet, $availableRoutes);
        $this->validateRootRoute($rootRoute);

        $pathRouteCollection = new OrderedRouteCollection();
        $pathRouteCollection->add($rootRoute);
        $queryStringRouteCollection = new OrderedRouteCollection();

        $addToCollectionFunction = $this->createAddToRoutesToGenerateClosure(
            $pathRouteCollection, $queryStringRouteCollection
        );

        if (! $this->isLandingPage($rootRoute)) {

            $this->processEachResourceSpecifier(
                $resourcesToGenerateFrom, $availableRoutes, $rootRoute, $addToCollectionFunction
            );
        }

        $url = $this->generateUrl($pathRouteCollection, $queryStringRouteCollection, $rootRoute, $rootResource);

        return $url;
    }

    /**
     * @param $rootResource
     */
    protected function validateRootResource($rootResource): void
    {
        if (!$rootResource instanceof ResourceInterface) {

            throw new \InvalidArgumentException("There is no root Resource to generate from, provide one");
        }
    }

    /**
     * @param OrderedRouteCollectionInterface $routesToGenerate
     * @return Url
     */
    protected function generatePathUrl(
        OrderedRouteCollectionInterface $routesToGenerate
    ): Url
    {
        $composition = new RouteComposition($routesToGenerate);
        $generatedUrl = $this->pathGenerator->generate($composition);

        return $generatedUrl;
    }

    /**
     * @param OrderedRouteCollectionInterface $routesToGenerate
     * @param RouteInterface $rootRoute
     * @return Url
     */
    protected function generateQueryStringUrl(
        OrderedRouteCollectionInterface $routesToGenerate, RouteInterface $rootRoute
    ): Url
    {
        $fakeRootRoute = $this->createFakeRootRoute($rootRoute);
        $routesToGenerate->add($fakeRootRoute);
        $composition = new RouteComposition($routesToGenerate);
        $generatedUrl = $this->queryStringGenerator->generate($composition);

        return $generatedUrl;
    }

    /**
     * @param RouteInterface $rootRoute
     * @return OrderedRoute
     */
    protected function createFakeRootRoute(RouteInterface $rootRoute): OrderedRoute
    {
        $rootQueryStringIdentifier = new Identifier('?', '');
        $fakeRootRoute = new Route(
            $rootRoute->getId(), $rootRoute->getType(), $rootRoute->getPosition(), $rootQueryStringIdentifier,
            $rootRoute->getController(), $rootRoute->getResource(), $rootRoute->getResourceSpecifiers()
        );
        $fakeRootRoute = new OrderedRoute($fakeRootRoute, []);

        return $fakeRootRoute;
    }

    /**
     * @param CollectionInterface $pathRoutesToGenerate
     * @param CollectionInterface $queryStringRoutesToGenerate
     * @return \Closure
     */
    protected function createAddToRoutesToGenerateClosure(
        CollectionInterface $pathRoutesToGenerate, CollectionInterface $queryStringRoutesToGenerate
    ): \Closure
    {
        $addToRoutesToGenerate = function (RouteInterface $route, int $position = null) use (
            $pathRoutesToGenerate, $queryStringRoutesToGenerate
        ) {

            $position = $position ?? $route->getPosition();
            $isPositionedInPath = $position === RouteInterface::POSITION_PATH;
            $routeCollection = $isPositionedInPath ? $pathRoutesToGenerate : $queryStringRoutesToGenerate;
            $routeCollection->add($route);
        };

        return $addToRoutesToGenerate;
    }

    /**
     * @param PairedResourceSpecifierInterface $resourceSpecifier
     * @param RouteInterface $route
     */
    protected function processPairedResourceSpecifier(
        PairedResourceSpecifierInterface $resourceSpecifier, RouteInterface $route
    ): void
    {
        $this->trimPairedResourceValues($route);
        $separator = '=';

        foreach ($resourceSpecifier->getPairedValues() as $pairedValue) {

            $originalString = $route->getIdentifier()->getString();
            $newString = $originalString . $separator . $pairedValue;
            $route->getIdentifier()->setString($newString);
            $separator = ',';
        }
    }

    /**
     * @param ResourceInterface $rootResource
     * @param Url $generatedPathUrl
     */
    protected function addPagination(ResourceInterface $rootResource, Url $generatedPathUrl): void
    {
        if ($rootResource instanceof PaginatedResource && $rootResource->getPage() > 0) {

            $this->paginationUrlDecorator->addPage($generatedPathUrl, $rootResource->getPage());
        }
    }

    /**
     * @param $rootRoute
     */
    protected function validateRootRoute($rootRoute): void
    {
        if (!$rootRoute instanceof RouteInterface) {

            throw new \InvalidArgumentException();
        }

        if ($rootRoute->getPosition() !== RouteInterface::POSITION_PATH) {

            throw new \LogicException();
        }
    }

    /**
     * @param RouteInterface $route
     */
    protected function trimPairedResourceValues(RouteInterface $route): void
    {
        $originalString = $route->getIdentifier()->getString();
        $trimmedString = preg_replace(self::RESOURCE_SPECIFIER_VALUES_REGEX, '', $originalString);
        $route->getIdentifier()->setString($trimmedString);
    }

    /**
     * @param RouteInterface $rootRoute
     * @return bool
     */
    protected function isLandingPage(RouteInterface $rootRoute): bool
    {
        $empty = !empty($rootRoute->getResourceSpecifiers());

        return $empty;
    }

    /**
     * @param OrderedRouteCollectionInterface $pathRouteCollection
     * @param OrderedRouteCollectionInterface $queryStringRouteCollection
     * @param RouteInterface $rootRoute
     * @param ResourceInterface $rootResource
     * @return Url
     */
    protected function generateUrl(
        OrderedRouteCollectionInterface $pathRouteCollection, OrderedRouteCollectionInterface $queryStringRouteCollection,
        RouteInterface $rootRoute, ResourceInterface $rootResource
    ): Url
    {
        $url = $this->createUrlFromPath('');
        $pathUrl = $this->generatePathUrl($pathRouteCollection);
        $url->setPath($pathUrl->getPath());

        if (count($queryStringRouteCollection) > 0) {

            $queryStringUrl = $this->generateQueryStringUrl($queryStringRouteCollection, $rootRoute);
            $url->setQuery($queryStringUrl->getQuery());
        }

        $this->addPagination($rootResource, $url);

        return $url;
    }

    /**
     * @param OrderedRouteCollectionInterface $availableRoutes
     * @param RouteInterface $route
     * @param RouteInterface $rootRoute
     * @param callable $addToCollectionFunction
     */
    protected function checkForParentRouteWithDifferentPosition(
        OrderedRouteCollectionInterface $availableRoutes, RouteInterface $route,
        RouteInterface $rootRoute, callable $addToCollectionFunction
    ): void
    {
        $parentRouteIds = $availableRoutes->getParentRouteIdsForChildId($route->getId());

        foreach ($parentRouteIds as $parentRouteId) {
            /** @var RouteInterface $parentRoute */
            $parentRoute = $availableRoutes->get($parentRouteId);

            if ($parentRoute !== $rootRoute && $parentRoute->getPosition() !== $route->getPosition()) {

                $addToCollectionFunction($parentRoute, $route->getPosition());
            }
        }
    }

    /**
     * @param ResourceCollectionInterface $resourcesToGenerateFrom
     * @param OrderedRouteCollectionInterface $availableRoutes
     * @param RouteInterface $rootRoute
     * @param callable $addToCollectionFunction
     */
    protected function processEachResourceSpecifier(
        ResourceCollectionInterface $resourcesToGenerateFrom, OrderedRouteCollectionInterface $availableRoutes,
        RouteInterface $rootRoute, callable $addToCollectionFunction
    ): void
    {
        /** @var ResourceSpecifierInterface $resourceSpecifier */
        foreach ($resourcesToGenerateFrom as $resourceSpecifier) {

            $routes = $availableRoutes->getByResourceSpecifierId($resourceSpecifier->getId());
            $route = end($routes);

            if ($route instanceof RouteInterface) {

                if ($resourceSpecifier->getType() === ResourceSpecifierInterface::TYPE_STANDALONE) {

                    $this->checkForParentRouteWithDifferentPosition(
                        $availableRoutes, $route, $rootRoute, $addToCollectionFunction
                    );
                }

                if ($resourceSpecifier instanceof PairedResourceSpecifierInterface) {

                    $this->processPairedResourceSpecifier($resourceSpecifier, $route);
                }

                $addToCollectionFunction($route);
            }
        }
    }
}