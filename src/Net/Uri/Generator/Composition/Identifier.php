<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

/**
 * Class Identifier
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class Identifier
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var array
     */
    protected $parentIds;

    /**
     * @var array
     * @example ['context' => 'order'] = [1 => 3]
     */
    protected $ordering;

    /**
     * @var string
     */
    protected $string;

    /**
     * Identifier constructor.
     * @param mixed $id
     * @param array $parentIds
     * @param array $ordering
     * @param string $string
     */
    public function __construct($id, array $parentIds, array $ordering, string $string)
    {
        $this->id = $id;
        $this->parentIds = $parentIds;
        $this->ordering = $ordering;
        $this->string = $string;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getParentIds(): array
    {
        return $this->parentIds;
    }

    /**
     * @return array
     */
    public function getOrdering(): array
    {
        return $this->ordering;
    }

    /**
     * @return string
     */
    public function getString(): string
    {
        return $this->string;
    }
}