<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Misc\DataType\UniqueStrongTypeAbstractCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\CompositionInterface;

/**
 * Class Composition
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class Composition extends UniqueStrongTypeAbstractCollection implements CompositionInterface
{
    /**
     * @var Identifier|null
     */
    protected $rootIdentifier = null;

    /**
     * @var array
     */
    protected $tree = [];

    /**
     * @inheritDoc
     */
    public function __construct(string $dataType = Identifier::class)
    {
        parent::__construct($dataType);
    }

    /**
     * @return Identifier|null
     */
    public function getRootIdentifier()
    {
        $rootIdentifier = $this->rootIdentifier;

        return $rootIdentifier;
    }

    /**
     * @param $parentId
     * @return Identifier[]|array
     */
    public function getChildIdentifiersIds($parentId)
    {
        $childIds = $this->tree[$parentId] ?? [];
        ksort($childIds);

        return $childIds;
    }

    /**
     * @param Identifier $item
     * @param null $key
     */
    public function add($item, $key = null)
    {
        parent::add($item, $item->getId());
    }

    /**
     * @inheritDoc
     */
    protected function onAdd($key, $item): void
    {
        /** @var Identifier $item */
        $item = $this->objects[$key];

        if ($this->isRootIdentifier($item)) {

            $this->rootIdentifier = $item;
        } else {

            $ordering = $item->getOrdering();

            foreach ($item->getParentIds() as $parentId) {

                $order = $ordering[$parentId];
                $this->tree[$parentId][$order] = $item->getId();
            }
        }
    }

    /**
     * @inheritDoc
     */
    protected function onRemove($key, $item): void
    {
        /** @var Identifier $item */
        $item = $this->objects[$key];

        if ($this->isRootIdentifier($item)) {

            $this->rootIdentifier = null;
        }

        foreach ($item->getParentIds() as $parentId) {

            $this->removeFromTree($item, $parentId);
        }
    }

    /**
     * @param Identifier $item
     * @return bool
     */
    protected function isRootIdentifier(Identifier $item): bool
    {
        $isRoot = empty($item->getParentIds());

        return $isRoot;
    }

    /**
     * @param Identifier $item
     * @param $parentId
     */
    protected function removeFromTree(Identifier $item, $parentId): void
    {
        if (isset($this->tree[$parentId])) {

            $keys = array_keys($this->tree[$parentId], $item->getId());

            foreach ($keys as $key) {

                unset($this->tree[$parentId][$key]);
            }
        }
    }
}