<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\CompositionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteInterface;

/**
 * Class RouteComposition
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class RouteComposition implements CompositionInterface
{
    /**
     * @var string
     */
    const TRAILING_SLASH = '/';

    /**
     * @var RouteCollectionInterface
     */
    protected $routeCollection;

    /**
     * @var ResourceCollectionInterface
     */
    protected $resourceCollection;

    /**
     * RouteComposition constructor.
     * @param OrderedRouteCollectionInterface $routeCollection
     */
    public function __construct(
        OrderedRouteCollectionInterface $routeCollection
    )
    {
        $this->routeCollection = $routeCollection;
    }

    /**
     * @inheritdoc
     */
    public function getRootIdentifier()
    {
        $identifier = null;
        $rootRoutes = $this->routeCollection->getWithoutParentRoutes();

        if (! empty($rootRoutes)) {

            /** @var RouteInterface $rootRoute */
            $rootRoute = end($rootRoutes);
            $identifier = new Identifier($rootRoute->getId(), [], [], $this->trimString($rootRoute));
        }

        return $identifier;

    }

    /**
     * @inheritdoc
     */
    public function getChildIdentifiersIds($parentId)
    {
        $childIds = $this->routeCollection->getChildRouteIdsForParentId($parentId);

        foreach ($childIds as $childId) {
            /** @var RouteInterface $route */
            $route = $this->routeCollection->get($childId);
            $resourceSpecifiers = $route->getResourceSpecifiers();
            $resourceSpecifier = end($resourceSpecifiers);

            if ($resourceSpecifier instanceof ResourceSpecifierInterface
                &&
                $resourceSpecifier->getType() === ResourceSpecifierInterface::TYPE_BOUND
                &&
                empty($this->routeCollection->getChildRouteIdsForParentId($childId))
            ) {
                unset($childIds[key($childIds)]);
            }
        }

        return $childIds;
    }

    /**
     * @inheritdoc
     */
    public function get($key)
    {
        $identifier = null;
        $route = $this->routeCollection->get($key);

        if ($route instanceof OrderedRouteInterface) {

            $id = $route->getId();
            $string = $this->trimString($route);
            $parentIds = array_keys($route->getOrdering());
            $ordering = $route->getOrdering();

            $identifier = new Identifier(
                $id, $parentIds, $ordering, $string
            );
        }

        return $identifier;
    }

    /**
     * @param RouteInterface $route
     * @return string
     */
    protected function trimString(RouteInterface $route): string
    {
        $string = ltrim($route->getIdentifier()->getString(), self::TRAILING_SLASH);

        return $string;
    }
}