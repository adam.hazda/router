<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Net\Interfaces\EndpointInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;

/**
 * Class OrderedRoute
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class OrderedRoute implements OrderedRouteInterface
{
    /**
     * @var RouteInterface
     */
    protected $route;

    /**
     * @var array
     */
    protected $ordering;

    /**
     * OrderedRoute constructor.
     * @param RouteInterface $route
     * @param array $ordering
     */
    public function __construct(RouteInterface $route, array $ordering)
    {
        $this->route = $route;
        $this->ordering = $ordering;
    }

    /**
     * @return array
     */
    public function getOrdering(): array
    {
        return $this->ordering;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->route->getId();
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->route->getType();
    }

    /**
     * @inheritDoc
     */
    public function getPosition(): int
    {
        return $this->route->getPosition();
    }

    /**
     * @inheritDoc
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->route->getIdentifier();
    }

    /**
     * @inheritDoc
     */
    public function getController(): string
    {
        return $this->route->getController();
    }

    /**
     * @inheritDoc
     */
    public function getResource(): ?ResourceInterface
    {
        return $this->route->getResource();
    }

    /**
     * @inheritDoc
     */
    public function setResource(ResourceInterface $resource): void
    {
        $this->route->setResource($resource);
    }

    /**
     * @inheritDoc
     */
    public function getResourceSpecifiers(): array
    {
        return $this->route->getResourceSpecifiers();
    }

    /**
     * @inheritDoc
     */
    public function setResourceSpecifiers(array $resourceSpecifiers): void
    {
        $this->route->setResourceSpecifiers($resourceSpecifiers);
    }
}