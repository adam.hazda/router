<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition\Interfaces;

use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;

/**
 * Class OrderedRouteCollection
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
interface OrderedRouteCollectionInterface extends RouteCollectionInterface
{
    /**
     * @param $parentId
     * @return array
     */
    public function getChildRouteIdsForParentId($parentId): array;


    /**
     * @param $childId
     * @return array
     */
    public function getParentRouteIdsForChildId($childId): array;

    /**
     * @return array
     */
    public function getWithoutParentRoutes(): array;
}