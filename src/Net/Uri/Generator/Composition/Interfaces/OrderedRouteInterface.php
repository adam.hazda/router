<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition\Interfaces;

use Hazadam\Router\Net\Interfaces\RouteInterface;

/**
 * Interface OrderedRouteInterface
 * @package Hazadam\Router\Net\Uri\Generator\Composition\Interfaces
 */
interface OrderedRouteInterface extends RouteInterface
{
    /**
     * @return array
     * @example [context => order] [1 => 2]
     */
    public function getOrdering(): array;
}