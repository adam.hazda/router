<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition\Interfaces;

use Hazadam\Router\Net\Uri\Generator\Composition\Identifier;


/**
 * Class Composition
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
interface CompositionInterface
{
    /**
     * @return Identifier|null
     */
    public function getRootIdentifier();

    /**
     * @param $parentId
     * @return Identifier[]|array
     */
    public function getChildIdentifiersIds($parentId);

    /**
     * @param $key
     * @return mixed
     */
    public function get($key);
}