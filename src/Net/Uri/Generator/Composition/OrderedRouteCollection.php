<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Net\RouteCollection;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteInterface;

/**
 * Class OrderedRouteCollection
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class OrderedRouteCollection extends RouteCollection implements OrderedRouteCollectionInterface
{
    /**
     * @var array
     */
    protected $tree = [];

    /**
     * @var array
     */
    protected $oppositeTree = [];

    /**
     * @var array
     */
    protected $routesWithoutParents = [];

    /**
     * OrderedRouteCollection constructor.
     * @param string $dataType
     */
    public function __construct(string $dataType = OrderedRouteInterface::class)
    {
        parent::__construct($dataType);
    }

    /**
     * @param $parentId
     * @return array
     */
    public function getChildRouteIdsForParentId($parentId): array
    {
        $ordering = $this->tree[$parentId] ?? [];
        ksort($ordering);
        $childIds = array_values($ordering);

        return $childIds;
    }

    /**
     * @param $childId
     * @return array
     */
    public function getParentRouteIdsForChildId($childId): array
    {
        $parentIds = array_keys($this->oppositeTree[$childId]);

        return $parentIds;
    }

    /**
     * @inheritdoc
     */
    public function getWithoutParentRoutes(): array
    {
        $ids = $this->routesWithoutParents;
        $items = $this->getIntersect($ids) ?? [];

        return $items;
    }

    /**
     * @inheritdoc
     */
    protected function onAdd($key, $item): void
    {
        parent::onAdd($key, $item);
        /** @var OrderedRouteInterface $item */
        $item = $this->objects[$key];
        $this->processOrdering($item, $key, function(&$map, $index) use ($key) {
            $map[$index] = $key;
        });
    }

    /**
     * @inheritdoc
     */
    protected function onRemove($key, $item): void
    {
        parent::onRemove($key, $item);
        /** @var OrderedRouteInterface $item */
        $item = $this->objects[$key];
        $this->processOrdering($item, $key, function(&$map, $key) {
            unset($map[$key]);
        });
    }

    /**
     * @param OrderedRouteInterface $item
     * @param $key
     * @param callable $method
     */
    protected function processOrdering(OrderedRouteInterface $item, $key, callable $method = null): void
    {
        foreach ($item->getOrdering() as $parentId => $order) {

            $method($this->tree[$parentId], $order);
            $method($this->oppositeTree[$key], $parentId);
        }

        if (empty($item->getOrdering()))
            $method($this->routesWithoutParents, $key);
    }
}