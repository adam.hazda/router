<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator\Composition;

use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;

/**
 * Class ResourceSet
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class ResourceSet
{
    /**
     * @var ResourceInterface|null
     */
    protected $rootResource = null;

    /**
     * @var ResourceSpecifierInterface[]
     */
    protected $resourceSpecifiers = [];

    /**
     * @return ResourceInterface|null
     */
    public function getRootResource(): ?ResourceInterface
    {
        return $this->rootResource;
    }

    /**
     * @param ResourceInterface $resource
     * @return ResourceSet
     */
    public function setRootResource(ResourceInterface $resource): self
    {
        $this->rootResource = $resource;

        return $this;
    }

    public function getResourceSpecifiers(): array
    {
        return $this->resourceSpecifiers;
    }

    /**
     * @param array $resourceSpecifiers
     * @return ResourceSet
     */
    public function setResourceSpecifiers(array $resourceSpecifiers): self
    {
        $this->resourceSpecifiers = $resourceSpecifiers;

        return $this;
    }

    /**
     * @param ResourceSpecifierInterface $resourceSpecifier
     * @return ResourceSet
     */
    public function addResourceSpecifier(ResourceSpecifierInterface $resourceSpecifier): self
    {
        $this->resourceSpecifiers[$resourceSpecifier->getId()] = $resourceSpecifier;

        return $this;
    }
}