<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri\Generator;

use Hazadam\Router\Net\Uri\Generator\Composition\Composition;
use Hazadam\Router\Net\Uri\Generator\Composition\Identifier;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\CompositionInterface;
use Hazadam\Router\Net\Uri\Generator\Interfaces\UrlGeneratorInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class PathGenerator
 * @package Hazadam\Router\Net\Uri\Generator\Composition
 */
class PathGenerator implements UrlGeneratorInterface
{
    use HelperTrait;

    /**
     * @var string
     */
    const ARGUMENT_NAMES = 'argument_names';

    /**
     * @var string
     */
    const ARGUMENT_VALUES = 'argument_values';

    /**
     * @var string
     */
    const ARGUMENT_NAME_VALUE = 'argument_name_value';

    /**
     * @var array
     */
    const DEFAULT_SEPARATORS = [
        self::ARGUMENT_NAMES => '/',
        self::ARGUMENT_VALUES => ',',
        self::ARGUMENT_NAME_VALUE => '='
    ];

    /**
     * @var array
     */
    protected $separators = [];

    /**
     * PathGenerator constructor.
     * @param array $separators
     */
    public function __construct(array $separators = [])
    {
        $this->separators = $separators === [] ? static::DEFAULT_SEPARATORS : $separators;
    }

    /**
     * @inheritDoc
     */
    public function generate(CompositionInterface $composition): Url
    {
        $path = '';

        $rootIdentifier = $composition->getRootIdentifier();

        if ($rootIdentifier !== null) {

            $path = $this->composePath(
                $composition, $this->separators[self::ARGUMENT_NAMES],
                $rootIdentifier, $this->separators[self::ARGUMENT_NAMES]
            );
        }

        $url = $this->createUrlFromPath($path);

        return $url;
    }

    /**
     * @param CompositionInterface $composition
     * @param array $children
     * @param string $separator
     * @return string
     */
    protected function processChildren(CompositionInterface $composition, array $children, string $separator): string
    {
        $path = '';

        foreach ($children as $childId) {

            $path .= $this->composePath(
                $composition, $separator, $composition->get($childId), $this->separators[self::ARGUMENT_NAME_VALUE]
            );
            $separator = $this->switchSeparators($separator);
        }

        return $path;
    }

    /**
     * @param string $separator
     * @return string
     */
    protected function switchSeparators(string $separator): string
    {
        if ($separator === $this->separators[self::ARGUMENT_NAME_VALUE]) {

            $separator = $this->separators[self::ARGUMENT_VALUES];
        }

        return $separator;
    }

    /**
     * @param CompositionInterface $composition
     * @param string $separator
     * @param Identifier $identifier
     * @param string $nextSeparator
     * @return string
     */
    protected function composePath(
        CompositionInterface $composition, string $separator, Identifier $identifier, string $nextSeparator
    ): string
    {
        $path = $separator . $identifier->getString();
        $path .= $this->processChildren(
            $composition, $composition->getChildIdentifiersIds($identifier->getId()), $nextSeparator
        );

        return $path;
    }
}