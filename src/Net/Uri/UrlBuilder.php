<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Uri;

use Hazadam\Router\Net\Uri\Interfaces\UrlBuilderInterface;

/**
 * Class UrlBuilder
 * @package Hazadam\Net\Uri
 */
class UrlBuilder implements UrlBuilderInterface
{
    /**
     * @inheritDoc
     */
    public function buildFromString(string $url): Url
    {
        $parsedUrl = parse_url($url);
        $url = new Url(
            $parsedUrl['scheme'] ?? null, $parsedUrl['user'] ?? null, $parsedUrl['pass'] ?? null,
            $parsedUrl['host'] ?? null, $parsedUrl['port'] ?? null, $parsedUrl['path'] ?? '',
            $parsedUrl['query'] ?? null, $parsedUrl['fragment'] ?? null
        );

        return $url;
    }

    /**
     * @inheritDoc
     */
    public function urlToString(Url $url): string
    {
        $stringUrl = $url->getScheme() ? $url->getScheme() . '://' : '';
        $stringUrl .= $url->getUser() ? $url->getUser() . ($url->getPass() ? ':' : '@') : '';
        $stringUrl .= $url->getPass() ? $url->getPass() . '@' : '';
        $stringUrl .= $url->getHost() ?? '';
        $stringUrl .= $url->getPort() ? ':' . strval($url->getPort()) : '';
        $stringUrl .= $url->getPath() ?? '';
        $stringUrl .= $url->getQuery() ? '?' . $url->getQuery() : '';
        $stringUrl .= $url->getFragment() ? '#' . $url->getFragment() : '';

        return $stringUrl;
    }
}