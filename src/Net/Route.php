<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\EndpointInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Misc\DataType\UniquelyIdentifiableTrait;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;

/**
 * Class Route
 * @package Hazadam\Router
 */
class Route implements RouteInterface
{
    use UniquelyIdentifiableTrait;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $position;

    /**
     * @var IdentifierInterface
     */
    protected $identifier;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var ResourceInterface|null
     */
    protected $resource;

    /**
     * @var ResourceSpecifierInterface[]
     */
    protected $resourceSpecifiers;

    /**
     * Route constructor.
     * @param $id
     * @param string $type
     * @param int $position
     * @param IdentifierInterface $identifier
     * @param string $controller
     * @param ResourceInterface|null $resource
     * @param array $resourceSpecifiers
     */
    public function __construct(
        $id, string $type, int $position, IdentifierInterface $identifier,
        string $controller, ?ResourceInterface $resource, array $resourceSpecifiers
    )
    {
        $this->id = $id;
        $this->type = $type;
        $this->position = $position;
        $this->identifier = $identifier;
        $this->controller = $controller;
        $this->resource = $resource;
        $this->resourceSpecifiers = $resourceSpecifiers;
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @return ResourceInterface|null
     */
    public function getResource(): ?ResourceInterface
    {
        return $this->resource;
    }

    /**
     * @param ResourceInterface $resource
     */
    public function setResource(ResourceInterface $resource): void
    {
        $this->resource = $resource;
    }

    /**
     * @return ResourceSpecifierInterface[]
     */
    public function getResourceSpecifiers(): array
    {
        return $this->resourceSpecifiers;
    }

    /**
     * @inheritDoc
     */
    public function setResourceSpecifiers(array $resourceSpecifiers): void
    {
        $this->resourceSpecifiers = $resourceSpecifiers;
    }
}