<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;
use Hazadam\Router\Misc\DataType\UniqueStrongTypeAbstractCollection;
use Hazadam\Router\Net\Interfaces\ResourceCollectionInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;

/**
 * Class ResourceCollection
 * @package Hazadam\Router\Net
 */
class ResourceCollection extends UniqueStrongTypeAbstractCollection implements ResourceCollectionInterface
{
    /**
     * @var ResourceInterface|null
     */
    protected $rootResource;

    /**
     * @var array
     */
    protected $resourceSpecifiers = [];

    /**
     * @var array
     */
    protected $resourceToSpecifiersTree = [];

    /**
     * @var array
     */
    protected $specifiersTree = [];

    /**
     * @inheritDoc
     */
    public function __construct(string $dataType = ResourceInterface::class)
    {
        parent::__construct($dataType);
    }

    /**
     * @inheritDoc
     * @param $item UniquelyIdentifiableInterface
     */
    public function add($item, $key = null)
    {
        parent::add($item);
    }

    /**
     * @inheritDoc
     */
    public function toResourceSet(): ResourceSet
    {
        $resourceSet = new ResourceSet();

        if ($this->rootResource)
            $resourceSet->setRootResource($this->rootResource);

        $resourceSet->setResourceSpecifiers($this->resourceSpecifiers);

        return $resourceSet;
    }

    /**
     * @inheritDoc
     */
    public function getChildren(UniquelyIdentifiableInterface $identifiable): array
    {
        $lookUpId = $identifiable->getId();

        if ($identifiable instanceof ResourceSpecifierInterface) {

            $keys = $this->specifiersTree[$lookUpId] ?? [];

        } elseif ($identifiable instanceof ResourceInterface) {

            $keys = $this->resourceToSpecifiersTree[$lookUpId] ?? [];
        }

        $items = $this->getIntersect($keys ?? []);

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function hasResourceSpecifierId($id): bool
    {
        return isset($this->resourceSpecifiers[$id]);
    }

    /**
     * @inheritDoc
     */
    protected function onAdd($key, $item): void
    {
        $item = $this->objects[$key];

        if ($item instanceof ResourceSpecifierInterface) {

            foreach ($item->getParentResourcesIds() as $parentId) {

                $this->resourceToSpecifiersTree[$parentId][$key] = null;
            }

            foreach ($item->getParentResourceSpecifiersIds() as $parentId) {

                $this->specifiersTree[$parentId][$key] = null;
            }

            $this->resourceSpecifiers[$item->getId()] = $item;

        } elseif ($item instanceof ResourceInterface) {

            $this->rootResource = $item;
        }
    }

    /**
     * @inheritDoc
     */
    protected function onRemove($key, $item): void
    {
        $item = $this->objects[$key];

        if ($item instanceof ResourceSpecifierInterface) {

            unset($this->resourceSpecifiers[$item->getId()]);

        } elseif ($item instanceof ResourceInterface) {

            $this->rootResource = null;
        }
    }

    /**
     * @param array $keys
     * @return array
     */
    protected function getIntersect(array $keys): array
    {
        $items = array_intersect_key($this->objects, $keys);

        return $items;
    }

    /**
     * @return \Traversable
     */
    public function getIterator()
    {
        foreach ($this->resourceSpecifiers as $resourceSpecifier) {

            yield $resourceSpecifier;
        }
    }
}