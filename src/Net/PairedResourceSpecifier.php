<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\PairedResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;

/**
 * Class PairedResourceSpecifier
 * @package Hazadam\Router\Net
 */
class PairedResourceSpecifier implements PairedResourceSpecifierInterface
{

    /**
     * @var
     */
    protected $resourceSpecifier;

    /**
     * @var array
     */
    protected $pairedValues;

    /**
     * PairedResourceSpecifier constructor.
     * @param array $pairedValues
     * @param $resourceSpecifier
     */
    public function __construct(ResourceSpecifierInterface $resourceSpecifier, array $pairedValues)
    {
        $this->resourceSpecifier = $resourceSpecifier;
        $this->pairedValues = $pairedValues;
    }

    /**
     * @inheritDoc
     */
    public function getPairedValues(): array
    {
        return $this->pairedValues ?? [];
    }

    /**
     * @inheritDoc
     */
    public function getRoutableType(): string
    {
        return $this->resourceSpecifier->getRoutableType();
    }

    /**
     * @inheritDoc
     */
    public function isSingleOccurrence(): bool
    {
        return $this->resourceSpecifier->isSingleOccurrence();
    }

    /**
     * @inheritDoc
     */
    public function isSingleValued(): bool
    {
        return $this->resourceSpecifier->isSingleValued();
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->resourceSpecifier->getType();
    }

    /**
     * @inheritDoc
     */
    public function getParentResourcesIds(): array
    {
        return $this->resourceSpecifier->getParentResourcesIds();
    }

    /**
     * @inheritDoc
     */
    public function setParentResourcesIds(array $parentResourcesIds): void
    {
        $this->resourceSpecifier->setParentResourcesIds($parentResourcesIds);
    }

    /**
     * @inheritDoc
     */
    public function getParentResourceSpecifiersIds(): array
    {
        return $this->resourceSpecifier->getParentResourceSpecifiersIds();
    }

    /**
     * @inheritDoc
     */
    public function setParentResourceSpecifiersIds(array $parentResourceSpecifiersIds): void
    {
        $this->resourceSpecifier->setParentResourceSpecifiersIds($parentResourceSpecifiersIds);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->resourceSpecifier->getId();
    }
}