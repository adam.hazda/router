<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;
use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\ResourceSet;

/**
 * Interface ResourceCollectionInterface
 * @package Hazadam\Router\Net\Interfaces
 */
interface ResourceCollectionInterface extends CollectionInterface
{
    /**
     * @return ResourceSet
     */
    public function toResourceSet(): ResourceSet;

    /**
     * @param UniquelyIdentifiableInterface $identifiable
     * @return array
     */
    public function getChildren(UniquelyIdentifiableInterface $identifiable): array;

    /**
     * @param $id
     * @return bool
     */
    public function hasResourceSpecifierId($id): bool;
}