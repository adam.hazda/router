<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;
use Hazadam\Router\Net\Uri\Interfaces\IdentifierInterface;

/**
 * Interface RouteInterface
 * @package Hazadam\Router\Interfaces
 */
interface RouteInterface extends UniquelyIdentifiableInterface
{
    /**
     * @var string
     */
    const CONTROLLER = 'controller';

    /**
     * @var string
     */
    const ARGUMENT_NAME = 'argument_name';

    /**
     * @var string
     */
    const ARGUMENT_VALUE = 'argument_value';

    /**
     * @var int
     */
    const POSITION_PATH = 0, POSITION_QUERY_STRING = 1, POSITION_FRAGMENT = 2;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return int
     */
    public function getPosition(): int;

    /**
     * @return IdentifierInterface
     */
    public function getIdentifier(): IdentifierInterface;

    /**
     * @return string
     */
    public function getController(): string;

    /**
     * @return ResourceInterface|null
     */
    public function getResource(): ?ResourceInterface;

    /**
     * @param ResourceInterface $resource
     */
    public function setResource(ResourceInterface $resource): void;

    /**
     * @return ResourceSpecifierInterface[]
     */
    public function getResourceSpecifiers(): array;

    /**
     * @param array $resourceSpecifiers
     */
    public function setResourceSpecifiers(array $resourceSpecifiers): void;
}