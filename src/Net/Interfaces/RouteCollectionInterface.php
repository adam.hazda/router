<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;

/**
 * Class RouteCollection
 * @package Hazadam\Router
 */
interface RouteCollectionInterface extends CollectionInterface
{
    /**
     * @param $string
     * @return array
     */
    public function getByIdentifier($string): array;

    /**
     * @param string $routableType
     * @return array
     */
    public function getByRoutableType(string $routableType): array;

    /**
     * @return array
     */
    public function getWithResources(): array;

    /**
     * @return array
     */
    public function getControllerRoutes(): array;

    /**
     * @param $resourceId
     * @return array
     */
    public function getAllByResourceId($resourceId): array;

    /**
     * @param $resourceSpecifierId
     * @return array
     */
    public function getByResourceSpecifierId($resourceSpecifierId): array;

    /**
     * @param array $resourceSpecifierIds
     * @return array
     */
    public function getCoveringResourceSpecifierIds(array $resourceSpecifierIds): array;

    /**
     * @param $parentId
     * @return array
     */
    public function getByParentResourceSpecifierId($parentId): array;

    /**
     * @param $parentId
     * @return array
     */
    public function getByParentResourceId($parentId): array;

    /**
     * @param $resourceId
     * @return array
     */
    public function getSimpleByResourceId($resourceId): array;
}