<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;

/**
 * Interface ResourceInterface
 * @package Hazadam\Router\Interfaces
 */
interface ResourceInterface extends UniquelyIdentifiableInterface
{
    /**
     * @return string
     */
    public function getRoutableType(): string;
}