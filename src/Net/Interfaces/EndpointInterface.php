<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

/**
 * Interface EndpointInterface
 * @package Hazadam\Router\Interfaces
 */
interface EndpointInterface
{
    /**
     * @return string
     */
    public function getControllerAction(): string;
}