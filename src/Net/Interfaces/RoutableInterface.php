<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

/**
 * Interface RoutableInterface
 * @package Hazadam\Router\Interfaces
 */
interface RoutableInterface
{
    public function getRoute();
}