<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

/**
 * Interface PaginatedResourceInterface
 * @package Hazadam\Router\Net\Interfaces
 */
interface PaginatedResourceInterface extends ResourceInterface
{
    /**
     * @return int
     */
    public function getPage(): int;
}