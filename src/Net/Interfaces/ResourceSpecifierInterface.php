<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

/**
 * Interface ResourceSpecifierInterface
 * @package Hazadam\Router\Interfaces
 */
interface ResourceSpecifierInterface extends ResourceInterface
{
    /**
     * @var string
     */
    const TYPE_STANDALONE = 'standalone';

    /**
     * @var string
     */
    const TYPE_BOUND = 'bound';

    /**
     * @var string
     */
    const TYPE_PAIRED = 'paired';

    /**
     * @return bool
     */
    public function isSingleOccurrence(): bool;

    /**
     * @return bool
     */
    public function isSingleValued(): bool;

    /**
     * Standalone, Bound, Paired
     *
     * @return string
     */
    public function getType(): string;

    /**
     * @return array
     */
    public function getParentResourcesIds(): array;

    /**
     * @param array $parentResourcesIds
     */
    public function setParentResourcesIds(array $parentResourcesIds): void;

    /**
     * @return array
     */
    public function getParentResourceSpecifiersIds(): array;

    /**
     * @param array $parentResourceSpecifiersIds
     */
    public function setParentResourceSpecifiersIds(array $parentResourceSpecifiersIds): void;
}