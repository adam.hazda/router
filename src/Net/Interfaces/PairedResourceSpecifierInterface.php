<?php declare(strict_types=1);

namespace Hazadam\Router\Net\Interfaces;

/**
 * Interface PairedResourceSpecifierInterface
 * @package Hazadam\Router\Net\Interfaces
 */
interface PairedResourceSpecifierInterface extends ResourceSpecifierInterface
{
    /**
     * @return array
     */
    public function getPairedValues(): array;
}