<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;
use Hazadam\Router\Misc\DataType\UniqueStrongTypeAbstractCollection;
use Hazadam\Router\Net\Interfaces\EndpointInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;

/**
 * Class RouteCollection
 * @package Hazadam\Router
 */
class RouteCollection extends UniqueStrongTypeAbstractCollection implements RouteCollectionInterface
{
    /**
     * @var array
     */
    protected $identifiersMap = [];

    /**
     * @var array
     */
    protected $routableTypesMap = [];

    /**
     * @var array
     */
    protected $controllerRoutes = [];

    /**
     * @var array
     */
    protected $withResources = [];

    /**
     * @var array
     */
    protected $resourcesMap = [];

    /**
     * @var array
     */
    protected $resourceSpecifiersMap = [];

    /**
     * @var array
     */
    protected $rs2rsMap = [];

    /**
     * @var array
     */
    protected $r2rsMap = [];

    /**
     * @var array
     */
    protected $coveredResourceSpecifiers;

    /**
     * @var array
     */
    protected $simpleRoute2ResourceMap;

    /**
     * @inheritDoc
     */
    public function __construct(string $dataType = RouteInterface::class)
    {
        parent::__construct($dataType);
    }

    /**
     * @param $string
     * @return array
     */
    public function getByIdentifier($string): array
    {
        $keys = $this->identifiersMap[$string] ?? [];
        $items = $this->getIntersect($keys);

        return $items ?: [];
    }

    /**
     * @param string $routableType
     * @return array
     */
    public function getByRoutableType(string $routableType): array
    {
        $keys = $this->routableTypesMap[$routableType] ?? [];
        $items = $this->getIntersect($keys);

        return $items ?? [];
    }

    /**
     * @inheritdoc
     */
    public function getWithResources(): array
    {
        $items = $this->getIntersect($this->withResources);

        return $items ?? [];
    }

    /**
     * @return array
     */
    public function getControllerRoutes(): array
    {
        $items = $this->getIntersect($this->controllerRoutes);

        return $items ?? [];
    }

    /**
     * @param $resourceId
     * @return array
     */
    public function getAllByResourceId($resourceId): array
    {
        $keys = $this->resourcesMap[$resourceId] ?? [];
        $items = $this->getIntersect($keys);

        return $items;
    }

    /**
     * @inheritDoc
     */
    public function getByResourceSpecifierId($resourceSpecifierId): array
    {
        $keys = $this->resourceSpecifiersMap[$resourceSpecifierId] ?? [];
        $items = $this->getIntersect($keys);

        return $items;
    }

    /**
     * @param $parentId
     * @return array
     */
    public function getByParentResourceSpecifierId($parentId): array
    {
        $keys = $this->rs2rsMap[$parentId] ?? [];
        $items = $this->getIntersect($keys);

        return $items;
    }

    /**
     * @param $parentId
     * @return array
     */
    public function getByParentResourceId($parentId): array
    {
        $keys = $this->r2rsMap[$parentId] ?? [];
        $items = $this->getIntersect($keys);

        return $items;
    }

    /**
     * @param UniquelyIdentifiableInterface $item
     * @param null $key
     * @return void
     */
    public function add($item, $key = null)
    {
        parent::add($item, $item->getId());
    }


    /**
     * @inheritdoc
     */
    public function getCoveringResourceSpecifierIds(array $resourceSpecifierIds): array
    {
        $items = [];

        if (! empty($resourceSpecifierIds)) {

            $hash = $this->createHashFromIds($resourceSpecifierIds);

            if (isset($this->coveredResourceSpecifiers[$hash])) {

                $items = $this->getIntersect($this->coveredResourceSpecifiers[$hash]);
            }
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    public function getSimpleByResourceId($resourceId): array
    {
        $keys = $this->simpleRoute2ResourceMap[$resourceId] ?? [];
        $items = $this->getIntersect($keys);

        return $items;
    }

    /**
     * @param array $keys
     * @return array
     */
    protected function getIntersect(array $keys): array
    {
        $items = array_intersect_key($this->objects, $keys);

        return $items;
    }

    /**
     * @inheritDoc
     */
    protected function onAdd($key, $item): void
    {
        /** @var Route $item */
        $item = $this->objects[$key];

        $this->processItem($key, $item, function(&$map, $key) {
            $map[$key] = null;
        });
    }

    /**
     * @inheritDoc
     */
    protected function onRemove($key, $item): void
    {
        /** @var RouteInterface $item */
        $item = $this->objects[$key];

        $this->processItem($key, $item, function(&$map, $key) {
            unset($map[$key]);
        });
    }

    /**
     * @param $key
     * @param RouteInterface $item
     * @param callable $method
     */
    protected function processItem($key, RouteInterface $item, callable $method): void
    {
        $method($this->identifiersMap[$item->getIdentifier()->getString()], $key);

        if ($item->getController() !== '') {

            $method($this->controllerRoutes, $key);
        }

        $childResourceSpecifierIds = [];

        /** @var ResourceSpecifierInterface $resourceSpecifier */
        foreach ($item->getResourceSpecifiers() as $resourceSpecifier) {

            $childResourceSpecifierIds[] = $resourceSpecifier->getId();

            $method($this->routableTypesMap[$resourceSpecifier->getRoutableType()], $key);
            $method($this->resourceSpecifiersMap[$resourceSpecifier->getId()], $key);

            foreach ($resourceSpecifier->getParentResourcesIds() as $parentId) {

                $method($this->r2rsMap[$parentId], $key);
            }

            foreach ($resourceSpecifier->getParentResourceSpecifiersIds() as $parentId) {

                $method($this->rs2rsMap[$parentId], $key);
            }
        }

        $resource = $item->getResource();

        if ($resource instanceof ResourceInterface) {

            $method($this->routableTypesMap[$item->getResource()->getRoutableType()], $key);
            $method($this->resourcesMap[$resource->getId()], $key);
            $method($this->withResources, $key);

            if (empty($childResourceSpecifierIds)) {

                $method($this->simpleRoute2ResourceMap[$resource->getId()], $key);
            } else {

                $hash = $this->createHashFromIds($childResourceSpecifierIds);
                $method($this->coveredResourceSpecifiers[$hash], $key);
            }
        }
    }

    /**
     * @param array $childResourceSpecifierIds
     * @return string
     */
    protected function createHashFromIds(array $childResourceSpecifierIds): string
    {
        sort($childResourceSpecifierIds);
        $source = implode('', $childResourceSpecifierIds);
        $hash = md5($source);

        return $hash;
    }
}