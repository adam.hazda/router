<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

/**
 * Trait RoutableTypeTrait
 * @package Hazadam\Router\Net
 */
trait RoutableTypeTrait
{
    /**
     * @var string
     */
    protected $routableType;

    /**
     * @return string
     */
    public function getRoutableType(): string
    {
        return $this->routableType;
    }
}