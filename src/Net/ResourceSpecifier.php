<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Misc\DataType\UniquelyIdentifiableTrait;
use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;

/**
 * Class ResourceSpecifier
 * @package Hazadam\Router\Net
 */
class ResourceSpecifier implements ResourceSpecifierInterface
{
    use UniquelyIdentifiableTrait, RoutableTypeTrait;

    /**
     * @var bool
     */
    protected $singleOccurrence;

    /**
     * @var bool
     */
    protected $singleValued;

    /**
     * @var array
     */
    protected $parentResourcesIds = [];

    /**
     * @var array
     */
    protected $parentResourceSpecifiersIds = [];

    /**
     * @var string
     */
    protected $type;

    /**
     * ResourceSpecifier constructor.
     * @param $id
     * @param string $routableType
     * @param string $type
     * @param bool $singleOccurrence
     * @param bool $singleValued
     * @param array $parentResourcesIds
     * @param array $parentResourceSpecifiersIds
     */
    public function __construct(
        $id, string $routableType, string $type, bool $singleOccurrence, bool $singleValued,
        array $parentResourcesIds, array $parentResourceSpecifiersIds
    )
    {
        $this->id = $id;
        $this->routableType = $routableType;
        $this->type = $type;
        $this->singleOccurrence = $singleOccurrence;
        $this->singleValued = $singleValued;
        $this->parentResourcesIds = $parentResourcesIds;
        $this->parentResourceSpecifiersIds = $parentResourceSpecifiersIds;
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isSingleOccurrence(): bool
    {
        return $this->singleOccurrence;
    }

    /**
     * @return bool
     */
    public function isSingleValued(): bool
    {
        return $this->singleValued;
    }

    /**
     * @return array
     */
    public function getParentResourcesIds(): array
    {
        return $this->parentResourcesIds;
    }

    /**
     * @param array $parentResourcesIds
     */
    public function setParentResourcesIds(array $parentResourcesIds): void
    {
        $this->parentResourcesIds = $parentResourcesIds;
    }

    /**
     * @return array
     */
    public function getParentResourceSpecifiersIds(): array
    {
        return $this->parentResourceSpecifiersIds;
    }

    /**
     * @param array $parentResourceSpecifiersIds
     */
    public function setParentResourceSpecifiersIds(array $parentResourceSpecifiersIds): void
    {
        $this->parentResourceSpecifiersIds = $parentResourceSpecifiersIds;
    }
}