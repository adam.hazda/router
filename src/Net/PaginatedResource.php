<?php declare(strict_types=1);

namespace Hazadam\Router\Net;

use Hazadam\Router\Net\Interfaces\PaginatedResourceInterface;
use Hazadam\Router\Net\Interfaces\ResourceInterface;

/**
 * Class PaginatedResource
 * @package Hazadam\Router\Net
 */
class PaginatedResource implements PaginatedResourceInterface
{
    /**
     * @var ResourceInterface
     */
    protected $resource;

    /**
     * @var int
     */
    protected $page;

    /**
     * PaginatedResource constructor.
     * @param ResourceInterface $resource
     * @param int $page
     */
    public function __construct(ResourceInterface $resource, int $page)
    {
        $this->resource = $resource;
        $this->page = $page;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->resource->getId();
    }

    /**
     * @inheritDoc
     */
    public function getRoutableType(): string
    {
        return $this->resource->getRoutableType();
    }

    /**
     * @inheritDoc
     */
    public function getPage(): int
    {
        return $this->page;
    }
}