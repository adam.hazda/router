<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

/**
 * Trait UniqueCollectionTrait
 * @package Hazadam\Router\Misc\DataType
 */
trait UniqueCollectionTrait
{
    /**
     * @inheritDoc
     */
    public function add($item, $key = null)
    {
        if (! $this->has($item)) {

            parent::add($item, $key);
        }
    }
}