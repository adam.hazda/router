<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

use Hazadam\Router\Misc\DataType\Interfaces\CollectionInterface;

/**
 * Class AbstractCollection
 * @package Hazadam\Router\Misc\DataType
 */
abstract class AbstractCollection implements CollectionInterface
{
    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var array
     */
    protected $objects = [];

    /**
     * @inheritDoc
     */
    public function add($item, $key = null)
    {
        if ($this->isKeyInvalid($key)) {

            throw new \InvalidArgumentException(sprintf(
                "You cannot use %s as the key", gettype($key)
            ));
        }

        $key = $this->getObjectIdIfObject($key);
        $origItem = $item;
        $item = $this->getObjectIdIfObject($item);

        if (isset($this->items[$key])) {

            $lastKey = $key;
        }

        if ($key !== null) {

            $this->items[$key] = $item;
        } else {

            $this->items[] = $item;
        }

        if (! isset($lastKey)) {

            end($this->items);
            $lastKey = key($this->items);
        }

        if (is_object($origItem)) {

            $this->objects[$lastKey] = $origItem;
        }

        $this->onAdd($lastKey, $item);
    }

    /**
     * @inheritDoc
     */
    public function remove($item)
    {
        if ($this->has($item)) {

            $item = $this->getObjectIdIfObject($item);
            $keys = array_keys($this->items, $item);

            foreach ($keys as $key ) {

                $this->onRemove($key, $item);
                unset($this->objects[$key]);
                unset($this->items[$key]);
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function removeByKey($key)
    {
        if ($key === null) {
            throw new \InvalidArgumentException(
                "Null is never a key, don't pass it"
            );
        }

        if ($this->hasKey($key)) {

            $key = $this->getObjectIdIfObject($key);

            $this->onRemove($key, $this->get($key));
            unset($this->objects[$key]);
            unset($this->items[$key]);
        }
    }

    /**
     * @inheritDoc
     */
    public function has($item): bool
    {
        if (is_object($item)) {

            $item = spl_object_id($item);
        }

        $has = in_array($item, $this->items);

        return $has;
    }

    /**
     * @inheritDoc
     */
    public function hasKey($key): bool
    {
        $key = $this->getObjectIdIfObject($key);
        $has = isset($this->items[$key]);

        return $has;
    }

    /**
     * @inheritDoc
     */
    public function get($key)
    {
        $item = null;

        if ($this->hasKey($key)) {

            $key = $this->getObjectIdIfObject($key);

            if (isset($this->objects[$key])) {

                $item = $this->objects[$key];
            } else {

                $item = $this->items[$key];
            }
        }

        return $item;
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        $items = [];

        foreach ($this->items as $key => $item) {

            if (isset($this->objects[$key])) {

                $items[$key] = $this->objects[$key];
            } else {

                $items[$key] = $item;
            }
        }

        return $items;
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        $iterator = (function() {

            foreach ($this->items as $key => $item) {

                if (isset($this->objects[$key])) {

                    $item = $this->objects[$key];
                }

                yield $key => $item;
            }
        })();

        return $iterator;
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->items);
    }

    /**
     * @param $key
     * @return bool
     */
    protected function isKeyInvalid($key): bool
    {
        return is_array($key) || is_bool($key);
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getObjectIdIfObject($key)
    {
        if (is_object($key)) {

            $key = spl_object_id($key);
        }

        return $key;
    }

    /**
     * @param $key
     * @param $item
     * @return void
     */
    abstract protected function onAdd($key, $item): void;

    /**
     * @param $key
     * @param $item
     * @return void
     */
    abstract protected function onRemove($key, $item): void;
}