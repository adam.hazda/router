<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType\Interfaces;

/**
 * Interface UniquelyIdentifiableInterface
 * @package Hazadam\Router\Validation
 */
interface UniquelyIdentifiableInterface
{
    /**
     * @return mixed
     */
    public function getId();
}