<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType\Interfaces;


/**
 * Interface TreeNodeInterface
 * @package Hazadam\Router\Misc\DataType\Interfaces
 */
interface TreeNodeInterface
{
    /**
     * @return array
     */
    public function getParentIds(): array;
}