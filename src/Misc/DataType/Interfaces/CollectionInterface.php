<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType\Interfaces;

/**
 * Interface CollectionInterface
 * @package Hazadam\Router\Misc\DataType\Interfaces
 */
interface CollectionInterface extends \Countable, \IteratorAggregate
{
    /**
     * @param $item
     * @param null $key
     * @return mixed
     */
    public function add($item, $key = null);

    /**
     * @param $item
     * @return mixed
     */
    public function remove($item);

    /**
     * @param $key
     * @return mixed
     */
    public function removeByKey($key);

    /**
     * @param $item
     * @return bool
     */
    public function has($item): bool;

    /**
     * @param $key
     * @return bool
     */
    public function hasKey($key): bool;

    /**
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * @return array
     */
    public function getAll(): array;
}