<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

/**
 * Class UniqueStrongTypeAbstractCollection
 * @package Hazadam\Router\Misc\DataType
 */
abstract class UniqueStrongTypeAbstractCollection extends StrongTypeAbstractCollection
{
    use UniqueCollectionTrait;
}