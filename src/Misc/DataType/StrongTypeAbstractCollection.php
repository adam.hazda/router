<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

/**
 * Class StrongTypeAbstractCollection
 * @package Hazadam\Router\Misc\DataType
 */
abstract class StrongTypeAbstractCollection extends AbstractCollection
{
    /**
     * @var string
     */
    private $dataType;

    /**
     * StrongTypeAbstractCollection constructor.
     * @param string $dataType
     */
    public function __construct(string $dataType)
    {
        $this->dataType = $dataType;
    }

    /**
     * @inheritDoc
     */
    public function add($item, $key = null)
    {
        if ($this->isValidScalarItem($item)) {

            throw new \InvalidArgumentException(sprintf(
                "'%s' type is invalid. You may only pass '%s' into collection",
                gettype($item), $this->dataType
            ));
        }

        if ($this->isValidObjectItem($item)) {

            throw new \InvalidArgumentException(sprintf(
                "'%s' type is invalid. You may only pass '%s' into collection",
                get_class($item), $this->dataType
            ));
        }

        parent::add($item, $key);
    }

    /**
     * @param $item
     * @return bool
     */
    protected function isValidDataType($item): bool
    {
        $is = false === is_subclass_of($item, $this->dataType) && get_class($item) !== $this->dataType;

        return $is;
    }

    /**
     * @param $item
     * @return bool
     */
    protected function isValidScalarItem($item): bool
    {
        $is = is_scalar($item) && $this->dataType !== gettype($item);

        return $is;
    }

    /**
     * @param $item
     * @return bool
     */
    protected function isValidObjectItem($item): bool
    {
        $is = is_object($item) && $this->isValidDataType($item);

        return $is;
    }
}