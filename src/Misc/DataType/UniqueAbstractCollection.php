<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

/**
 * Class UniqueAbstractCollection
 * @package Hazadam\Router\Misc\DataType
 */
abstract class UniqueAbstractCollection extends AbstractCollection
{
    use UniqueCollectionTrait;
}