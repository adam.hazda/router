<?php declare(strict_types=1);

namespace Hazadam\Router\Misc\DataType;

/**
 * Trait UniquelyIdentifiedTrait
 * @package Hazadam\Router\Misc\DataType
 */
trait UniquelyIdentifiableTrait
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}