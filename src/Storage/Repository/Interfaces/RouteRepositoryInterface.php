<?php declare(strict_types=1);

namespace Hazadam\Router\Storage\Repository\Interfaces;

use Hazadam\Router\Net\Interfaces\ResourceInterface;
use Hazadam\Router\Net\Interfaces\RouteCollectionInterface;
use Hazadam\Router\Net\Uri\Generator\Composition\Interfaces\OrderedRouteCollectionInterface;

/**
 * Interface RouteRepositoryInterface
 * @package Hazadam\Router\Storage\Repository\Interfaces
 */
interface RouteRepositoryInterface
{
    /**
     * @param array $identifiers
     * @param string $httpMethod
     * @param $languageId
     * @param null $domainId
     * @return RouteCollectionInterface
     */
    public function fetchRoutesByIdentifiers(
        array $identifiers, string $httpMethod, $languageId, $domainId = null
    ): RouteCollectionInterface;

    /**
     * @param ResourceInterface $rootResource
     * @param array $resourceSpecifiers
     * @param string $httpMethod
     * @param $languageId
     * @param null $domainId
     * @return RouteCollectionInterface
     */
    public function fetchRoutesByResources(
        ResourceInterface $rootResource, array $resourceSpecifiers, string $httpMethod, $languageId, $domainId = null
    ): OrderedRouteCollectionInterface;
}