<?php declare(strict_types=1);

namespace Hazadam\Router\Language\Resolving\Interfaces;

use Hazadam\Router\Language\Language;
use Hazadam\Router\Net\Uri\Url;

/**
 * Interface LanguageResolverInterface
 * @package Hazadam\Router\Language\Resolving\Interfaces
 */
interface LanguageResolverInterface
{
    /**
     * @param Url $url
     * @return Language
     */
    public function resolveFromUrl(Url $url): Language;
}