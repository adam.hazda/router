<?php declare(strict_types=1);

namespace Hazadam\Router\Language\Resolving;

use Hazadam\Router\Language\Language;
use Hazadam\Router\Language\Resolving\Interfaces\LanguageResolverInterface;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class AbstractLanguageResolver
 * @package Hazadam\Router\Language\Resolving
 */
abstract class AbstractLanguageResolver implements LanguageResolverInterface
{
    /**
     * @var string
     */
    const REGEX_PATTERN = '(.*)';

    /**
     * @var array
     */
    protected $languagesMap;

    /**
     * @var mixed
     */
    protected $defaultLanguageId;

    /**
     * TldResolver constructor.
     * @param array $languagesMap
     * @param $defaultLanguageId
     */
    public function __construct(array $languagesMap, $defaultLanguageId)
    {
        $this->languagesMap = $languagesMap;
        $this->defaultLanguageId = $defaultLanguageId;

        if (! isset($languagesMap[$defaultLanguageId])) {

            throw new \InvalidArgumentException(sprintf(
                "You have passed a default language '%s' that is not " .
                "among passed languages in map, fix it! Passed map: (%s)",
                $defaultLanguageId, implode(", ", $languagesMap)
            ));
        }
    }

    /**
     * @inheritDoc
     */
    public function resolveFromUrl(Url $url): Language
    {
        $languageId = $this->defaultLanguageId;
        $languageIdentifier = $this->languagesMap[$this->defaultLanguageId];

        if ($this->isUrlSearchable($url)) {

            $searchedPart = $this->getPartToSearch($url);
            $matches = $this->searchForIdentifier($searchedPart);

            if (! empty($matches)) {

                $languageIdentifier = $matches[1];
                $languageId = array_search($languageIdentifier, $this->languagesMap);
            }
        }

        $language = $this->createNewLanguage($languageId, $languageIdentifier);

        return $language;
    }

    /**
     * @param $key
     * @param string $languageIdentifier
     * @return Language
     */
    protected function createNewLanguage($key, string $languageIdentifier): Language
    {
        $language = new Language($key, $languageIdentifier);

        return $language;
    }

    /**
     * @param string $path
     * @return mixed
     */
    protected function searchForIdentifier(string $path)
    {
        preg_match(static::REGEX_PATTERN, $path, $matches);

        return $matches;
    }

    /**
     * @param Url $url
     * @return bool
     */
    abstract protected function isUrlSearchable(Url $url): bool;

    /**
     * @param Url $url
     * @return string
     */
    abstract protected function getPartToSearch(Url $url): string;
}