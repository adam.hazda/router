<?php declare(strict_types=1);

namespace Hazadam\Router\Language\Resolving;
use Hazadam\Router\Net\Uri\Url;

/**
 * Class TldResolver
 * @package Hazadam\Router\Language\Resolving
 */
class TldResolver extends AbstractLanguageResolver
{
    /**
     * @var string
     */
    const REGEX_PATTERN = "#\.(\w+)$#";

    /**
     * @inheritDoc
     */
    protected function isUrlSearchable(Url $url): bool
    {
        $is = is_string($url->getHost()) && $url->getHost() !== '';

        return $is;
    }

    /**
     * @inheritDoc
     */
    protected function getPartToSearch(Url $url): string
    {
        $part = $url->getHost();

        return $part;
    }
}