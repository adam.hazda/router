<?php declare(strict_types=1);

namespace Hazadam\Router\Language\Resolving;

use Hazadam\Router\Net\Uri\Url;

/**
 * Class PathResolver
 * @package Hazadam\Router\Language\Resolving
 */
class PathResolver extends AbstractLanguageResolver
{
    /**
     * @var string
     */
    const REGEX_PATTERN = "#^\/(\w+)#";

    /**
     * @var string
     */
    const ROOT = '/';

    /**
     * @inheritDoc
     */
    protected function isUrlSearchable(Url $url): bool
    {
        $is = is_string($url->getPath()) && $url->getPath() !== self::ROOT;

        return $is;
    }

    /**
     * @inheritDoc
     */
    protected function getPartToSearch(Url $url): string
    {
        $part = $url->getPath();

        return $part;
    }
}