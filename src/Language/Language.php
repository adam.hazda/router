<?php declare(strict_types=1);

namespace Hazadam\Router\Language;

use Hazadam\Router\Misc\DataType\Interfaces\UniquelyIdentifiableInterface;

/**
 * Class Language
 * @package Hazadam\Router\Language
 */
class Language implements UniquelyIdentifiableInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     */
    protected $stringRepresentation;

    /**
     * Language constructor.
     * @param mixed $id
     * @param string $stringRepresentation
     */
    public function __construct($id, string $stringRepresentation)
    {
        $this->id = $id;
        $this->stringRepresentation = $stringRepresentation;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->stringRepresentation;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }
}