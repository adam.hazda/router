<?php

use Hazadam\Router\Net\Interfaces\ResourceSpecifierInterface;
use Hazadam\Router\Net\Interfaces\RouteInterface;
use Hazadam\Router\Net\PairedResourceSpecifier;
use Hazadam\Router\Net\Resource;
use Hazadam\Router\Net\ResourceCollection;
use Hazadam\Router\Net\ResourceSpecifier;
use Hazadam\Router\Net\Route;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRoute;
use Hazadam\Router\Net\Uri\Generator\Composition\OrderedRouteCollection;
use Hazadam\Router\Net\Uri\Generator\PaginationUrlDecorator;
use Hazadam\Router\Net\Uri\Generator\PathGenerator;
use Hazadam\Router\Net\Uri\Generator\QueryStringGenerator;
use Hazadam\Router\Net\Uri\Generator\ResourceUrlGenerator;
use Hazadam\Router\Net\Uri\Generator\RootRouteResolver;
use Hazadam\Router\Net\Uri\Identifier;

require_once __DIR__ . '/vendor/autoload.php';

$generator = new ResourceUrlGenerator(new RootRouteResolver(),
    new PathGenerator(), new QueryStringGenerator(),
    new PaginationUrlDecorator()
);

$start = microtime(true);

$rootIdentifier = new Identifier('category', '');
$rootResource = new Resource(2, '');
$rootRoute = new Route(1, RouteInterface::CONTROLLER, RouteInterface::POSITION_PATH, $rootIdentifier, '', $rootResource, []);
$rootRoute = new OrderedRoute($rootRoute, []);

$availabilityIdentifier = new Identifier('available', '');
$availabilityResourceSpecifier = new ResourceSpecifier(1, 'parameter_name', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [2], []);
$availabilityRoute = new Route(2, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $availabilityIdentifier, '', null, [$availabilityResourceSpecifier]);
$availabilityRoute = new OrderedRoute($availabilityRoute, [1 => 2]);

$colorIdentifier = new Identifier('color', '');
$colorResourceSpecifier = new ResourceSpecifier(2, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []);
$colorRoute = new Route(3, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $colorIdentifier, '', null, [$colorResourceSpecifier]);
$colorRoute = new OrderedRoute($colorRoute, [1 => 1]);

$blueIdentifier = new Identifier('blue', '');
$blueResourceSpecifier = new ResourceSpecifier(3, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$blueRoute = new Route(4, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $blueIdentifier, '', null, [$blueResourceSpecifier]);
$blueRoute = new OrderedRoute($blueRoute, [3 => 2]);

$redIdentifier = new Identifier('red', '');
$redResourceSpecifier = new ResourceSpecifier(4, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$redRoute = new Route(5, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $redIdentifier, '', null, [$redResourceSpecifier]);
$redRoute = new OrderedRoute($redRoute, [3 => 1]);

$orangeIdentifier = new Identifier('orange', '');
$orangeResourceSpecifier = new ResourceSpecifier(5, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$orangeRoute = new Route(6, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $orangeIdentifier, '', null, [$orangeResourceSpecifier]);
$orangeRoute = new OrderedRoute($orangeRoute, [3 => 3]);

$yellowIdentifier = new Identifier('yellow', '');
$yellowResourceSpecifier = new ResourceSpecifier(6, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$yellowRoute = new Route(7, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $yellowIdentifier, '', null, [$yellowResourceSpecifier]);
$yellowRoute = new OrderedRoute($yellowRoute, [3 => 4]);

$greenIdentifier = new Identifier('green', '');
$greenResourceSpecifier = new ResourceSpecifier(7, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$greenRoute = new Route(8, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $greenIdentifier, '', null, [$greenResourceSpecifier]);
$greenRoute = new OrderedRoute($greenRoute, [3 => 5]);

$darkBlueIdentifier = new Identifier('dark-blue', '');
$darkBlueResourceSpecifier = new ResourceSpecifier(8, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$darkBlueRoute = new Route(9, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $darkBlueIdentifier, '', null, [$darkBlueResourceSpecifier]);
$darkBlueRoute = new OrderedRoute($darkBlueRoute, [3 => 6]);

$pinkIdentifier = new Identifier('pink', '');
$pinkResourceSpecifier = new ResourceSpecifier(9, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [2]);
$pinkRoute = new Route(10, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $pinkIdentifier, '', null, [$pinkResourceSpecifier]);
$pinkRoute = new OrderedRoute($pinkRoute, [3 => 7]);

$priceRangeIdentifier = new Identifier('price-range', '');
$priceRangeResourceSpecifier = new ResourceSpecifier(10, 'parameter_name', ResourceSpecifierInterface::TYPE_PAIRED, false, false, [2], []);
$priceRangePairedResourceSpecifier = new PairedResourceSpecifier($priceRangeResourceSpecifier, ['200-300', '2000']);
$priceRangeRoute = new Route(11, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $priceRangeIdentifier, '', null, [$priceRangePairedResourceSpecifier]);
$priceRangeRoute = new OrderedRoute($priceRangeRoute, [1 => 3]);

$sizeIdentifier = new Identifier('size', '');
$sizeResourceSpecifier = new ResourceSpecifier(11, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []);
$sizeRoute = new Route(12, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $sizeIdentifier, '', null, [$sizeResourceSpecifier]);
$sizeRoute = new OrderedRoute($sizeRoute, [1 => 4]);

$smallIdentifier = new Identifier('small', '');
$smallResourceSpecifier = new ResourceSpecifier(12, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [11]);
$smallRoute = new Route(13, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $smallIdentifier, '', null, [$smallResourceSpecifier]);
$smallRoute = new OrderedRoute($smallRoute, [12 => 1]);

$mediumIdentifier = new Identifier('medium', '');
$mediumResourceSpecifier = new ResourceSpecifier(13, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [11]);
$mediumRoute = new Route(14, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $mediumIdentifier, '', null, [$mediumResourceSpecifier]);
$mediumRoute = new OrderedRoute($mediumRoute, [12 => 2]);

$largeIdentifier = new Identifier('large', '');
$largeResourceSpecifier = new ResourceSpecifier(14, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [11]);
$largeRoute = new Route(15, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $largeIdentifier, '', null, [$largeResourceSpecifier]);
$largeRoute = new OrderedRoute($largeRoute, [12 => 3]);

$extraLargeIdentifier = new Identifier('extra-large', '');
$extraLargeResourceSpecifier = new ResourceSpecifier(15, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [11]);
$extraLargeRoute = new Route(16, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $extraLargeIdentifier, '', null, [$extraLargeResourceSpecifier]);
$extraLargeRoute = new OrderedRoute($extraLargeRoute, [12 => 4]);

$manufacturerIdentifier = new Identifier('manufacturer', '');
$manufacturerResourceSpecifier = new ResourceSpecifier(16, 'parameter_name', ResourceSpecifierInterface::TYPE_BOUND, false, false, [2], []);
$manufacturerRoute = new Route(17, RouteInterface::ARGUMENT_NAME, RouteInterface::POSITION_PATH, $manufacturerIdentifier, '', null, [$manufacturerResourceSpecifier]);
$manufacturerRoute = new OrderedRoute($manufacturerRoute, [1 => 5]);

$panasonicIdentifier = new Identifier('panasonic', '');
$panasonicResourceSpecifier = new ResourceSpecifier(17, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$panasonicRoute = new Route(18, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $panasonicIdentifier, '', null, [$panasonicResourceSpecifier]);
$panasonicRoute = new OrderedRoute($panasonicRoute, [17 => 1]);

$boshIdentifier = new Identifier('bosh', '');
$boshResourceSpecifier = new ResourceSpecifier(18, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$boshRoute = new Route(19, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $boshIdentifier, '', null, [$boshResourceSpecifier]);
$boshRoute = new OrderedRoute($boshRoute, [17 => 2]);

$whirlpoolIdentifier = new Identifier('whirlpool', '');
$whirlpoolResourceSpecifier = new ResourceSpecifier(19, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$whirlpoolRoute = new Route(20, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $whirlpoolIdentifier, '', null, [$whirlpoolResourceSpecifier]);
$whirlpoolRoute = new OrderedRoute($whirlpoolRoute, [17 => 3]);

$phillipsIdentifier = new Identifier('phillips', '');
$phillipsResourceSpecifier = new ResourceSpecifier(20, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$phillipsRoute = new Route(21, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $phillipsIdentifier, '', null, [$phillipsResourceSpecifier]);
$phillipsRoute = new OrderedRoute($phillipsRoute, [17 => 4]);

$bohemiaIdentifier = new Identifier('bohemia', '');
$bohemiaResourceSpecifier = new ResourceSpecifier(21, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$bohemiaRoute = new Route(22, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $bohemiaIdentifier, '', null, [$bohemiaResourceSpecifier]);
$bohemiaRoute = new OrderedRoute($bohemiaRoute, [17 => 5]);

$laysIdentifier = new Identifier('lays', '');
$laysResourceSpecifier = new ResourceSpecifier(22, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$laysRoute = new Route(23, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $laysIdentifier, '', null, [$laysResourceSpecifier]);
$laysRoute = new OrderedRoute($laysRoute, [17 => 6]);

$huaweiIdentifier = new Identifier('huawei', '');
$huaweiResourceSpecifier = new ResourceSpecifier(23, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$huaweiRoute = new Route(24, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $huaweiIdentifier, '', null, [$huaweiResourceSpecifier]);
$huaweiRoute = new OrderedRoute($huaweiRoute, [17 => 7]);

$nokiaIdentifier = new Identifier('nokia', '');
$nokiaResourceSpecifier = new ResourceSpecifier(24, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$nokiaRoute = new Route(25, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_PATH, $nokiaIdentifier, '', null, [$nokiaResourceSpecifier]);
$nokiaRoute = new OrderedRoute($nokiaRoute, [17 => 8]);

$samsungIdentifier = new Identifier('samsung', '');
$samsungResourceSpecifier = new ResourceSpecifier(25, 'parameter_value', ResourceSpecifierInterface::TYPE_STANDALONE, false, false, [], [16]);
$samsungRoute = new Route(26, RouteInterface::ARGUMENT_VALUE, RouteInterface::POSITION_QUERY_STRING, $samsungIdentifier, '', null, [$samsungResourceSpecifier]);
$samsungRoute = new OrderedRoute($samsungRoute, [17 => 9]);

$resourceCollection = new ResourceCollection();
$resourceCollection->add($rootResource);
$resourceCollection->add($availabilityResourceSpecifier);
$resourceCollection->add($colorResourceSpecifier);
$resourceCollection->add($blueResourceSpecifier);
$resourceCollection->add($redResourceSpecifier);
$resourceCollection->add($orangeResourceSpecifier);
$resourceCollection->add($yellowResourceSpecifier);
$resourceCollection->add($greenResourceSpecifier);
$resourceCollection->add($darkBlueResourceSpecifier);
$resourceCollection->add($pinkResourceSpecifier);
$resourceCollection->add($priceRangePairedResourceSpecifier);
$resourceCollection->add($sizeResourceSpecifier);
$resourceCollection->add($smallResourceSpecifier);
$resourceCollection->add($mediumResourceSpecifier);
$resourceCollection->add($largeResourceSpecifier);
$resourceCollection->add($extraLargeResourceSpecifier);
$resourceCollection->add($manufacturerResourceSpecifier);
$resourceCollection->add($panasonicResourceSpecifier);
$resourceCollection->add($boshResourceSpecifier);
$resourceCollection->add($whirlpoolResourceSpecifier);
$resourceCollection->add($phillipsResourceSpecifier);
$resourceCollection->add($bohemiaResourceSpecifier);
$resourceCollection->add($laysResourceSpecifier);
$resourceCollection->add($huaweiResourceSpecifier);
$resourceCollection->add($nokiaResourceSpecifier);
$resourceCollection->add($samsungResourceSpecifier);


$availableRoutes = new OrderedRouteCollection();
$availableRoutes->add($rootRoute);
$availableRoutes->add($availabilityRoute);
$availableRoutes->add($colorRoute);
$availableRoutes->add($blueRoute);
$availableRoutes->add($redRoute);
$availableRoutes->add($orangeRoute);
$availableRoutes->add($yellowRoute);
$availableRoutes->add($greenRoute);
$availableRoutes->add($darkBlueRoute);
$availableRoutes->add($pinkRoute);
$availableRoutes->add($priceRangeRoute);
$availableRoutes->add($sizeRoute);
$availableRoutes->add($smallRoute);
$availableRoutes->add($mediumRoute);
$availableRoutes->add($largeRoute);
$availableRoutes->add($extraLargeRoute);
$availableRoutes->add($manufacturerRoute);
$availableRoutes->add($panasonicRoute);
$availableRoutes->add($boshRoute);
$availableRoutes->add($whirlpoolRoute);
$availableRoutes->add($phillipsRoute);
$availableRoutes->add($bohemiaRoute);
$availableRoutes->add($laysRoute);
$availableRoutes->add($huaweiRoute);
$availableRoutes->add($nokiaRoute);
$availableRoutes->add($samsungRoute);

foreach (range(0, 1000) as $i) {

    $url = $generator->generate($resourceCollection, $availableRoutes);
}


$stop = microtime(true);
var_dump($stop - $start);